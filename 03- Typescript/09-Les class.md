# Les classes
## 1- Déclaration des classes : 
Une classe se déclare de cette façon:
````typescript
class Car {
 //il faut d'abord déclarer la propriété avant de l'enregistrer
Engine: string; // par défaut, Engine est public

    constructor(engine: string) { 
        this.Engine = engine; 
    }
 }
````
## 2-Héritage: 
### Public :
````typescript
class Animal {
public name: string;

public constructor(theName: string) {
this.name = theName;
}

public move(distanceInMeters: number) {
console.log(`${this.name} moved ${distanceInMeters}m.`);
}
}
````
=> Dans TypeScript, chaque membre est public par défaut.

### Privé :
TypeScript a également sa propre façon de déclarer un membre comme étant marqué private, il n'est pas accessible depuis l'extérieur de sa classe conteneur. Par exemple:
````typescript
class Animal {
private name: string;

constructor(theName: string) {
this.name = theName;
}
}

new Animal("Cat").name;
//Property 'name' is private and only accessible within class 'Animal'.
````
### Protected :

Le protectedmodificateur agit un peu comme le privatemodificateur, à l'exception du fait que les membres déclarés protectedsont également accessibles dans les classes dérivées. Par exemple,
`````typescript
class Person {
protected name: string;
constructor(name: string) {
this.name = name;
}
}

class Employee extends Person {
private department: string;

constructor(name: string, department: string) {
super(name);
this.department = department;
}

public getElevatorPitch() {
return `Hello, my name is ${this.name} and I work in ${this.department}.`;
}
}

let howard = new Employee("Howard", "Sales");
console.log(howard.getElevatorPitch());
console.log(howard.name);
//Property 'name' is protected and only accessible within class 'Person' and its subclasses.
`````
=> **public** = Les paramètres sont accessibles à l’extérieur de la classe.

=> **private** = les paramètres ne sont accessibles qu’à l’intérieur de la classe.

=> **protected** = les paramètres sont accessibles dans la classe et les classes qui en héritent d'une classe “enfant”.

### Déclaration d'interface et une classe : 
````typescript
interface Icar { // déclartation d'une interface
name: strings;  
model: strings; 
move(): void; // void ne retourne rien
}
class Car implements Icar { // class Car doit implementer le contrat défini dans l'interface ICar
name: strings;
model:strings;

    move(): void{
        
    }
}

// création d'instance
let car: Icar = new Car(); // car va etre du type ICar
car.name // ici on peut accéder au nom 
````

--------------
Lien de vidéo :
[Typescript - class ](https://youtu.be/u3FkhyrHJCs)