# Enums

Un ajout utile à l'ensemble standard de types de données de JavaScript est le enum. Comme dans les langages comme C #, une énumération est un moyen de donner des noms plus conviviaux à des ensembles de valeurs numériques.

Pour créer un énumérateur on utilise le mot clé **enum**.

## Exemple :
``` typescript
enum Color {
Red,
Green,
Blue,
}
let c: Color = Color.Green;
```
Par défaut, les énumérations commencent à numéroter leurs membres à partir de 0. Vous pouvez modifier cela en définissant manuellement la valeur de l'un de ses membres. Par exemple, nous pouvons commencer l'exemple précédent à au 1lieu de 0:
``` typescript
enum Color {
Red = 1,
Green,
Blue,
}
let c: Color = Color.Green;
```


## 1- Enumération numérique : 
``` typescript
enum Direction {// Une énumération est définie à l'aide du enum
Up = 1,//  Up est initialisé avec 1
Down,//2
Left,//3
Right,//4
}
// ous les membres suivants sont automatiquement incrémentés à partir de ce point.
 //En d'autres termes, Direction.Up a la valeur 1, Down a 2, Left a 3et Right a 4.

```
On peut aussi ne pas réinitialiser 

dans ce cas on a :
``` typescript
enum Direction {
Up,// Up aurait la valeur 0( 0 par défaut et les autres seront incrémentées de 1)
Down,// Down aurait 1,
Left,//2
Right,//3
}
```
## 2- Enumération des chaines: 

 ## Exemple :
``` typescript
enum Direction {
Up = "UP",
Down = "DOWN",
Left = "LEFT",
Right = "RIGHT",
}
```

------------ 
Liens de vidéo:
[Typescript - enums](https://youtu.be/o__NKdLOq8g)
