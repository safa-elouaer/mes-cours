# Interfaces 
- Il s'agit d'un contrat pour nos objets.
- Il rassemble aux types
- On ajoute le I au début des noms pour dire qu'il s'agit d'interface.(mais pas obligatoire)

- Pour déclarer une interface on utilise le lot clé **interface** .

Une interface spécifie une liste de champs et de fonctions pouvant être attendus sur toute classe implémentant l'interface. À l'inverse, une classe ne peut implémenter une interface que si elle possède tous les champs et toutes les fonctions spécifiés sur l'interface.

## 1- Syntaxe:
````typescript
interface NomInterface {
parameterName: parameterType;
optionalParameterName ?: parameterType;
}
````
### Exemple 1:
````typescript
interface I1 {
a: number;
}

interface I2 {
b: string;
}
//L'interface I1 ci-dessus indique que la structure de données implémentant I1 doit avoir à minima un attribut nommé a de type number. L'interface I2 indique que la structure de données l'implémentant doit avoir à minima un attribut nommé b de type string.

````
un autre exemple :
````typescript 
interface IVehicule {
model: string; 
km: number; 
move(): boolean; 
marque?: string; // propriété optionnel

}

let car: IVehicule; 
car ={
model: 'Audi',
km: 12,
move: function (){
return true;
},
marque: 'ok' ;

}
let car: IVehicule; 
car ={
model: 'Audi',
km: 12,
move: function (){
return true;
},
marque: 'ok'

};
````
## 2- Cas des fonctions:
````typescript
interface Myfn { 
(): boolean; 

}

let myFn: MyFn = function() { 
return true; 
};
````
- 2ème exemple :
### Exemple 2 (dans le cas des fonctions) :
````typescript
interface IPerson{  
firstName: string;
lastName: string;
}
function greeter(person: IPerson){
return "Hello " + person.firstName + " " + person.lastName + ".";
}

var myVar: IPerson =  { firstName:"Sylvain", lastName: "Sidambarom"}
greeter(myVar); //  Hello Sylvain Sidambarom.
````
## 3- Cas des tableaux : 
````typescript
interface RandomArray {//nommer l'interface
[index: number]: string;
}
// On déclare une varaiable arr tableau 
let arr: RandomArray = ["Ma valeur"];

console.log(arr[0]); 
````
## 4- Interface avec héritage : 
````typescript
interface IVehicule { 
model: string;
}
interface ICar extends IVehicule{ // ICar prends les propriétés de IVehicule
wheels: number;
}
let car: ICar = {
wheels: 4, 
model: 'ok'
};
````

-------------

Lien de vidéo :
[Typescript - interfaces](https://youtu.be/fJQZGKtMGTw)



