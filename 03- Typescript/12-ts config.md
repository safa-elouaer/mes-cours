# Configuration typescript

L'exécution tsc locale compilera le projet le plus proche défini par a tsconfig.json, vous pouvez compiler un ensemble de fichiers TypeScript en passant un ensemble de fichiers que vous voulez.

## Exemple :
**--help** ou **-h** : un message d'aide.

**--build** ou **-b**:Construit ce projet et toutes ses dépendances spécifiées par les références de projet.

**--pretty**: Stylisez les erreurs et les messages en utilisant la couleur et le contexte.

**--project** ou **-p**: Compilez un projet avec un fichier de configuration valide.

**--version** ou **-v**:la version du compilateur

**--watch** ou **- w**: Exécutez le compilateur en mode veille. Regardez les fichiers d'entrée et déclenchez la recompilation en cas de modifications.

**--init** :Initialise un projet TypeScript et crée un tsconfig.jsonfichier.

**--outDir** :Redirigez la structure de sortie vers le répertoire.

--------------------
Lien de vidéo :
[Typescript - ts config](https://youtu.be/gUeO7Vg3GWU)



