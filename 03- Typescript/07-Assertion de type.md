# Assertions de types

Les assertions de type sont un moyen de dire au compilateur "fais-moi confiance, je sais ce que je fais".

TypeScript suppose que vous, le programmeur, avez effectué toutes les vérifications spéciales dont vous avez besoin.

Les assertions de type ont deux formes.
> as ou bien la syntaxe «angle-bracket» <>

## Exemple 1 : 
``` typescript
let someValue: unknown = "this is a string";

let strLength: number = (someValue as string).length;
```

## Exemple 2 : 
``` typescript
let someValue: unknown = "this is a string";

let strLength: number = (<string>someValue).length;
``` 

------------
Lien de vidéo :

[Typescript - types assertions ](https://youtu.be/Osl7mcX0jbs)
