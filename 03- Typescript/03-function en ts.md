
# Les fonctions

Comme en JavaScript, les fonctions TypeScript peuvent être créées à la fois en tant que fonction nommée ou en tant que fonction anonyme. Cela vous permet de choisir l'approche la plus appropriée pour votre application, que vous construisiez une liste de fonctions dans une API ou une fonction ponctuelle à transférer vers une autre fonction.

En javascript on déclare les fonctions comme suit : 
``` javascript
// Named function
function add(x, y) {
  return x + y;
}

// Anonymous function
let myAdd = function (x, y) {
  return x + y;
};
``` 
## 1-Saisir une fonction en Typescript : 
On ajoute un typage aux paramètres de la fonction.

Ajoutons des types à nos exemples simples précédents:
``` typescript

function add(x: number, y: number): number {
return x + y;
}

let myAdd = function (x: number, y: number): number {
return x + y;
};
```
## 2- Ecrire le type de fonction :

Le type d'une fonction comprend les deux mêmes parties: le type des arguments et le type de retour. Lors de l'écriture de l'ensemble du type de fonction, les deux parties sont nécessaires. Nous écrivons les types de paramètres comme une liste de paramètres, en donnant à chaque paramètre un nom et un type. Ce nom est juste pour aider à la lisibilité.
``` typescript
let myAdd: (x: number, y: number) => number = function (
x: number,
y: number
): number {
return x + y;
};
```
Par défaut et en l'absence d'instruction return, le type du résultat d'une fonction est void, c'est-à-dire aucun résultat.

>void : rien
> 
>=> : va retourner
### Void : 
Ce type est souvent utilisé pour déclarer le type de retour d’une fonction qui ne renvois rien

*Exemple*:
``` typescript
function display(message: string): void{
console.log(message);
}
```
### Exemple de typage d'une fonction : 
``` typescript
function triple(n: number): number {
return 3 * n;
}
//La fonction triple ci-dessus est déclarée comme prenant un paramètre de type number et renvoyant une valeur de type number
``` 
## 3- Paramètre facultatif :
En JavaScript, chaque paramètre est facultatif et les utilisateurs peuvent les laisser désactivés comme bon leur semble. 

Quand ils le font, leur valeur est undefined.
Nous pouvons obtenir cette fonctionnalité dans TypeScript en ajoutant un ? à la fin des paramètres que nous voulons être facultatifs. Par exemple, disons que nous voulons que le paramètre de nom ci-dessus soit facultatif:

>? : argument optionnel

### Exemple : 
``` typescript
function buildName(firstName: string, lastName?: string) {
if (lastName) return firstName + " " + lastName;
else return firstName;
}

let result1 = buildName("Bob"); // works correctly now
let result2 = buildName("Bob", "Adams", "Sr."); // error, too many parameters
Expected 1-2 arguments, but got 3.
let result3 = buildName("Bob", "Adams"); // ah, just right
```
Tous les paramètres facultatifs doivent suivre les paramètres obligatoires.

Si nous voulions rendre le prénom facultatif, plutôt que le nom de famille, nous aurions besoin de changer l'ordre des paramètres dans la fonction, en mettant le prénom en dernier dans la liste.

------------
[Typescript - functions](https://youtu.be/0iGLn7h73Hs)