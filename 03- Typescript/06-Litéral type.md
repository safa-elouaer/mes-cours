# Litéral type
Un littéral est un sous-type plus concret d'un type collectif.
Il existe aujourd'hui trois ensembles de types littéraux disponibles dans TypeScript: les chaînes, les nombres et les booléens.
## 1- Types littéraux de chaîne : 

En pratique, les types de littéraux de chaîne se combinent parfaitement avec les types d'union, les protections de type et les alias de type. 

Vous pouvez utiliser ces fonctionnalités ensemble pour obtenir un comportement semblable à une énumération avec des chaînes.

### Exemple : 
``` typescript
let transition: 'ease-in' | 'ease-out' ; // type chaine de cararctere deja defini
transition = 'ease-out' ; // il faut assigner soit   ease-in ou ease-out

function transformX(transition : 'ease-in' | 'ease-out'): void {

}
transformX("ease-in")
```
## 2- Types littéraux numériques : 

TypeScript a également des types de littéraux numériques, qui agissent de la même manière que les littéraux de chaîne ci-dessus.
``` typescript
function rollDice(): 1 | 2 | 3 | 4 | 5 | 6 {
return (Math.floor(Math.random() * 6) + 1) as 1 | 2 | 3 | 4 | 5 | 6;
}

const result = rollDice();
```

----------
Lien vidéo :

[Typescript - literal types](https://youtu.be/8pexkpGCe5A)
