# Les notions de base de Typescript

## 1- Typescript:

TypeScript est un langage de programmation développé par Microsoft en 2012.

C’est un langage open source, développé comme un sur-ensemble de Javascript. 

Ce le langage introduit des fonctionnalités optionnelles comme le typage ou encore la programmation orientée objet.

Le code TypeScript est transformé en code JavaScript via le compilateur TypeScript ou Babel . 

Ce JavaScript est un code propre et simple qui s'exécute partout où JavaScript s'exécute: dans un navigateur, sur Node.JS ou dans vos applications.

## 2- Node.js:

Node est le runtime qui permet d'écrire toutes nos tâches côté serveur, en JavaScript, telles que la logique métier, la persistance des données et la sécurité.

Node ajoute également des fonctionnalités que le JavaScript du navigateur standard ne possède pas, comme par exemple l'accès au système de fichiers local.

**Node.js est une plateforme de développement Javascript. Ce n'est pas un serveur, ce n'est pas un framework, c'est juste le langage Javascript avec des bibliothèques permettant de réaliser des actions comme écrire sur la sortie standard, ouvrir/fermer des connections réseau ou encore créer un fichier.**

## 3- npm:

**npm** c'est **Node Package Manager**  NPM est un gestionnaire de packages pour les packages Node.js, ou des modules.
Le programme NPM est installé sur votre ordinateur lorsque vous installez Node.js.

### Qu'est-ce qu'un package?

Un package dans Node.js contient tous les fichiers dont vous avez besoin pour un module.

Les modules sont des bibliothèques JavaScript que vous pouvez inclure dans votre projet.

- NPM crée un dossier nommé "node_modules", où le package sera placé. Tous les packages que vous installez à l'avenir seront placés dans ce dossier.

- Tous les packages npm sont définis dans des fichiers appelés package.json .

Le contenu de package.json doit être écrit en JSON .

Au moins deux champs doivent être présents dans le fichier de définition: nom et version .

### Exemple :
>
>{
>"name" : "foo",
>
>"version" : "1.2.3",
>
>"description" : "A package for fooing things",
> 
>"main" : "foo.js",
> 
>"keywords" : ["foo", "fool", "foolish"],
> 
>"author" : "John Doe",
> 
>"licence" : "ISC" 
>}
>
### Gérer les dépendances :
npm peut gérer les dépendances .

npm peut (en une seule ligne de commande) installer toutes les dépendances d'un projet.

Les dépendances sont également définies dans package.json.

____________________
<a id="liens"></a>
Liens des vidéos:
1- [presentation nodejs](https://youtu.be/8Ak786GELow)

2- [presentation npm](https://youtu.be/N-RpiBQF4Zg)

3- [présentation typescript](https://youtu.be/a114c2kEgLU)

4- [configurer un package nodejs](https://youtu.be/s3WTu1jJgIc)
