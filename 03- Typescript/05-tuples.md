# Tuples

Les types de tuple vous permettent d'exprimer un tableau avec un nombre fixe d'éléments dont les types sont connus, mais pas nécessairement les mêmes. Par exemple, vous souhaiterez peut-être représenter une valeur sous la forme d'une paire de a stringet a number:

## Exemple : 
``` typescript
// Declare a tuple type
let x: [string, number];
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
x = [10, "hello"]; // Error
Type 'number' is not assignable to type 'string'.
Type 'string' is not assignable to type 'number'.
```

---------
Lien de vidéo : 
[Typescript - tuples]( https://youtu.be/_p4Jn5tDNSU)