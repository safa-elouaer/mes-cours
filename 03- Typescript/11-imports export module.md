# Import export module 

## 1-Les modules : 
Les modules sont exécutés dans leur propre étendue, pas dans la portée globale; cela signifie que les variables, fonctions, classes, etc. déclarées dans un module ne sont pas visibles en dehors du module à moins qu'elles ne soient explicitement . 

A l'inverse, pour consommer une variable, une fonction, une classe, une interface, etc. exportée depuis un module différent, il faut l'importer.

## 2- Exporter : 
Toute déclaration (telle qu'une variable, une fonction, une classe, un alias de type ou une interface) peut être exportée en ajoutant le  mot - clé **export**.

### Exemple : 
````typescript
interface ICar {  
    model: string; 
    km: number; 
}

class Car implements Icar { 

    constructor( public model: string, public km: number) {}
}

let car = new Car('BMW', 156);


export { Car, ICar} ;// on met ce qu'on veut exporter

export default Car;

````

## 2- Importer : 
L'importation est à peu près aussi simple que l'exportation à partir d'un module.

Pour importer il nous faut que : le mot clé **import** et le chemin d'accès grace au mot clé **from**

### Exemple : 
````typescript
import Car from './car';// from c'est pour le chemin d'accès

import * as fromCar from './car' ; //*as fromCar  pour dire qu'on va récupérer tout les paramètres

let car = new Car('RS3', 124)
````

-------------------

Lien de vidéo :
[Typescript - import export modules ](https://youtu.be/u6hSND2wNcw)
