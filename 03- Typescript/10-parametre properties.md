# Paramètres properties

Les propriétés des paramètres vous permettent de créer et d'initialiser un membre en un seul endroit
````typescript
class Person { // déclare une class 
    
    public name: string; 
    
    constructor(public name: string) { 
    this.name =name; 
    }
}
let person: Person = new Person('Jean');//typer avec le nom de la classe

console.log(typeof person, person.name);

````

---------------

lien de vidéo :
[ parameter properties]( https://youtu.be/iwC6cAd2y4U)