# Basic Types

TypeScript a plusieurs types tels que number, string, boolean, enum, void, null, undefined, any, never, array, et tuple.

Dans TypeScript, nous prenons en charge les mêmes types que ceux auxquels vous vous attendez en JavaScript, avec un type d'énumération supplémentaire ajouté pour aider les choses.

## 1- Booléen : 

Le type de données le plus basique est la simple valeur true / false, que JavaScript et TypeScript appellent une boolean valeur.

### Exemple : 

``` Typescript
let var1: boolean = false;
```
## 2- Les nombres : 

Comme dans JavaScript, tous les nombres de TypeScript sont des valeurs à virgule flottante ou des BigIntegers. Ces nombres à virgule flottante obtiennent le type number, tandis que les BigIntegers obtiennent le type bigint. En plus des littéraux hexadécimaux et décimaux, TypeScript prend également en charge les littéraux binaires et octaux introduits dans ECMAScript 2015.
``` Typescript
let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;
let big: bigint = 100n;
```
## 3- Chaine de caractère : 

Un autre élément fondamental de la création de programmes en JavaScript pour les pages Web et les serveurs consiste à travailler avec des données textuelles. Comme dans d'autres langues, nous utilisons le type stringpour faire référence à ces types de données textuelles. Tout comme JavaScript, TypeScript utilise également des guillemets doubles ( ") ou des guillemets simples ( ') pour entourer les données de chaîne.
``` Typescript
let color: string = "blue";
color = 'red';
```

## 4- Les tableaux : 

TypeScript, comme JavaScript, vous permet de travailler avec des tableaux de valeurs. Les types de tableaux peuvent être écrits de deux manières. Dans le premier, vous utilisez le type des éléments suivi de []pour désigner un tableau de ce type d'élément:

### Exemple : 
``` Typescript
let list: number[] = [1, 2, 3];
```

## 5- Dans le cas ou on a plusieurs type:

Dans le cas ou on a plusieurs type juste il faut ajouter une pipe:

### Exemple :

``` Typescript

let nb: string |[]|boolean=3;
nb=3;

``` 
____________________
<a id="liens"></a>
Liens des vidéos :
[ basic types]( https://youtu.be/WlyIWGLzHo4)
