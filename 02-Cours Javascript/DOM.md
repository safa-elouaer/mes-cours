# Le DOM
## I-Introduction:

Le DOM signifie **Document Object Model** est une interface de programmation qui est une présentation du HTML d'une page web et qui permet d'accéder aux éléments de cette page web et les modifier avec le langage Javascript.

##II- Manipulation de DOM:

###1- S'assurer que le DOM est chargé :
Le cycle de vie d'une page HTML comporte trois événements importants:


-    **DOMContentLoaded-** le navigateur est entièrement chargé en HTML et l'arborescence DOM est construite, mais les ressources externes telles que les images <img>et les feuilles de style ne sont peut-être pas encore chargées.

- **load** - non seulement le HTML est chargé, mais également toutes les ressources externes: images, styles, etc.

- **beforeunload/unload** - l'utilisateur quitte la page.

Chaque événement peut être utile:

- **DOMContentLoaded event** - DOM est prêt, le gestionnaire peut donc rechercher des nœuds DOM, initialiser l'interface.

- **load événement** - les ressources externes sont chargées, donc les styles sont appliqués, les tailles d'image sont connues, etc.

- **beforeunload événement** - l'utilisateur quitte: nous pouvons vérifier si l'utilisateur a enregistré les modifications et lui demander s'il souhaite vraiment partir.

- **unload** - l'utilisateur est presque parti, mais nous pouvons toujours lancer certaines opérations, comme l'envoi de statistiques


*load événement est déclenché lorsque la page entière est chargée, y compris toutes les ressources dépendantes telles que les feuilles de style et les images. Ceci est à l'opposé de DOMContentLoaded, qui est déclenché dès que le DOM de la page a été chargé, sans attendre la fin du chargement des ressources.*

Il faut s'assurer que le Dom est bien chargé, pour ce faire on a deux méthodes :
La première méthode est :
``` javascript
window.addEventListener("DOMContentLoaded", function () {
    console.log("DOM CHARGÉ");
});
```
La deuxième est :
``` javascript
window.onload = function() {
console.log('page is fully loaded');
};
```
*Remarque*: 

Il faut toujours écrire la balise de javascript à la fin de la balise body et non pas la balise head.

### 3- Sélectionner un élément grace à son ID:

Le document est l'objet auquel on a directement accès dans notre code javascript. Le document est le point de départ de DOM.
Pour chercher ou récupérer un élément grace à son Id on a la méthode :

>document.getElementById()

#### Syntaxe:
``` javascript
let element = document.getElementById(id);
```
getElementById(<id>) prend en paramètre l' id  de l'élément que vous recherchez et vous retournera cet élément s'il a été trouvé.

Par exemple :

 Si l'on part du code HTML suivant : 
``` html 
<p id="my-anchor">My content</p> ,
```
On pourra trouver cet élément avec le code JavaScript suivant :
``` javascript
const myAnchor = document.getElementById('my-anchor');
``` 
###4- Sélectionner un élément grace à sa classe :
**getElementByClassName()**: Cette méthode fonctionne de la même manière que la précédente, mais fera sa recherche sur la  class  des éléments et retournera la liste des éléments qui correspondent.

**getElementsByClassName(<classe>)** prend en paramètre la classe des éléments à rechercher et vous retournera une liste d'éléments correspondants.

#### Syntaxe:
``` javascript
let elements = document.getElementsByClassName(names)
```
#### Exemple :

 Si l'on part du code HTML suivant :

``` html 
<div>
    <div class="content">Contenu 1</div>
    <div class="content">Contenu 2</div>
    <div class="content">Contenu 3</div>
</div>
``` 
On pourra retrouver la liste des éléments ayant la classe content avec le code JavaScript suivant 
``` javascript
const contents = document.getElementsByClassName('content');
const firstContent = contents[0];
```

### 5- Sélectionner un ou des éléments : 

La méthode **querySelector()** de l'interface Document retourne le premier Element dans le document correspondant au sélecteur - ou groupe de sélecteurs - spécifié(s), ou null si aucune correspondance n'est trouvée.

Si vous avez besoin d'une liste de tous les éléments correspondant aux sélecteurs spécifiés, vous devez utiliser **querySelectorAll()** à la place.
#### Syntaxe:

``` javascript

let element = document.querySelector(sélecteurs)
``` 

- Pour sélectionner un élément grace à son Id :On ajoute  ~ # ~


- Pour sélectionner un élément grace à sa classe :On ajoute ~.~

#### Exemple:

``` javascript

let el = document.querySelector(".maclasse");

```
### 6- Manipuler le style d'un élément :

Avec la propriété style, vous pouvez récupérer et modifier les différents styles d'un élément.
 Mais avant de modifier le style, on doit récupérer notre element.
####    Exemple :
``` javascript
let div = document.getElementById("div1");
div.style.marginTop = ".25cm";
```
``` javascript
elt.style.color = "#fff";      // Change la couleur du texte de l'élément à blanche
elt.style.backgroundColor = "#000"; // Change la couleur de fond de l'élément en noir
elt.style.fontWeight = "bold"; // Met le texte de l'élément en gras
```
``` javascript
const square=document.querySelector(.square);
square.style.backgroungColor='yellow';
```
#### La méthode setInterval :

On peut aussi dynamiser grace à une méthode **setInterval**

La méthode setInterval() permet d’exécuter une fonction ou un bloc de code en l’appelant en boucle selon un intervalle de temps fixe entre chaque appel.

Cette méthode va prendre en arguments le bloc de code à exécuter en boucle et l’intervalle entre chaque exécution exprimé en millisecondes.


#####Syntaxe:
``` javascript
setInterval(function, milliseconds, param1, param2, ...)
```

``` javascript
setInterval(function(){  }, timeout:);
```
``` html 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<script>
		function changeimage() {
			document.getElementById("serie").src = tabimages[numimage];
			if (numimage == 2) numimage = 0;
			else numimage++;
		}
		var tabimages = new Array("hopper1.jpg", "hopper2.jpg", "hopper3.jpg");
		var numimage = 1;
		</script>
	</head>
	<body>
		<img src="hopper1.jpg" id="serie">
		<script>
		setInterval(changeimage, 1000);
		</script>
	</body>
</html>
``` 

### 7- Manipuler les classes d'un élément :

La méthode setInterval nous permet d’exécuter une fonction de manière répétée, en commençant après l’intervalle de temps, puis en répétant continuellement à cet intervalle.

Pour changer des classes dans un élément, il faut toujours récupérer l'élément puis faire la modification.

#### Exemple : 

``` javascript
const square = document.query.Selector('.square');//Pour récupérer mon élément
console.log(square);//on affiche dans la console pour vérifier qu'on a bien récupéré notre élément
let i=0;//j'ai initialisé le i
setInterval( function(){
if(i%2){ 
square.classList.add('bigsquare');//pour accéder à ala classe on a classList et pour chnager le bigsquare
}else{
square.classList.remove('bigsquare');
}
i++//incrémenter +1
},timeout=1000);//temps en milliseconde.
```
###8- Créer et ajouter au Dom un élément :

####a- Créer un élément : 

**document.createElement** : la méthode document.createElement() crée un élément HTML du type spécifié par tagName
##### Syntaxe:
``` javascript
let element = document.createElement(tagName[, options]);
```
 Elle prend en paramètre le nom de la balise de notre élément et nous renvoie l'élément nouvellement créé

#####Exemple : 
``` javascript
const newElt = document.createElement("div");
``` 
#### b - Ajouter des enfants : 
**appendChild**:  Cette fonction permet d'ajouter un élément à la liste des enfants du parent depuis lequel la fonction est appelée.

#####Exemple : 
``` javascript
const newElt = document.createElement("div");
let elt = document.getElementById("main");

elt.appendChild(newElt);
```
``` javascript
/ Crée un nouvel élément paragraphe et l'ajoute à la fin du corps du document
let p = document.createElement("p");
document.body.appendChild(p);
```
#### c- Remplacer et supprimer des éléments : 

Il existe les fonctions **removeChild** et **replaceChild**, afin de respectivement supprimer et remplacer un élément

##### Exemple : 

``` javascript
const newElt = document.createElement("div");
let elt = document.getElementById("main");
elt.appendChild(newElt);

elt.removeChild(newElt);    // Supprime l'élément newElt de l'élément elt
elt.replaceChild(document.createElement("article"), newElt);    // Remplace l'élément newElt par un nouvel élément de type article
```

###9- Ecouter les événements de DOM : 

**Un événement** est une réaction à une action émise par l'utilisateur, comme le clic sur un bouton ou la saisie d'un texte dans un formulaire.

Un événement en JavaScript est représenté par un nom (click , mousemove ...) et une fonction que l'on nomme une  callback . Un événement est par défaut propagé, c'est-à-dire que si nous n'indiquons pas à l'événement que nous le traitons, il sera transmis à l'élément parent, et ainsi de suite jusqu'à l'élément racine.

**addEventListener()** :Cette fonction nous permet d'écouter tous types d'événements (pas que le clic).

**addEventListener(<event>, <callback>)** :prend en paramètres le nom de l'événement à écouter , et la fonction à appeler dès que l'événement est exécuté.

#### Exemple : 
``` javascript

const elt = document.getElementById('mon-lien'); // On récupère l'élément sur lequel on veut détecter le clic
elt.addEventListener('click', function() {  // On écoute l'événement click
elt.innerHTML = "C'est cliqué !"; // On change le contenu de notre élément pour afficher "C'est cliqué !"
});
``` 

____________________
<a id="liens"></a>
Liens des vidéos:

1- [S'assurer que le dom est chargé](https://youtu.be/xlYXBUe3SAQ)

2- [Sélectionner un élément grâce à son id](https://youtu.be/eHDOuEVDU48)

3- [Sélectionner des éléments grâce à leurs classes](https://youtu.be/Wx2DEhYDBMM)

4- [Sélectionner un ou des éléments](https://youtu.be/FHQ5s5M9J4U)

5- [Manipuler le style d'un élémen](https://youtu.be/FSig5e2c2o8)

6-[Manipuler les classes d'un élément](https://youtu.be/lZWrLJt_Eag)

7- [Créer et ajouter au dom un élément](https://youtu.be/BWci30RFuoo)

8-[Manipuler le dom : Écouter les évènements du dom](https://youtu.be/udEhxZrNMUw)
