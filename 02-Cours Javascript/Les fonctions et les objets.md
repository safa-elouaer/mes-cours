# Les fonctions et les Objets

## I-Les fonctions:

### 1-Déclaration des fonctions:
La déclaration d'une fonction est construite avec le mot-clé function, suivi par :

- Le nom de la fonction.

- Une liste d'arguments à passer à la fonction, entre parenthèses et séparés par des virgules.

- Les instructions JavaScript définissant la fonction, entre accolades, { }.
####Syntaxe:
``` javascript
function Nom_De_La_Fonction(argument1, argument2, ...) {
liste d'instructions
}
```
### 2- fonction anonyme:

Une fonction anonyme de la même façon qu’une fonction classique, en utilisant le mot clef function mais en omettant le nom de la fonction après.

#### Exemple : 
``` javascript
let helloWorld = function(){ // cette fonction est anonyme
alert('Hello World !');
}; // attention au point-virgule, il ne faut jamais l'oublier !

helloWorld(); // affichera une alerte "Hello World !"
```
### 3- Les fonctions callback:

Une fonction de rappel (aussi appelée callback en anglais) est une fonction passée dans une autre fonction en tant qu'argument, qui est ensuite invoquée à l'intérieur de la fonction externe pour accomplir une sorte de routine ou d'action

#### Exemple :
``` javascript
function calculer(nb1, nb2, callback) {
let sum = parseFloat(nb1) + parseFloat(nb2)

    // Je regarde si callback est différent
    if(callback !== undefined) {
        callback(sum);
    }
}

// Je lance mon calcule
calculer(2, 3, function(sum) {
alert(sum);
// Mon alert ne va se déclencher qu'une fois le calcule terminé
});
``` 
###4- L'objet argument : 

L'objet de l'argument contient un tableau des arguments utilisés lorsque la fonction a été appelée (appelée).

#### Syntaxe : 
>arguments

Vous pouvez accéder aux arguments d'une fonction à l'intérieur de celle-ci en utilisant l'objet arguments. Cet objet contient une entrée pour chaque argument passé à la fonction, l'indice de la première entrée commençant à 0
``` javascript
arguments[0]
arguments[1]
arguments[2]
``` 
### 5- Une fonction auto-Invoquée : 
Les expressions de fonction peuvent être faites "auto-invocation".

Une expression de l'auto-invocation est invoqué (commencé) automatiquement, sans être appelé.

Les expressions de fonction exécutera automatiquement si l'expression est suivie par () .

Vous ne pouvez pas auto-invoquer une déclaration de fonction.

Vous devez ajouter des parenthèses autour de la fonction pour indiquer qu'elle est une expression de fonction

#### Exemple : 
``` javascript
(function () {
var x = "Hello!!";      // I will invoke myself
})();
``` 

## II- Les Objets : 
Les objets en JS sont des concepts relativement simple. Un objet vous permettra de stocker des pair clé valeur ainsi que des méthodes (des functions).


### 1- Déclarer un objet : 
Ce sont des séries de paires clés/valeurs séparées par des virgules, entre des accolades. Les objets peuvent être enregistrés dans une variable :
##### Exemple :
````js 
let myBook = {
title: 'The Story of Tau',
author: 'Will Alexander',
numberOfPages: 250,
isAvailable: true
};
````
Exemple 2 :
````javascript

var bookArray = ["Big Brother", "1984", "Peter Pan"];

var myBox = {
height: 6,
width: 10,
length: 730,
meterial : "cardboard",
content : bookArray
};
````
### 2-Accédez aux données d'un objet
Maintenant que vous savez comment créer un objet en JavaScript, voyons comment accéder aux données dans un objet avec la notation pointée (dot notation), expliquée ci-dessous.

Une fois qu'un objet est enregistré dans une variable, vous pouvez accéder à ses données comme dans l'exemple ci-dessous.
````js
let myBook = {
    title: "L'Histoire de Tao",
    author: "Will Alexander",
    numberOfPages: 250,
    isAvailable: true
};

let bookTitle = myBook.title;  // "L'Histoire de Tao"
let bookPages = myBook.numberOfPages  // 250
````
=>Pour cela, utilisez le nom de la variable qui contient l'objet, un point (  .  ), puis le nom de la clé dont vous souhaitez récupérer la valeur.

