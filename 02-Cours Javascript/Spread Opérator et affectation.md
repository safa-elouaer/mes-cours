# Spread Operator et affectation affectation par définition

## 1-Spread operator:
*Le spread operator*, aussi appelé opérateur de décomposition qui se présente ni plus ni moins sous la forme de points de suspension : ... permet de faire des copies de variables en un tour de main (il permet d'étaler les variables).

*par exemple*

``` javascript
let arr1=[1,2,3];
let arr2=[5,8,9];
let arr3=[...arr1, ...arr2];
\\Pour étaler les deux tablaux 
dans arr3 il suffit d'ajouter les ... 
```

Simple et efficace pour copier une variable. Ici je dis bien copier, c’est-à-dire que la variable d’origine et sa copie sont totalement indépendantes. Nous pouvons sans problème modifier l’une sans pour autant modifier l’autre. Si nous avions simplement utilisé le symbole =, la variable n’aurait pas été copiée

- Cet opérateur on peut l'utiliser aussi sur les objets.
## 2-Affectation par décomposition :

L'affectation par décomposition : permet d'extraire  des données d'un tableau ou d'un objet grâce à une syntaxe dont la forme ressemble à la structure du tableau ou de l'objet.

 #### Syntaxe: 

``` javascript
let a, b, rest;
[a, b] = [10, 20];
console.log(a); // 10
console.log(b); // 20

[a, b, ...rest] = [10, 20, 30, 40, 50];
console.log(a); // 10
console.log(b); // 20
console.log(rest); // [30, 40, 50]

``` 

 #### Description:
   
   Ces expressions utilisant des littéraux pour les objets ou les tableaux permettent de créer simplement des données regroupées. Une fois créées, on peut les utiliser de n'importe quelle façon, y compris comme valeur renvoyée par une fonction.

``` javascript
const x = [1, 2, 3, 4, 5];
 // On crée un "paquet" de données
const [y, z] = x; 
// On utilise l'affectation par décomposition
console.log(y); // 1
console.log(z); // 2
```

###  Affectation de décomposition dans un tableau:

*exemple*:

``` javascript
const toto = ["un", "deux", "trois"];
// sans utiliser la décomposition
const un = toto[0];
const deux = toto[1];
const trois = toto[2];

// en utilisant la décomposition
const [un, deux, trois] = toto;
```

**L'affectation par décomposition peut être effectuée sans qu'il y ait de déclaration directement dans l'instruction d'affectation**. Par exemple :
``` javascript
let a, b;
[a, b] = [1, 2];
console.log(a);  // 1
console.log(b);  // 2
```

**Valeur par défaut**

On peut définir une valeur par défaut au cas où la valeur extraite du tableau soit undefined. 

*Par exemple*

``` javascript
let a, b;

[a = 5, b = 7] = [1];
console.log(a); // 1
console.log(b); // 7
```
---------------------------------------------------

lien des vidéos:

1-Operator spread: https://www.youtube.com/watch?v=w-vmVE5rzJc&feature=youtu.be

2-Affectation par décomposition: https://www.youtube.com/watch?v=3rpp3wzJ4Tg&feature=youtu.be
