 # Les opérateurs dans Javascript
Un opérateur est un symbole qui va être utilisé pour effectuer certaines actions notamment sur les variables et leurs valeurs
l existe différents types d’opérateurs qui vont nous servir à réaliser des opérations de types différents. Les plus fréquemment utilisés sont :


*Les opérateurs arithmétiques 

*Les opérateurs d’affectation / d’assignation (=)

*Les opérateurs de comparaison 

*Les opérateurs d’incrémentation et décrémentation 

*Les opérateurs logiques 

*L’opérateur de concaténation 

*L’opérateur ternaire 

*l’opérateur virgule.


 <span style='color:#26B260'>1-Les opérateurs arithmétiques:</span>
Les opérateurs arithmétiques utilisent des valeurs numériques (variables ou littéraux) comme opérandes et renvoient une valeur numérique. Les opérateurs arithmétiques standard sont l'addition (+),
la soustraction (-), la multiplication (*), et la division (/).

 <span style='color:#E2EB96'>a-L'addition:</span> L'opérateur d'addition permet d'obtenir la somme des opérandes numériques ou bien la concaténation de chaînes de caractères.

<span style='color:#FF0000 '>*syntaxe* :</span> Opérateur : x + y

<span style='color:#E2EB96'>b- La multiplication:</span>l’opérateur * va nous permettre de multiplier les valeurs de deux variables


<span style='color:#E2EB96'>c- La Division:</span> L'opérateur / va nous permettre de diviser deux variables.


<span style='color:#E2EB96'>d-Modulo (reste d’une division euclidienne):</span> L'opérateur % correspond au reste entier d’une division euclidienne. Par exemple, lorsqu’on divise 5 par 3, le résultat est 1 et il reste 2 dans le cas d’une division euclidienne. Le reste, 2, correspond justement au modulo

<span style='color:#E2EB96'>e-	Exponentielle (élévation à la puissance d’un nombre par un autre):</span> L'opérateur ** à l’élévation à la puissance d’un nombre par un autre nombre. La puissance d’un nombre est le résultat d’une multiplication répétée de ce nombre par lui-même. 

<span style='color:#E2EB96'>f-	Les opérateurs d'affectations:</span> 

Les opérateurs d’affectation vont nous permettre, comme leur nom l’indique, d’affecter une certaine valeur à une variable.

Nous connaissons déjà bien l’opérateur d’affectation le plus utilisé qui est le signe =. Cependant, vous devez également savoir qu’il existe également des opérateurs « combinés » qui vont effectuer une opération d’un certain type (comme une opération arithmétique par exemple)
et affecter en même temps.
*Par exemple*:
+=:Additionne puis affecte le résultat;

%=:Calcule le modulo puis affecte le résultat
*exemple de syntaxe*:

![exemple d'affectation  en Js](https://www.pierre-giraud.com/wp-content/uploads/2019/05/javascript-operateur-affectation-assignation-combine.png)

<span style='color:#E2EB96'>g-La concaténation:</span>l’opérateur de concaténation est le signe +

<span style='color:#E2EB96'>h-L'incrémentation:</span> L'opérateur d'incrément ++ ajoute une unité à son opérande et renvoie une valeur

-Si l'opérateur est utilisé en suffixe (par exemple : x++), **il renvoie la valeur avant l'incrémentation**.

-Si l'opérateur est utilisé en préfixe (par exemple : ++x), **il renvoie la valeur après l'incrémentation**.
i- La décrémentation :L'opérateur de décrément --  soustrait une unité à son opérande et renvoie une valeur.


-Si l'opérateur est utilisé en **suffixe** (par exemple : x--), **il renvoie la valeur avant la décrémentation**.

-Si l'opérateur est utilisé en **préfixe** (par exemple : --x), **il renvoie la valeur après la décrémentation**.

<span style='color:#E2EB96'>I-Plus unaire:</span>L'opérateur unaire plus (+) précède son opérande, l'évaluation correspond à son opérande, converti en nombre si possible et si ce n'est pas déjà un nombre.

<span style='color:#26B260'>2-Les opérateurs logiques:</span>
on a 3 opérateurs logiques : l’opérateur logique « ET », l’opérateur logique « OU » et l’opérateur logique « NON ».

<span style='color:#E2EB96'> a-AND (ET):</span> L'opérateur <span style='color:#3364FF '>&& </span>Lorsqu’il est utilisé avec des valeurs booléennes, renvoie true si toutes les comparaisons sont évaluées à true ou false sinon




<span style='color:#E2EB96'> b-OR (OU):</span> L'opérateur <span style='color:#3364FF '> \\ </span> Lorsqu’il est utilisé avec des valeurs booléennes, renvoie true si au moins l’une des comparaisons est évaluée à true ou false sinon

<span style='color:#E2EB96'>c-NO (NON):</span> L'opérateur <span style='color:#3364FF '>! </span> Renvoie false si une comparaison est évaluée à true ou renvoie true dans le cas contraire


<span style='color:#26B260'>3-Les opérateurs de comapraison:</span>

*<span style='color:#3364FF '>*L'égalité stricte*:</span> L'opérateur === Permet de tester l’égalité en **termes** de valeurs et de **types**

*<span style='color:#3364FF '>*L'égalité simple*:</span> L'opérateur == Permet de tester l’égalité sur les valeurs

*<span style='color:#3364FF '>*L'inégalité stricle*:</span> L'opérateur !== Permet de tester la différence en valeurs ou en types


*<span style='color:#3364FF '>*L'inégalité simple*:</span> L'opérateur != Permet de tester la différence en valeurs


*<span style='color:#3364FF '>*Strictement supérieur*:</span> L'opérateur > Permet de tester si une valeur est strictement supérieure à une autre

*<span style='color:#3364FF '>*supérieur ou égale*:</span> L'opérateur >= Permet de tester si une valeur est supérieure ou égale à une autre

*<span style='color:#3364FF '>*Inférieur *:</span> L'opérateur < Permet de tester si une valeur est strictement inférieure à une autre

*<span style='color:#3364FF '>*Inférieur ou égale*:</span> L'opérateur <= Permet de tester si une valeur est inférieure ou égale à une autre

