# Les tableaux /Arrays

## <span style='color:#1A1DD0'>Arrays:</span> 
c'est une structure des données qui permet d'organiser des valeurrs numérotées.

Elle peut contenit tout types des données par exemple : des nombres , des strings , des objets ou d'autres tableaux contenant eux même des autres tableaux.

Toutes les valeurs dans un tableau sont accessibles grâce à leurs **index** et leurs positions dans le tableau.

**Dans Javascript les index commence toujours par 0**.

* Comme les chaines des caractères on pourra savoir la longueur d'un tableau en accédant à la propriété <span style='color:#D01A36'>.length</span>

Aussi l'index de dernier élément du tableau est égale à l'index de la longueur -1

### <span style='color:#E2EB96'>1- Créer un tableau=</span>
 Pour créer un tableau il faut mettre deux crochets [] et dans ces deux crochets on va mettre les valeurs qu'on va insérer dans le tableau , et chacunes de ces valeurs sont séparées par des **virgules**

>const arr=[1, 2, 3]

- pour afficher ce tableau : console.log(arr);
On peut aussi créer des tableau autrement:
cont arr= New Array(arrayLength:valeur);( dans les parenthèes il faut noter la longueur de notre tableau)

### <span style='color:#E2EB96'>2- Remplir notre tableau=</span>
Pour remplir notre tableau on a <span style='color:#D01A36'> .fill </span>

exemple:
``` javascript

const arr= New Array ().fill(true)
// (entre les parethèses on note les valeurs qu'on veut ajouter)
``` 

### <span style='color:#E2EB96'>3- Accéder à une valeur grace à son index=</span>

#### <span style='color:#1A1DD0'> Syntaxe</span> 
``` javascript
const arr =[ "Jean", "Sam"];

//Pour afficher la première valeur on note  :

console.log(arr[0]);

// Pour afficher la dernière valeur on note 

console.log(arr[arr.length-1]);
``` 
### <span style='color:#E2EB96'>4- insérer une valeur dans le tableau=</span>

à l'aide de <span style='color:#D01A36'> .push </span>

*exemple*
``` javascript
const products=[];
products.push(600);
```

### <span style='color:#E2EB96'>5- supprimer et remplacer une valeur dans le tableau=</span>

A l'aide de la méthode <span style='color:#D01A36'> splice </span> :La méthode splice() modifie le contenu d'un tableau en retirant des éléments et/ou en ajoutant de nouveaux éléments à même le tableau.On peut ainsi vider ou remplacer une partie d'un tableau.



#### *syntaxe*
``` javascript
arr.splice(start:0, deleteCount:1);//Je veux commencer à supprimer la première valeur, et je veux supprimer qu'une seile valeur.
```
*Pour remplacer une valeur par une autre

>arr.splice(start:,deleteCount:, items"l'élément que je voulais ajouter au lieu du dernier");

### <span style='color:#E2EB96'>6- Ajouter un élément dans un tableau=</span>

Si l’on souhaite ajouter un élément, deux possibilités :
``` javascript
let tableau = ['A', 'B'];
tableau[] = 'C';
console.log(tableau);
// La manière littérale, retourne ['A', 'B', 'C']
tableau.push('D');
console.log(tableau);
// La manière objet, Retourne ['A', 'B', 'C', 'D']
``` 

la méthode push() ajoute notre élément à la fin du tableau. Si vous souhaitez l’ajouter au début, vous pouvez utiliser unshift()

*exemple*
``` javascript
let tableau = ['A', 'B'];
tableau.unshift(22);
console.log(tableau);
// Retourne [22, 'A', 'B'];
```

### <span style='color:#E2EB96'>7- Supprimer un élément d'un tableau=</span>

- Si vous avez besoin de supprimer le dernier élément d’un tableau, **pop()**

*exemple*:
``` javascript
let tableau = ['A', 'B', 'C'];
tableau.pop();
console.log(tableau);
// Retourne ['A', 'B'];
``` 
- Même principe avec **shift()** pour supprimer le premier élément du tableau :
``` javascript
let tableau = ['A', 'B', 'C'];
tableau.shift();
console.log(tableau);
// Retourne ['B', 'C'];
```
### <span style='color:#E2EB96'>8- Trier un tableau=</span>

Il existe plusieurs méthodes de Array pour vous permettre de trier vos tableaux. Si vous souhaitez les classer par ordre alphabétique, utilisez sort() :

*exemple*:
``` javascript
let tableau = ['E', 'A', 'D'];
tableau.sort();
console.log(tableau);
// Retourne ['A', 'D', 'E']
```
