# Javascript


## <span style='color:#FF0000 '>1-Notions de bases: </span>

<span style='color:#26B260'>**Javascript**:</span>est un langage de programmation crééen 1995,  son objectif est de dynamiser les sites et d'ajouter des animation .
Le Javascript est un langage **haut niveau**.

<span style='color:#26B260'>**Polyfill**:</span>Un polyfill est un morceau de code ,utilisé pour fournir des fonctionnalités modernes sur les navigateurs plus
anciens qui ne le prennent pas en charge de manière native.(c'est un bout de code qui va ajouter des fonctionnalités à un ancien code)

<span style='color:#26B260'>**Transpiler**</span>:  transforme du code en code, dans un même langage, par exemple JavaScript ES6 en JavaScript ES5, o
u du JavaScript non optimisé en JavaScript optimisé.

<span style='color:#26B260'>**Typage dynamique**:</span> Cela signifie qu'il n'est pas nécessaire de déclarer le type d'une variable avant de l'utiliser. Le type de la variable sera automatiquement déterminé lorsque le programme sera exécuté. Cela signifie également que la même variable pourra
avoir différents types au cours de son existence.

<span style='color:#26B260'>**Type des données**:</span>Le type d'une variable ou d'une constante est tout simplement le genre des
données qu'elle enregistre.  En JavaScript on a 7 types de données primitifs:

1- <span style='color:#E2EB96'>*Boolean*:</span>Les booléens sont des variables qui sont soient <span style='color:#FF0000 '> vraies </span> soient <span style='color:#FF0000 '>fausses.</span>
Elles ne peuvent donc que prendre ces deux valeurs.

2- <span style='color : #E2EB96'>*Null* :</span> Null est une valeur qui ne représente rien. C'est nous même qui l'attribuons à une variable, c'est donc en quelque sorte une absence
intentionnelle de valeur pour une variable

3- <span style='color : #E2EB96'>*Undefined* :</span>undefined veut dire qu'une variable à été défini, mais cette variable n'a aucune valeur.

Alors c'est différent de null, quand on déclare une variable, Javascript lui affecte automatiquement undefined
si on n'associe pas à cette variable une valeur.

4- <span style='color : #E2EB96'>*Number*:</span>représente les<span style='color:#FF0000 '> nombres </span>comme nous les connaissons que ce soit des entiers ou des décimales, c'est aussi simple que ça. En Javascript, nous n'avons pas besoin de spécifier si notre nombre est un entier
ou décimal, le langage le traite automatiquement.


5- <span style='color : #E2EB96'>*String* (chaines de caractères):</span> sont une suite de caractère, comme ce texte par exemple.

Pour créer une chaines de caractères, il faut toujours le mettre dans des apostrophes doubles

6- <span style='color : #E2EB96'>*Symbol* </span>(type introduit avec ECMAScript 6):
Un Symbol est un type de données unique et immuable.

Un symbol est un nouveau type de données primitif apparu avec ES6. Sa caractéristique principale est que chaque symbole est totalement unique, contrairement aux autres types
de données qui peuvent être écrasés, donc modifiés.

7- <span style='color : #E2EB96'>*Object*:</span> Objet, peut contenir plusieurs variables de type differents

# <span style='color : #FF0000 '>2-Les variables et les constantes en Javascript:</span>

<span style='color:#E2EB96'>a- Les variables:</span> Une variable est un conteneur servant à stocker des informations de manière temporaire,
comme une chaine de caractères (un texte) ou un nombre par exemple.

**Le propre d’une variable est de pouvoir varier, c’est-à-dire de pouvoir stocker différentes valeurs au fil du temps et c’est cette particularité qui les rend si utiles.**

<span style='color : #FF3371'>*Déclaration d'une variable* : </span>Pour déclarer une variable en JavaScript, on utilise le mot <span style='color:#FF0000 '>let </span>suivi du nom qu’on souhaite donner à notre variable.

Concernant le nom de nos variables, nous avons une grande liberté dans le nommage de celles-ci mais il y a quand même quelques règles à respecter :


- Le nom d’une variable doit obligatoirement commencer par une lettre ou un underscore (_) et ne doit pas commencer par un chiffre ;

- Le nom d’une variable ne doit contenir que des lettres, des chiffres et des underscores mais pas de caractères spéciaux ;

- Le nom d’une variable ne doit pas contenir d’espace.

=> Aussi il existe des noms « réservés » en JavaScript il ne faut pas les utiliser comme variables.

~On utilise généralement la convention lower camel case pour définir les noms de variable en JavaScript. Cette convention stipule simplement que lorsqu’un nom de variable est composé de plusieurs mots, on colle les mots ensemble en utilisant une majuscule pour chaque mot sauf le premier. Par exemple, si je décide de nommer une variable « monage » j’écrirai en JavaScript let monAge ou var monAge~

Pour initialiser une variable, on utilise l’opérateur<span style='color : #FF3371'> = </span> qui est dans ce cas non pas un opérateur d’égalité mais un opérateur d’assignation ou d’affectation

<span style='color : #FF0000 '>*La différence entre déclarer, initialiser et assigner*</span>

Lorsqu’on assigne une valeur pour la première fois à une variable, c’est-à-dire lorsqu’on stocke une valeur dans une variable qui n’en stockait pas encore, on dit également qu’on initialise une variable

<span style='color : #E2EB96'>Assigner </span> c'est affecter une nouvelle valeur dans une variable déjà initialisée, on va se contenter d’utiliser à nouveau l’opérateur d’affectation =.

En faisant cela, la nouvelle valeur va venir écraser l’ancienne valeur stockée qui sera alors supprimée.

<span style='color:#FF3371'>*Exemple*:</span>

![déclaration de variable en Js](https://www.pierre-giraud.com/wp-content/uploads/2019/05/javascript-variable-changement-valeur.png)

<span style='color : #E2EB96'>b-Les constantes :</span>Une constante est similaire à une variable au sens où c’est également un conteneur pour une valeur. Cependant, à la différence des variables, on ne va pas pouvoir modifier la valeur d’une constante.

<span style='color:#FF3371'>Déclaration d'une constante :</span> Pour créer ou déclarer une constante en JavaScript, nous allons utiliser le mot clef const.

On va pouvoir déclarer une constante exactement de la même façon qu’une variable à la différence qu’on va utiliser const à la place de let.

<span style='color:#FF3371'>*Exemple*:</span> 

![déclaration d'une constante en Js](https://www.pierre-giraud.com/wp-content/uploads/2019/05/javascript-creation-declaration-constante.png)

#<span style='color : #FF0000 '> 3- Ou écrire le code Javascript?:</span>
On va pouvoir placer du code JavaScript à trois endroits différents :

*Directement dans la balise ouvrante d’un élément HTML 

*Dans un élément script, au sein d’une page HTML 

*Dans un fichier séparé contenant exclusivement du JavaScript et portant l’extension .js.
````html
<span style='color:#FF0000 '>Important</span>


<span style='color : #3364FF '> 
console.log(ce qu'on veut afficher )</span>////Pour afficher dans la console 

 <span style='color : # 364FF '>  </span>//Les instructions sont limitées par des points virgules;

````
