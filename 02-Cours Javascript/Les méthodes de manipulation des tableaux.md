# Les méthodes de manipulation des tableaux

On a plusieurs méthodes pour manipuler les tableaux :
on a:

 >.forEach()
> 
> .find()
> 
> .findIndex()
> 
> .map()
> 
> .reduce()
> 
> .filter()
> 
> .some()
> 
> every()

## <span style='color:#1A1DD0'>1-forEach:</span>

La méthode forEach() permet d'exécuter une fonction donnée sur chaque élément du tableau.
forEach prend en paramètre une fonction callback, et l’execute en lui passant chaque élément de la collection.

La méthode forEach et la fonction callback acceptent des paramètres supplémentaires :

![img.pnj](https://www.hugopich.com/wp-content/uploads/2018/10/for-each-syntax-2-1-2.png)


*forEach*  n’est pas conçu pour produire une valeur de retour. La méthode retourne **undefined**.
Elle n’est donc pas chainable avec d’autres méthodes JavaScript qui arriveraient après forEach.

- Pas moyen de sortir de la boucle avant la fin, ni avec un break ni avec un return dans le callback.
Pas moyen d’utiliser continue, mais un return peut faire l’affaire.
  
*exemple*:

Itérer sur un Array, avec une fonction anonyme, est très simple :

``` javascript
const array = [1, 5, 10];
array.forEach(function(currentValue) {
console.log(currentValue);
});
```

## <span style='color:#1A1DD0'>2-find:</span>

- **.find()** est une méthode offerte par l'objet Array (comme .forEach()) qui prend en argument une fonction, le prédicat, qui va s'appliquer sur tous les éléments du tableau. Le prédicat doit retourner une valeur booléenne (true ou alors false) qui indique si l'élément que l'on recherche dans le tableau est le bon ! Lorsque le prédicat retourne donc true l'élément du tableau passé au prédicat est celui qui est retourné dans la variable


La méthode find() renvoie la valeur du premier élément trouvé dans le tableau qui respecte la condition donnée par la fonction de test passée en argument.
Sinon, la valeur undefined est renvoyée.


### <span style='color:#F00'>Description:</span>

- La méthode find exécute la fonction callback une fois pour chaque élément présent dans le tableau jusqu'à ce qu'elle retourne une valeur vraie (qui peut être convertie en true). Si un élément est trouvé, find retourne immédiatement la valeur de l'élément. Autrement, find retourne undefined. La méthode callback est seulement appelée pour les index du tableau pour lesquels on dispose d'une valeur.

- La méthode callback est appelée avec trois arguments : la valeur de l'élément, l'index de l'élément, et l'objet correspondant au tableau traversé

### <span style='color:#F00'> syntaxe:</span>
``` javascript

arr.find(callback(element[, index[, tableau]])[, thisArg])
``` 

## <span style='color:#1A1DD0'>3-findIndex:</span>

- La méthode findIndex() renvoie l'indice du premier élément du tableau qui satisfait une condition donnée par une fonction. Si la fonction renvoie faux pour tous les éléments du tableau, le résultat vaut -1.

### <span style='color:#F00'>Description:</span>
La méthode findIndex exécute la fonction callback une fois pour chaque élément présent dans le tableau (le tableau est parcouru entre les indices 0 et length-1 compris) jusqu'à ce que callback renvoie une valeur vraie.

S'il existe un tel élément, findIndex renverra immédiatement l'indice de l'élément concerné. **Sinon, findIndex renverra -1**. À la différence des autres méthodes liées aux tableaux comme some(), callback est également appelée pour les indices du tableau pour lesquels aucun élément n'est défini.

callback possède trois arguments : la valeur de l'élément, l'indice de l'élément et l'objet Array qui est parcouru


### <span style='color:#F00'> syntaxe:</span>
``` javascript

arr.findIndex(callback(element, index, tableau)
``` 
### <span style='color:#F00'> Exemple:</span>

``` javascript

function findC(element) {
return element === 'C';
}
let tableau = ['A', 'B', 'C', 'D', 'C'];
tableau.findIndex(findC);
// Retourne 2, l'indice de notre premier C
```

## <span style='color:#1A1DD0'>4-.map():</span>

La méthode map() crée un nouveau tableau avec les résultats de l'appel d'une fonction fournie sur chaque élément du tableau appelant

### <span style='color:#F00'>Description:</span>

- Lorsqu'on utilise map, la fonction callback fournie en argument est exécutée une fois pour chacun des éléments du tableau, dans l'ordre du tableau. Chaque résultat de l'opération sur un élément sera un élément du nouveau tableau. La fonction callback est appelée uniquement pour les indices du tableau pour lesquels il y a des valeurs affectées (y compris si cette valeur est undefined)

### <span style='color:#F00'> syntaxe:</span>

``` javascript

array.map(callback)
```
en cas général on a 

``` javascript

array.map (fonction (currentValue, index, arr))
``` 

**Paramètres**: Cette méthode accepte deux paramètres mentionnés ci-dessus et décrits ci-dessous:

function (currentValue, index, arr): C'est un paramètre obligatoire et il s'exécute sur chaque élément du tableau. Il contient trois paramètres répertoriés ci-dessous:
- **currentValue**: C'est un paramètre obligatoire et il contient la valeur de l'élément actuel.
- **index**: C'est un paramètre facultatif et il contient l'index de l'élément courant.

- **arr**: C'est un paramètre facultatif et il contient le tableau.


### <span style='color:#F00'> Exemple:</span>

*exemple sur les tableaux*
``` javascript
let nombres = [1, 4, 9];
let doubles = nombres.map(function(num) {
return num * 2;
});
// doubles vaut désormais [2, 8, 18].
// nombres vaut toujours [1, 4, 9]
```
## <span style='color:#1A1DD0'>5-.reduce():</span>

- La reduce()méthode réduit le tableau à une valeur unique.

- La méthode exécute une fonction fournie pour chaque valeur du tableau (de gauche à droite). reduce()

- La valeur de retour de la fonction est stockée dans un accumulateur (résultat / total).


### <span style='color:#F00'>Description:</span>
reduce() exécute la fonction callback une fois pour chaque élément présent dans le tableau et ignore les éléments vides du tableau. La fonction callback utilise quatre arguments :

L'accumulateur (la valeur retournée par le précédent appel de la fonction callback), ou la valeur initiale s'il sagit du premier appel ;
la valeur de l'élément courant ;
l'index de l'élément courant ;
le tableau parcouru par la méthode.
La première fois que la fonction callback est appelée, valeurInitiale et valeurCourante peuvent correspondre à un ou deux éléments. Si valeurInitiale est fournie dans l'appel de reduce(), alors accumulateur sera égale à valeurInitiale et valeurCourante sera égale à la première valeur de la liste. Si valeurInitiale n'est pas fournie, alors accumulateur sera égale à la première valeur de la liste, et valeurCourante sera alors égale à la seconde.

Autrement dit, si valeurInitiale n'est pas fournie, reduce exécutera la fonction de rappel à partir de l'indice 1 et la première valeur du tableau (d'indice 0) sera utilisée pour valeurInitiale.



### <span style='color:#F00'> syntaxe:</span>

``` javascript

array.reduce(function(total, currentValue, currentIndex, arr), initialValue)
```

- Elle permet de transformer un tableau en une toute autre valeur en utilisant un accumulateur.

Elle prend deux arguments.

Le premier est **une fonction de callback** qui va s'exécuter sur tous les éléments du tableau un à un, en prenant 3 arguments (2 obligatoires et un optionnel) et en retournant toujours l'accumulateur. Le premier est ce fameux accumulateur. C'est une valeur persistante entre les différentes exécutions de la fonction qui va représenter ce que l'on va retourner à l'issue de la méthode .reduce(). Le second paramètre est **l'item courant du tableau** et le troisième (que je n'utilise pas dans l'exemple plus haut) est l'index de l'item dans le tableau.

Le dernier argument du .reduce() est **la valeur initiale de l'accumulateur**

### <span style='color:#F00'> Exemple:</span>

``` javascript
[0, 1, 2, 3, 4].reduce(function(accumulateur, valeurCourante, index, array){
return accumulateur + valeurCourante;
});
``` 
Dans ce cas La fonction callback sera appelée quatre fois, avec les arguments et les valeurs de retour de chaque appel suivant :

![img.png](../image/Capture reduce 1.PNG)

La valeur retournée par reduce() sera alors celle du dernier appel de la callback (ici 10).

Il est aussi possible d'utiliser une fonction fléchée au lieu d'une fonction classique. Le code suivant, par exemple, produit le même résultat que l'exemple précédent :

``` javascript
[0, 1, 2, 3, 4].reduce(
(accumulateur, valeurCourante) => accumulateur + valeurCourante;
);

``` 
Si on fournit une valeur initiale comme second argument à l'appel de reduce(), le résultat sera alors le suivant :

``` javascript
[0, 1, 2, 3, 4].reduce(function(accumulateur, valeurCourante, index, array){
  return accumulateur + valeurCourante;
}, 10);
```
![img.png](../image/img.png)

Ici, la valeur renvoyée par reduce() serait 20.
### <span style='color:#F00'> Exemple:</span>

``` javascript
var total = [0, 1, 2, 3].reduce((a, b)=> a + b,0);
// total == 6

```

## <span style='color:#1A1DD0'>6-.Filter():</span>

La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine qui remplissent une condition déterminée par la fonction callback.

filter() prend en argument un prédicat qui va tester tous les éléments du tableau et mettre dans un nouveau tableau les éléments qui match avec le prédicat, tout simplement. Le traitement se fait en une seule ligne.

### <span style='color:#F00'>Description:</span>


filter() appelle la fonction callback fournie pour chacun des éléments d'un tableau, et construit un nouveau tableau contenant tous les éléments pour lesquels l'appel de callback retourne true ou une valeur équivalente à true dans un contexte booléen. La fonction callback n'est utilisée que pour les éléments du tableau ayant une valeur assignée — les index supprimés ou pour lesquels il n'y a jamais eu de valeur ne sont pas pris en compte. Les éléments du tableau qui ne passent pas le test effectué par la fonction callback sont ignorés, ils ne sont pas inclus dans le nouveau tableau.

La fonction callback est appelée avec trois arguments :

la valeur de l'élément courant,
l'index de l'élément courant,
l'objet Array parcouru.

### <span style='color:#F00'>Syntaxe:</span>

``` javascript
arr.filter(callback); // callback(elementCourant[, index[, tableauEntier]])
```

### <span style='color:#F00'>Valeur de retour:</span>

Un nouveau tableau contenant les éléments qui respectent la condition du filtre. Si aucun élément ne respecte la condition, c'est un tableau vide qui est renvoyé.

### <span style='color:#F00'>Exemple:</span>
``` javascript
const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

const result = words.filter(word => word.length > 6);

console.log(result);
// expected output: Array ["exuberant", "destruction", "present"]
```
## <span style='color:#1A1DD0'>7-.some():</span>

La méthode some() teste si au moins un élément du tableau passe le test implémenté par la fonction fournie. Elle renvoie un booléen indiquant le résultat du test.

### <span style='color:#F00'>Description:</span>

La méthode some() exécute la fonction callback une seule fois pour chaque élément présent dans le tableau jusqu'à ce qu'elle en trouve un pour lequel callback renvoie une valeur équivalente à true dans un contexte booléen. Si un tel élément est trouvé, some() renvoie immédiatement true. Dans le cas contraire, some renvoie false. callback n'est invoquée que pour les indices du tableau auxquels des valeurs sont assignées ; elle n'est pas invoquée pour les indices qui ont été supprimés ou auxquels aucune valeur n'a jamais été assignée.

La fonction callback est invoquée avec trois paramètres : la valeur de l'élément, l'indice de l'élément et l'objet Array parcouru.

### <span style='color:#F00'>Syntaxe:</span>

``` javascript
array.some(function(currentValue, index, arr)
```
### <span style='color:#F00'>Valeur de retour:</span>

true si la fonction callback renvoie une valeur équivalente à true pour au moins un des éléments du tableau, sinon elle renvoie false.

### <span style='color:#F00'>Exemple:</span>
``` javascript
const array = [1, 2, 3, 4, 5];

// checks whether an element is even
const even = (element) => element % 2 === 0;

console.log(array.some(even));
// expected output: true
```

## <span style='color:#1A1DD0'>8-.Every():</span>

La méthode every() permet de tester si tous les éléments d'un tableau vérifient une condition donnée par une fonction en argument. Cette méthode renvoie un booléen pour le résultat du test.

La méthode every () exécute la fonction une fois pour chaque élément présent dans le tableau:

S'il trouve un élément de tableau où la fonction renvoie une valeur fausse , every () retourne faux (et ne vérifie pas les valeurs restantes)
Si aucun faux ne se produit, every () retourne vrai
### <span style='color:#F00'>Description:</span>
La méthode every exécute la fonction callback fournie sur chacun des éléments contenus dans le tableau jusqu'à ce qu'un élément pour lequel la fonction callback renvoie une valeur fausse (falsy value) soit trouvé. Si un tel élément est trouvé, la méthode every renvoie directement false. Sinon, si la fonction callback a renvoyé une valeur vraie pour tous les éléments, la méthode every renverra true. La fonction callback n'est appelée que pour les indices du tableau pour lesquels il existe des valeurs affectées. Elle n'est pas appelée pour les indices supprimés ou ceux qui n'ont jamais reçu de valeur.

callback est appelée avec trois arguments : la valeur de l'élément en cours de traitement, l'indice de l'élément dans le tableau et le tableau qui est parcouru

### <span style='color:#F00'>Syntaxe:</span>
``` javascript
array.every(function(currentValue, index, arr)
``` 
### <span style='color:#F00'>Exemple:</span>
``` javascript
const isBelowThreshold = (currentValue) => currentValue < 40;

const array1 = [1, 30, 39, 29, 10, 13];

console.log(array1.every(isBelowThreshold));
// expected output: true
```
____________________
<a id="liens"></a>
Liens des vidéos:
1. [La méthode ]
