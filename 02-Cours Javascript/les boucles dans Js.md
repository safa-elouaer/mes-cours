#  Les boucles dans Javascript

Les boucles permettent de rép éter des actions simplement et rapidement. Une boucle peut être vue comme une version informatique de « copier N lignes » ou de « faire X fois quelque chose »

## <span style='color:#E2EB96'> 1- Boucle for :</span>
Une boucle for répète des instructions jusqu'à ce qu'une condition donnée ne soit plus vérifiée. La boucle for JavaScript ressemble beaucoup à celle utilisée en C ou en Java. Une boucle for s'utilise de la façon suivante :

``` javascript
for ([expressionInitiale]; [condition]; [expressionIncrément])
  instruction
``` 
Voici ce qui se passe quand une boucle for s'exécute :


1- L'expression initiale expressionInitiale est exécutée si elle est présente. Généralement, on utilise cette expression pour initialiser un ou plusieurs compteurs dont on se servira dans la boucle. Il est possible d'utiliser des expressions plus complexes si besoin. Elle peut servir à déclarer des variables.

2- L'expression condition est évaluée, si elle vaut true, les instructions contenues dans la boucle sont exécutées. Si la valeur de condition est false, la boucle for se termine. Si la condition est absente, elle est considérée comme true.

3_ L'instruction est exécutée. Si l'on souhaite exécuter plusieurs instructions, on utilisera un bloc d'instructions ({ ... }) afin de les grouper.

4- Si elle est présente, l'expression de mise à jour expressionIncrément est exécutée.

5- On retourne ensuite à l'étape 2.

 ### <span style='color:#F00'> syntaxe:</span>
``` javascript
for (Initialisation; Condition; Incrémentation)
{
code à exécuter
}
``` 

### <span style='color:#F00'> Exemple:</span>

![img](../image/Capture boucle for.PNG)

## <span style='color:#E2EB96'> 2- Boucle While:</span>

- Le while est l’instructions de boucle la plus simple que l’on retrouve en JavaScript. Cette instruction ne prend qu’un seul argument : une condition. Tant que la condition retourne true, la boucle se poursuit.

- Toute variable à initialiser, valeur à actualiser, test à effectuer (hors celui de la condition) doivent s’effectuer avant ou dans le corps de la boucle.

- Si la condition n'est pas vérifiée, l'instruction instruction n'est pas exécutée et le contrôle passe directement à l'instruction suivant la boucle.

- Le test de la condition s'effectue avant d'exécuter instruction. Si la condition renvoie true (ou une valeur équivalente), instruction sera exécutée et la condition sera testée à nouveau. Si la condition renvoie false (ou une valeur équivalente), l'exécution s'arrête et le contrôle est passé à l'instruction qui suit while.

- Pour pouvoir utiliser plusieurs instructions dans la boucle, on utilisera une instruction de bloc ({ ... }) afin de les regrouper.

- Dans certaine situation, vous ne pouvez pas connaitre à l’avance le nombre total d’instructions à répéter, comme pour l’affichage des membres d’un site de rencontre. La boucle while est plus appropriée dans ce cas.

. Si le nombre de répétion (ou itération) est connu, utiliser la syntaxe for.
### <span style='color:#F00'> Syntaxe:</span>
``` javascript
while (condition vraie){
continuer à faire quelque chose
}
``` 
### <span style='color:#F00'> Exemple:</span>
*exemple 1:*

![img](../image/Capture boucle while.PNG)
*exemple 2:*

``` javascript
let n = 0;
let x = 0;
while (n < 3) {
n++;
x += n;
}
```

## <span style='color:#E2EB96'> 3- Boucle do while :</span>

L'instruction do...while permet de répéter un ensemble d'instructions jusqu'à ce qu'une condition donnée ne soit plus vérifiée. (NdT : littéralement « do...while » signifie « faire... tant que »)

Celle-ci est la petite sœur de la boucle while, son fonctionnement est sensiblement le même mais l’évaluation de la condition est inversée. La boucle s’exécute puis évalue la condition.

Si cette dernière retourne true alors la boucle continue, sinon, elle s’arrête. Si on tanspose les exemples du while, le premier exemple donne le même résultat, mais dans le second, la boucle s’exécutera une fois de plus.

### <span style='color:#F00'> Syntaxe:</span>

``` javascript
do
instruction
while (condition);
```
### <span style='color:#F00'> Description:</span>

instruction est exécutée au moins une fois avant que la condition soit vérifiée. Pour utiliser plusieurs instructions à cet endroit, on utilisera une instruction de bloc ({ ... }) pour regrouper différentes instructions. Si la condition est vérifiée, l'instruction est à nouveau exécutée. À la fin de chaque exécution, la condition est vérifiée. Quand la condition n'est plus vérifiée (vaut false ou une valeur équivalente), l'exécution de l'instruction do...while est stoppée et le contrôle passe à l'instruction suivante.

### <span style='color:#F00'> Exemple:</span>
*Exemple 1:*
``` javascript
let i = 0;
do {
i += 1;
console.log(i);
} while (i < 5);
```
*Exemple 2:*
![img](../image/Capture boucle do while.PNG)

## <span style='color:#E2EB96'> 4- Boucle if else :</span>

L'instruction if exécute une instruction si une condition donnée est vraie ou équivalente à vrai. Si la condition n'est pas vérifiée, il est possible d'utiliser une autre instruction.
### <span style='color:#F00'> Syntaxe:</span>

``` javascript

if (condition vraie) {
une ou plusieurs instructions;
}
```
Ainsi, si la condition est vérifiée, les instructions s'exécutent. Si elle ne l'est pas, les instructions ne s'exécutent pas et le programme passe à la commande suivant l'accolade de fermeture.
De façon un peu plus évoluée, il y a l'expression if...else
### <span style='color:#F00'> Syntaxe:</span>

``` javascript

if (condition vraie) {
instructions1;
}
else {
instructions2;
}
``` 

``` javascript
if (condition1)
instruction1
else if (condition2)
instruction2
else if (condition3)
instruction3
...
else
instructionN

```
### <span style='color:#F00'> Exemple:</span>
*Exemple 1:*
``` javascript
if (x > 50){
// faire quelque chose
} else if (x > 5) {
// faire autre chose
} else {
// faire encore autre chose
}
```
*Exemple 2:*
``` javascript
if (cipher_char == from_char) {
  result = result + to_char;
  x++;
} else {
  result = result + clear_char;
}
``` 
## <span style='color:#E2EB96'> 5- Switch :</span>

L’instruction switch va nous permettre d’exécuter un code en fonction de la valeur d’une variable. On va pouvoir gérer autant de situations ou de « cas » que l’on souhaite.

En cela, l’instruction switch représente une alternative à l’utilisation d’un if…else if…else.

Cependant, ces deux types d’instructions ne sont pas strictement équivalentes puisque dans un switch chaque cas va être lié à une valeur précise. En effet, l’instruction switch ne supporte pas l’utilisation des opérateurs de supériorité ou d’infériorité.

Dans certaines (rares) situations, il va pouvoir être intéressant d’utiliser un switch plutôt qu’un if…else if…else car cette instruction peut rendre le code plus clair et légèrement plus rapide dans son exécution.
### <span style='color:#F00'> Syntaxe:</span>

``` javascript

switch(expression)
{
case etiquette 1:
// code à exécuter bloc 1
break;
case etiquette 2:
// code à exécuter bloc 2
break;
default:
// code à exécuter si expression n’est vrai pour aucune des case 1 et 2
}
```
Voici comment cela fonctionne: d'abord nous avons une seule expression entre les parenthèses (le plus souvent une variable), qui est évaluée une fois.
La valeur de l'expression est ensuite comparée avec les valeurs pour chaque case dans la structure.
S'il y a une occurrence, le bloc de code associé à cette occurrence est exécuté.
L'instruction break, elle permet d'interrompre le déroulement du code quand la condition est réalisée.

Remarque : Utilisez break pour éviter l'exécution automatique du code dans la case suivant .
### <span style='color:#F00'> Description:</span>

Une instruction switch commence par évaluer l'expression fournie (cette évaluation ne se produit qu'une fois). Si une correspondance est trouvée, le programme exécutera les instructions associées. Si plusieurs cas de figure correspondent, le premier sera sélectionné (même si les cas sont différents les uns des autres).

Le programme recherche tout d'abord une clause case dont l'expression est évaluée avec la même valeur que l'expression d'entrée (au sens de l'égalité stricte. Si une telle clause est trouvée, les instructions associées sont exécutées. Si aucune clause case n'est trouvée, le programme recherche la clause optionnelle default et si elle existe, les instructions correspondantes sont exécutées. Si cette clause optionnelle n'est pas utilisée, le programme continue son exécution après l'instruction switch. Par convention, la clause default est utilisée en dernière mais cela n'est pas nécessaire.

L'instruction break peut optionnellement être utilisée pour chaque cas et permet de s'assurer que seules les instructions associées à ce cas seront exécutées. Si break n'est pas utilisé, le programme continuera son exécution avec les instructions suivantes (des autres cas de l'instruction switch).
### <span style='color:#F00'> Exemple:</span>

``` javascript
switch (expr) {
case "Oranges":
console.log("Oranges : 0.59 € le kilo.");
break;
case "Pommes":
console.log("Pommes : 0.32 € le kilo.");
break;
case "Bananes":
console.log("Bananes : 0.48 € le kilo.");
break;
case "Cerises":
console.log("Cerises : 3.00 € le kilo.");
break;
case "Mangues":
case "Papayes":
console.log("Mangues et papayes : 2.79 € le kilo.");
break;
default:
console.log("Désolé, nous n'avons plus de " + expr + ".");
}

console.log("Autre chose ?");
```
### Que se passe t'il si on oublie un Break?

Si on omet une instruction break, le script exécutera les instructions pour le cas correspondant et aussi celles pour les cas suivants jusqu'à la fin de l'instruction switch ou jusqu'à une instruction break


**L'instruction Break permet d'interrompre le déroulement d'une boucle et  la termine immédiatement**

**L'instruction continue permet d'interrompre le déroulement d'une boucle mais elle ne la termine pas immédiatement, elle la saute(L’opérateur break permet de sortir d’une boucle).**
## <span style='color:#E2EB96'> 6-Ternaire :</span>

L'opérateur (ternaire) conditionnel est le seul opérateur JavaScript qui comporte trois opérandes. Cet opérateur est fréquemment utilisé comme raccourci pour la déclaration de Instructions/if...else.

C'est un opérateur à trois opérandes. La condition est évaluée et une valeur est retounée.
### <span style='color:#F00'> Syntaxe:</span>

``` javascript

// Si condition true alors l'opérateur retourne valeur_si_true
// Si condition false alors l'opérateur retourne valeur_si_false
condition ? valeur_si_true : valeur_si_false ;

// Faire la différence avec cette structure qui ne retourne rien
if (condition) {...} else {...}
``` 
La bonne façon de l'utiliser c'est de l'utiliser pour ce qu'il est c'est à dire un opérateur et d'exploiter sa valeur de retour. Vous pouvez le faire dans une affectation. Vous en avez un exemple ci-dessous : si la condition est vraie alors la valeur 1 est affectée à x sinon c'est la valeur 0. Vous le trouverez aussi dans la condition d'une structure conditionnelle.

### <span style='color:#F00'> Exemple:</span>

``` javascript
let condition=true;

let x = condition ? 1 : 0 ;

alert(x);
```
On peut mettre plusieurs opérateurs ternaires en "cascade". Ca permet d'évaluer plusieurs conditions.

``` javascript
let nombre=200;

let  taille = nombre > 100 ? 'Très grand' : nombre > 10 ? 'Grand' : 'Petit';

alert(taille);
```
-----------------------------------

<a id="liens"></a>
Liens des vidéos:

Boucle if else : https://youtu.be/ocrlAWPfatc

ternaire: https://youtu.be/u-hl9u9vuf0

 Switch:  https://youtu.be/6HR9xP-o5Pg

boucle for:  https://youtu.be/cNd9PZe0yx4

boucle do  while: https://youtu.be/9hfptxKDlFw

boucle while:  https://youtu.be/XZcBJztx8Dc




