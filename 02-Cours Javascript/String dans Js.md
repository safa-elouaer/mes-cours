# Les chaines de caractères ou String
<span style='color:#E2EB96'>1- Syntaxe=</span>

Pour déclarer un e chaine de caractère on utilise la syntaxe **str**
exemple :
````js
let  str ='Mon test';
````
En JavaScript, vous pouvez envelopper vos chaînes entre des guillemets simples ou doubles.

<span style='color:#E2EB96'>2- Echappement des caractères=</span>
signifie que nous les marquons de manière à ce qu'ils soient reconnus comme partie intégrante du texte, et non pas comme symbole de code. Dans JavaScript, nous le faisons en mettant une barre oblique inverse juste avant le caractère
 car parfois dans notre texte on aura soit des citations avec des guillemets ou des appostrophes ce qui va nous poser des problèmes et des confusions entre les guillemets de la commande et celles du texte.

*exemple*
````js
let str='C\' est Mardi';
//(le slash back pour l'échappement des caractère) 
````
<span style='color:#1AD020'>*Quelques notations d'échappement :</span>
> \ ': citation simple
> 
> \n: ajouter un saut de ligne 
>
>\r: retour chariot

<span style='color : #E2EB96'>3- Concaténation=</span>
L'ajout des chaines des caractères entre elles par l'opérateur +.

<span style='color : #E2EB96'>4- La longueur de la chaine des caractères=</span>
La propriété length représente la longueur d'une chaine de caractères.

### <span style='color:#1A1DD0'>syntaxe : </span>
````js
str.length
````        

<span style='color : #E2EB96'>5- Mettre en majuscule une chaine des caractères=</span>

La méthode <span style='color:#D01A30'>toUpperCase() </span>retourne la valeur de la chaîne courante, convertie en majuscules.

### <span style='color:#1A1DD0'>syntaxe : </span>
````js
str.toUpperCase()
````
<span style='color:#E2EB96'>5- Mettre en miniscule une chaine des caractères=</span>

La méthode <span style='color:#D01A30'>toLowerCase() </span>retourne la chaîne de caractères courante en minuscules

### <span style='color:#1A1DD0'>syntaxe </span>:

````js
str.toLowerCase()
````

<span style='color : #E2EB96'>6- Accéder à un caractère grace à sa position dans une chaine des caractères=</span>

Pour accéder à un caractère il faut juste mettre str et mettre dans les crochets sa postion sachant qu'on commence par la postion a la position zéro

### <span style='color : #1A1DD0'>syntaxe : </span>
````js
str[1] // juste  change la valeur pour accéder aux caractères recherchés
````        
<span style='color:#E2EB96'>7- La position de string dans un autre string=</span>


La méthode indexOf() renvoie l'indice de la chaine de caractère recherché  au sein de la chaîne courante (à partir de indexDébut)


###<span style='color:#1A1DD0'>syntaxe </span>
````js
str.indexOf(valeurRecherchée)
````

