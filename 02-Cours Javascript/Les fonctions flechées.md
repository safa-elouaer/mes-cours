# Les fonctions fléchées
##1- Introduction:
Une expression de fonction fléchée (arrow function en anglais) permet d’avoir une syntaxe plus courte que les expressions de fonction et ne possède pas ses propres valeurs pour this, arguments, super, ou new.target.

Les fonctions fléchées sont souvent anonymes et ne sont pas destinées à être utilisées pour déclarer des méthodes.




Les fonctions fléchées nous permettent d'écrire une syntaxe de fonction plus courte.
## 2-Syntaxe:
``` javascript
([param] [, param]) => {
instructions
}

(param1, param2, …, param2) => expression
// équivalent à
(param1, param2, …, param2) => {
return expression;
}

// Parenthèses non nécessaires quand il n'y a qu'un seul argument
param => expression

// Une fonction sans paramètre peut s'écrire avec un couple
// de parenthèses
() => {
instructions
}
```
### Exemple : 
``` javascript
let a = [
"We're up all night 'til the sun",
"We're up all night to get some",
"We're up all night for good fun",
"We're up all night to get lucky"
];

// Sans la syntaxe des fonctions fléchées
let a2 = a.map(function (s) { return s.length });
// [31, 30, 31, 31]

// Avec, on a quelque chose de plus concis
let a3 = a.map( s => s.length);
// [31, 30, 31, 31]
```

##3-Description :

Deux facteurs sont à l’origine de la conception des fonctions fléchées : 

- une syntaxe plus courte et l’absence de this spécifique à la fonction. Sur ce dernier point, cela signifie qu’une fonction fléchée ne lie pas son propre **this** au sein de la fonction (il en va de même avec **arguments**, **super** ou **new.target**).



##4-Différence entre *this* dans la fonction fléchée et celle dans la fonction classique:

La manipulation de **this** est également différente dans les fonctions fléchées par rapport aux fonctions régulières.

En bref, avec les fonctions fléchées, il n'y a pas de liaison this.

Dans les fonctions régulières, le *this* mot clé représentait l'objet qui appelait la fonction, qui pouvait être la **fenêtre**, le **document**, un **bouton** ou **autre**.

Avec les fonctions fléchées, le *this* représente toujours l'objet qui a défini la fonction fléchée.

### Exemple :
``` javascript

function Personne () {
// Le constructeur Personne() définit `this` comme lui-même.
this.age = 0;

setInterval(function grandir () {
// En mode non strict, la fonction grandir() définit `this`
// comme l'objet global et pas comme le `this` defini
// par le constructeur Personne().
this.age++;
}, 1000);
}

let p = new Personne();

```
Avec ECMAScript 3/5, ce problème a pu être résolu en affectant la valeur de this à une autre variable :
``` javascript
function Personne () {
let that = this;
that.age = 0;

setInterval(function grandir () {
// La fonction callback se réfère à la variable `that`
// qui est le contexte souhaité
that.age++;
}, 1000);
}
```

Autrement, on aurait pu utiliser une fonction de liaison afin que la bonne valeur this soit passée à la fonction grandir.

Les fonctions fléchées ne créent pas de nouveau contexte, elles utilisent la valeur this de leur contexte. Aussi, si le mot-clé this est utilisé dans le corps de la fonction, le moteur recherchera la référence à cette valeur dans une portée parente. Le code qui suit fonctionne ainsi de la façon attendue car le this utilisé dans setInterval est le this de la portée de Personne :
``` javascript

function Personne () {
this.age = 0;

setInterval(() => {
this.age++;
// |this| fait bien référence à l'objet personne
}, 1000);
}

let p = new Personne();
```

