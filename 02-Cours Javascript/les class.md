# Les class
## Introduction 

En réalité, les classes sont juste des fonctions spéciales. Ainsi, les classes sont définies de la même façon que les fonctions : par déclaration, ou par expression.

## 1- La déclaration des classes:

Pour utiliser une déclaration de classe simple, on utilisera le mot-clé class, suivi par le nom de la classe que l'on déclare

## 2- Syntaxe:
``` javascript
class MyClass {
// class methods
constructor() { ... }
method1() { ... }
method2() { ... }
method3() { ... }
...
}
```

- Utilisez ensuite new MyClass()pour créer un nouvel objet avec toutes les méthodes répertoriées.

- La constructor()méthode est appelée automatiquement par new, nous pouvons donc y initialiser l'objet.

## 3-Propriétés par défaut :

Ces sont les propriétés qui sont déclarés et assignées dés le début du programme ; donc chaque nouvel objet va prendre les memes valeurs.


### 4-La méthode constructor():

La méthode constructor est une méthode spéciale qui permet de créer et d'initialiser les objet créés avec une classe. Il ne peut y avoir qu'une seule méthode avec le nom "constructor" pour une classe donnée

La méthode constructeur est une méthode spéciale:

- Il doit avoir le nom exact de "constructor"
- Il est exécuté automatiquement lors de la création d'un nouvel objet
- Il est utilisé pour initialiser les propriétés des objets

*exemple*:
``` javascript
class User {

constructor(name) {
this.name = name;
}

sayHi() {
alert(this.name);
}

}

// Usage:
let user = new User("John");
user.sayHi();
```
Quand new User("John")est appelé:

Un nouvel objet est créé.
Le constructors'exécute avec l'argument donné et lui assigne this.name.
… Ensuite, nous pouvons appeler des méthodes objet, telles que user.sayHi().
- La constructor()méthode est appelée automatiquement par new, nous pouvons donc y initialiser l'objet.

## 5- Héritage entre classes:


La classe enfant hérite de toutes les méthodes d'une autre classe.

L'héritage est utile pour la réutilisabilité du code: réutilisez les propriétés et les méthodes d'une classe existante lorsque vous créez une nouvelle classe.

Pour créer un héritage de classe, utilisez le **extends** mot - clé

**Remarque:** La super()méthode fait référence à la classe parente.



>La chose à savoir ici est que nous devons utiliser le mot clef super() qui permet d’appeler le constructeur parent dans le constructor() de notre classe fille afin que les propriétés soient correctement initialisées.

### Définition et utilisation du **Super**

Le super mot-clé fait référence à la classe parente.

Il est utilisé pour appeler le constructeur de la classe parent et pour accéder aux propriétés et méthodes du parent.

### Syntaxe de **Super**:
``` javascript
super(arguments);  // calls the parent constructor (only inside the constructor)
super.parentMethod(arguments);  // calls a parent method
```

*exemple:*
``` javascript
class Car {
constructor(brand) {
this.carname = brand;
}
present() {
return 'I have a ' + this.carname;
}
}

class Model extends Car {
constructor(brand, mod) {
super(brand);
this.model = mod;
}
show() {
return this.present() + ', it is a ' + this.model;
}
}

mycar = new Model("Ford", "Mustang");
document.getElementById("demo").innerHTML = mycar.show();
```

##6-Propriétés et méthodes static:

Le mot-clé static permet de définir une méthode statique d'une classe. Les méthodes statiques ne sont pas disponibles sur les instances d'une classe mais sont appelées sur la classe elle-même. Les méthodes statiques sont généralement des fonctions utilitaires (qui peuvent permettre de créer ou de cloner des objets par exemple)

### Syntaxe:

>static nomMéthode() { ... }
 ### Description :
 
 Les méthodes statiques sont utilisées lorsque la méthode ne s'applique qu'à la classe elle-même et pas à ses instances. Les méthodes statiques sont généralement utilisées pour créer des fonctions utilitaires.

### Exemple :
``` javascript
class Triple {
static triple(n) {
if (n === undefined) {
n = 1;
}
return n * 3;
}
}

class SuperTriple extends Triple {
static triple(n) {
return super.triple(n) * super.triple(n);
}
}

console.log(Triple.triple());       // 3
console.log(Triple.triple(6));      // 18
console.log(SuperTriple.triple(4)); // 144
var tp = new Triple();
console.log(SuperTriple.triple(4)); // 144 (pas d'impact de l'affectation du parent)
console.log(tp.triple()); // tp.triple n'est pas une fonction

``` 

____________________
<a id="liens"></a>
Liens des vidéos :

1-[Déclarer une Classe](https://youtu.be/9_NWpR6xg4I)

2- [Propriétés par défaut](https://youtu.be/x7j_0Tx9eGc)

3- [La méthode Constructor](https://youtu.be/7aJOq2NvFvA)

4-[Héritage entre les classes](https://youtu.be/1b8YR1auFcw)

5-[Ajouter des méthodes]( https://youtu.be/9RiPoY_9sPo)

6-[Propriétés et méthodes static](https://youtu.be/9sjSMVCAspY)
