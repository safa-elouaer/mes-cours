# Introduction

Mongodb est:
- un système de gestion de base de données **NoSQL** orientée document, crée en 2007 

- open-source
- disponibilité de plusieurs fonctionnalités SQL (COUNT, GROUP BY, ORDER BY, SUM...)
- possibilité d’accéder aux données via une console JavaScript
- des drivers disponibles pour plusieurs langages de programmation (Java, JavaScript,
  PHP, Python, Ruby...)
- données stockées sous format JSON (JavaScript Object Notation)
- SGBD NoSQL le plus populaire en 2017 (et le plus utilise en ´ France)
## SQL vs MongoDB


- Base = Base
- Table = Collection
- Enregistrement (tuple) = Document
- En BDR, tous les tuples d’une table ont les memes champs (mais les valeurs peuvent être différentes (les valeurs sont affectées à
des colonnes)
- Dans une collection MongoDB, les documents peuvent ne pas
avoir un champ partage (pas de colonnes dans un document ´
MongoDB).

=>MEAN: MongoDB ExpressJS Angular NodeJS

=> Avec ses 4 technologies totalement basées sur JavaScript il est possible de couvrir la totalité des composants nécessaires à la création de sites web dynamiques ou d'application web. Il est possible de développer un projet complet en JavaScript sur 3 niveaux :


- **Le niveau d'affichage** : avec Angular ou React

- **Le niveau d'application** : avec Express et Node JS

- **Le niveau de base de données** : avec mongoDB
  
## Mongoose 

![img](../image/img_75.png)

Comme vous pouvez le voir, Mongoose va servir de passerelle entre notre serveur Node.js et notre serveur MongoDB.

=>Mongoose est un module Node.js qui s'installe avec NPM (Node Package Manager).

## Présentation de Mongodb
- Mongodb est le plus popular de SGBD orienté document, un SGBD no SQL.
- Les bases de données SQL :utilise un langage de squelette structuré, ils ont un schéma prédéfini et ils sont relationnelles

- Les bases de données SQL sont basés sur les tables alors que les bases de données non SQL sont des magasins de documents de clé, valeur de graphiques ou de colonnes larges

## Présentation de mongoose

Mongoose est un packet de javascript qui permet de connecter notre application node.js avec une base de données Mongodb.

## Installation 
1- Allez sur le lien https://www.mongodb.com/try/download/community
(onglet Community server), choisissez la version 4.4.1, msi comme Package et Windows comme Platform

2- Installez le fichier téléchargé.

3- Sous la racine du disque dur (C:\ sous Windows), créez l’arborescence data\db : c’est l’arborescence par défaut qui sera cherchée par MongoDB.

4- Copiez le chemin absolu du repertoire ´ bin de MongoDB dans
Programmes files

5- Dans la barre de recherche, cherchez Système ensuite cliquez Paramètres système avancés

6- Choisissez Variables d’environnement ensuite dans Variables utilisateur cliquez sur Nouvelle

7- Saisissez comme nom de variable PATH et comme valeur le chemin absolu du repertoire ´bin de MongoDB dans Programmes files




--------------------
Lien de vidéo 

[001 | MongoDB - introduction](https://youtu.be/zqwAOjVwCFM)

[002 | MongoDB - présentation de mongodb ]( https://youtu.be/1CGmzHLxs2M)

[003 | MongoDB - présentation de mongoose](https://youtu.be/2MEXa_KttOY)

[004 | MongoDB - installation de mongodb]( https://youtu.be/VDPsAIrxY7M)