# Component cycle life

Chaque composant React passe par différentes étapes durant lesquelles il est possible d'intervenir. On appelle ça le cycle de vie d'un composant. Celui-ci se découpe en 3 parties :

**Mount** : le montage. Il intervient quand une instance du composant est créé dans le DOM.

**Update** : la mise à jour. Ce cycle de vie est déclenché par un changement d'état du composant.

**Unmount** : le démontage. Cette méthode est appelée une fois qu'un composant est retiré du DOM.

## Les différentes évènements : 

![img](../image/img_36.png)

### Mount
- constructor()
- statique getDerivedStateFromProps ()
- render()
- componentDidMount()
### constructor :
Comme dans la programmation objet, le constructeur est ce qui est appelé en premier. Il intervient dès que le composant doit apparaître dans le DOM virtuel. Votre constructeur de class reçoit en premier paramètre ses props. Si vous l'implémentez, il sera important de bien appeler votre class parente avec le mot-clé super afin de lui fournir les props.

C'est au niveau de votre constructeur que vous allez initialiser l'état de votre composant (le state). Il vous arrivera également de venir .bind() certaines méthodes pour garantir le this de certaines méthodes.

### render()
Cette méthode est celle du rendu. Nous l'avons déjà vu dans un cours précédent. Elle intervient pour rendre votre JSX dans le DOM virtuel (et donc générer le HTML). C'est à ce moment là que vous avez l'état de votre composant à jour. Que ce soit vos props ou votre state, vos données sont disponibles et prêtes à être manipulées afin de rendre ce que vous souhaitez.

### componentDidMount()
Enfin, dès que le composant a fini de se monter, cette méthode s'exécute. Le DOM est correctement chargé et cette fonction devient intéressante pour, par exemple, tenter de manipuler les éléments du DOM.
#### Exemple :
````jsx
class Items extends Component {
constructor(props) {
super(props)
console.log('[constructor]')
}

    componentDidMount() {
        console.log('[componentDidMount]')
    }

    render() {
        console.log('[render]')
        return <div>Liste d'items</div>
    }
}
````

=>
![img](../image/img_35.png)

#### Exemple 2 :
````jsx
class Compteur extends React.Component {

constructor(props) {
super(props);
console.log('1');
this.state = {ok: '1'};
}

static getDerivedStateFromProps(state){
console.log('2');
return state;
}

render() {
console.log('4');
return <div><h1>{this.props.name}</h1>
<p>{this.props.children}</p>
</div>
}

componentDidMount(){
console.log('4');
}
}
````
### Update
- **statique getDerivedStateFromProps ()**
- **shouldComponentUpdate(nextProps, nextState, nextContext)**

- **render()**
- **getSnapshotBeforeUpdate ()**
- **componentDidUpdate()**
  Le cycle de vie de la mise à jour s'active dans 2 cas :

- La modification du state, votre état local

- La modification d'une props de l'un de vos parents

Dès qu'il intervient un de ces 2 cas, votre composant va déclencher un re-render. Dès lors, il va engager les différentes méthodes que l'on a vu au dessus, puis re-rendre le JSX avec la mise à jour de son état.


### Unmount
Le démontage d'un composant intervient lorsque celui-ci disparaît du DOM

- componentWillUnmount()

----------------------------
Lien de vidéo 

[005. React - Component lifecycle](https://youtu.be/OxbJrk2uy_s)


