# Déclarer un composant
## Créer un composant :

Les composants React sont des éléments autonomes que vous pouvez réutiliser tout au long d'une page. En fabriquant de petits morceaux de code ciblés, vous pouvez déplacer et réutiliser des morceaux à mesure que votre application se développe.

=>Il existe deux types de composants personnalisés : par classe et fonctionnels. 
![img](../image/img_34.png)
###  Par des Class 
Tout d'abord, importez React et la classe de composant, puis exportez Instructions avec les lignes suivantes :
````jsx
import React from 'react';
class Compteur extends React.Component {

    render(){//Render va s’occuper de retourner un élément JSX.
        return <div><h1>{this.props.name}</h1>
            <p>{this.props.children}</p>
        </div>
    }
}

ReactDOM.render(<Compteur name="Safa">Safa est une élève</Compteur>, document.querySelector('#app'));
````
=> Une petite explication :
- Importer React permettra de convertir le JSX.
- Le nom de la classe doit être en majuscule et doit correspondre au nom du fichier. 
-  La méthode render() renvoie le code JSX que vous voulez afficher dans le navigateur.

De manière générale, **render()** contient ce qui va être affiché dans notre page HTML. => A noter que render() est obligatoire, vous ne pouvez pas déclarer un composant sans la définir.
### Par une fonction 

Le moyen le plus simple de définir un composant consiste à écrire une fonction JavaScript :
````jsx
function Welcome(props) {
return <h1>Bonjour, {props.name}</h1>;
}
````
=>Cette fonction est un composant React valide car elle accepte un seul argument « props » (qui signifie « propriétés ») contenant des données, et renvoie un élément React
#### Exemple : 
````jsx
function CompteurFn(props) {
console.log(props);
return <div> <h1>Compteur : <span>{props.name}</span></h1>
<p>{props.children}</p>
  </div>
}

ReactDOM.render(<Compteur name="Safa">Jean est un élève</Compteur>, document.querySelector('#app'));
````

----------------------
Lien de Vidéo:
[004. React - Declarer un composant ]( https://youtu.be/tQ8SP-YDae8)

