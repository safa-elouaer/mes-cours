# Keys list

## 1-Key :
Les clés aident React à identifier quels éléments d’une liste ont changé, ont été ajoutés ou supprimés. Vous devez donner une clé à chaque élément dans un tableau afin d’apporter aux éléments une identité stable.

### Exemple :
````jsx
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
  <li key={number.toString()}>
    {number}
  </li>
);
````
=>Le meilleur moyen de choisir une clé est d’utiliser quelque chose qui identifie de façon unique un élément d’une liste parmi ses voisins. Le plus souvent on utilise l’ID de notre donnée comme clé :

### Exemple : 
````jsx
const todoItems = todos.map((todo) =>
  <li key={todo.id}>
    {todo.text}
  </li>
);
````
Quand vous n’avez pas d’ID stable pour les éléments affichés, vous pouvez utiliser l’index de l’élément en dernier recours :

````jsx 
const todoItems = todos.map((todo, index) =>
  // Ne faites ceci que si les éléments n'ont pas d'ID stable
  <li key={index}>
    {todo.text}
  </li>
);
````
=>Nous vous recommandons de ne pas utiliser l’index comme clé si l’ordre des éléments est susceptible de changer. Ça peut avoir un effet négatif sur les performances, et causer des problèmes avec l’état du composant.

=>Les clés doivent être stables, prévisibles et uniques. Des clés instables (comme celles produites par Math.random()) entraîneront la re-création superflue de nombreuses instances de composants et de nœuds DOM, ce qui peut dégrader les performances et perdre l’état local des composants enfants.

---------------------------
Lien de vidéo :

[009. React - Lists key](https://youtu.be/Jd4OwxQlLiY)
