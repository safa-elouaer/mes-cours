# Affichage conditionnel 

L’affichage conditionnel en React fonctionne de la même façon que les conditions en Javascript.
Il faut tout d’abord garder en mémoire que JSX produit une expression JavaScript et non une instruction. Par conséquent, certaines parties du langage qui constituent des instructions, comme un  if  , un  for  ou une déclaration, n’y sont pas possibles.

L’astuce consiste à remplacer nos instructions classiques de branchement conditionnel par des expressions utilisant les opérateurs logiques (typiquement, &&  ,  ||  et l’opérateur ternaire  ? : ).
## 1- l'opérateur logique && :

Dans certains cas, plutôt que d'utiliser le mot clé if, il est plus pratique et plus concis de recourir à une condition à la volée (Inline If) avec l'opérateur &&.

On utilisera l’opérateur logique **&&** , avec la condition comme **opérande de gauche**, et la grappe JSX comme **opérande de droite**.

Selon la sémantique de base de JavaScript, si la condition échoue, l'opérande de droite ne sera pas évaluée, et vaudra au global **false**. 

JSX ignorera alors ce **false** et rien n’apparaîtra dans le DOM navigateur à cet endroit.
=>Dans JavaScript Moyen，true && expression : Toujours revenir expression，Et si false && expression :Toujours revenir **false**

=> Dans React :Si la condition est true，&& :L'élément à droite sera rendu Si oui et si false React Ignore et rien n’apparaîtra dans le DOM navigateur à cet endroit.

### Exemple : 

````jsx
class Compteur extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            nb:props.defaultValue,
        };
    }
    increment(){

        this.setState((state)=>{{nb:state.nb+1 }});
    };
    reset(){
        this.setState(()=>{
            return {nb:0};
        })
    }
    decrement (){
        this.setState((state)=>{
           if (state.nb>=1){
               return {nb:state.nb-1};
           }
           return 0;
        })
    }
   render() {
       let bntReset;
       if (this.state.nb>0){
           bntReset=<button>Réinitialiser</button>
       }
       return <div>
           <h1> {this.props.name}</h1>
       <p>{this.state.nb}</p>
           { (this.state.nb>=1)&& ( <button onClick={()=>this.reset()}>Décrementer</button>)}
       <button onClick={()=>this.reset()}>Incrementer</button>
           {bntReset}
       </div>
   }
}
   class Acceuil extends React.Component{

    render(){
      return <div>
          <CompteurFn name='Jean'></CompteurFn>
           <Compteur name='Sam'></Compteur>
       </div>
    }
}

 ReactDOM.render(<Acceuil/>, document.querySelector('#app'));
````
## 2- Le ternaire : 

Si nous voulons l’équivalent d’un **if…else…**, nous utiliserons plutôt **l’opérateur ternaire ~?… : ~**, qui est l’équivalent naturel sous forme d’expression.
>condition ? exprSiVrai : exprSiFaux 

### Exemple : 
````jsx

class Compteur extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            nb:props.defaultValue,
        };
    }
    increment(){

        this.setState((state)=>{{nb:state.nb+1 }});
    };
    reset(){
        this.setState(()=>{
            return {nb:0};
        })
    }
    decrement(){
        this.setState((state) => {
            return {
                nb: state.nb >= 1 ? state.nb - 1 : 0
            }
        })
    }

}
   render() 
{
       let bntReset;
       if (this.state.nb>0){
           bntReset=<button>Réinitialiser</button>
       }
       return <div>
           <h1> {this.props.name}</h1>
       <p>{this.state.nb}</p>
           { (this.state.nb>=1)&& ( <button onClick={()=>this.reset()}>Décrementer</button>)}
       <button onClick={()=>this.reset()}>Incrementer</button>
           {bntReset}
       </div>
   
}
   class Acceuil extends React.Component{

    render(){
      return <div>
          <CompteurFn name='Jean'></CompteurFn>
           <Compteur name='Sam'></Compteur>
       </div>
    }
}

 ReactDOM.render(<Acceuil/>, document.querySelector('#app'));
````
 *Exemple 2* : 
````jsx
render()
{
  const isLoggedIn = this.state.isLoggedIn;
  return (
    <div>
      L’utilisateur <b>{isLoggedIn ? 'est actuellement' : 'n’est pas'}</b> connecté.
    </div>
  );
}
````
*Exemple 3* :
````jsx
<p>{user.loggedIn ? <WelcomeLabel /> : <LoginLink />}</p>

<p>{user.admin ? (
  <a href="/admin">Faire des trucs de VIP</a>
) : (
  <a href="/request-admin">Demander à devenir VIP</a>
)}</p>
````

---------------------------------
Lien de Vidéo 
[008. React - Affichage conditionnel ](https://youtu.be/W1LcnwIvjxQ)