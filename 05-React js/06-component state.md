# Component state 

Les composants sont comme des fonctions JavaScript. Or, dans l'utilisation d'une fonction, on reçoit généralement au moins un paramètre. C'est le cas pour nos composants React, vous recevez ce que l'on appelle des props. A la fin, nous retournons des éléments React qui décrivent ce qui doit apparaître à l’écran.

## Props et state : 
Une **props** ou un **state**, les 2 correspondent aux propriétés d'un composant : ce qui vont lui permettre d'avoir des informations sur ce qu'il est. La différence fondamental entre les 2 sont leur provenance :

=>**Les props** proviennent de l'extérieur du composant

=>**Le state** est une "propriété", son état local
### 1- Les props :

Une props est une propriété que l'on passe à un composant. C'est donc une information qui vient de "l'exterieur" du composant, généralement, de son parent direct (mais pas toujours).

Les props d’un composant sont sous forme d’un objet qui contient des informations sur ce composant. Pour voir les props de ce dernier on utilise l’expression : **this.props**.


=>En règle générale, les props sont “immuable’”, donc ne doivent pas être changés.

**Exemple** :
````jsx
class Hello extends React.Component {
render() {
return(
<div>
<h1>{this.props.text}</h1>
</div>
);
}
}
````

=>Les données en React **circulent en une seule direction -> du parent à l’enfant**. Ainsi, ces props ne doivent pas être modifiés à l'intérieur des composants puisque ces derniers reçoivent des props de leur parent.

### 2- Les states :

A l'inverse des props, le state est “muable” et donc peut être modifié.

En React, le state est interne à un composant, tandis que les props sont transmis au composant. Il est utilisé dans les composants pour garder une trace des informations entre les différents rendus.

L'état du composant est stocké dans "this.state".
````jsx 
class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {counter: 0};
    }
}
````
L'état du composant peut être modifié en appelant :
````jsx
this.setState(data, callback);
````
=>L'argument "data" peut être un objet ou une fonction qui renvoie un objet contenant des clés à mettre à jour. Le callback est optionnel, s'il est fourni il sera appelé après la fin du rendu du composant. Vous aurez rarement besoin d'utiliser ce rappel puisque React prendra soin de garder votre interface utilisateur à jour.

**Exemple** : 

````jsx
Fn(props)
{
console.log(props);
return <div>
<h1> Compteur : <span> {props.name} </span></h1>
<p> {props.children} </p>
</div>
}

class Compteur extends React.Component {

    constructor(props) {
super(props);
this.state = {
name: props.name,
nb: 0,
};

        setTimeout(() =>{this.setState({name: this.state.name + '-', nb: 1,});
        }, 2000);
    }

    render() {
        return <div>
            <h1> <span>{this.state.name}</span></h1>
            <p> {this.state.nb}</p>
        </div>
    }
}

class Accueil extends React.Component {

    render() {
        return <div>
            <CompteurFn name='Compteur 1'/>
            <Compteur name='Compteur 2'/>
        </div>
    }
}
ReactDOM.render(<Accueil/>, document.querySelector('#app'))
````


----------------------
Lien de Vidéo

[006. React - Component state](https://youtu.be/-PVCxQM-bBI)