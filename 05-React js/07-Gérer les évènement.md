# Gérer les évènements 

La gestion des évènements sur les composants React va vous sembler similaire à celle que l'on utilise au niveau du DOM en mode "inline". 

Cependant, elle est plus normalisée et le gestionnaire d'évènement est plus puissant que les évènements classiques que l'on connait. A chaque évènement, vous allez fournir une fonction qui devra être exécuter (un callback) alors que sur du JavaScript "inline", vous écrivez littéralement les différentes instructions.

Les événements React sont écrits dans la syntaxe **camelCase**:

**onClick** au lieu de **onclick**.

Les gestionnaires d'événements React sont écrits entre **accolades** :

**onClick={shoot}** au lieu de **onClick="shoot()"**.

**Exemple** :
````jsx
<button onClick={shoot}>Take the Shot!</button>
````
### Exemple : 
EN HTML
````HTML
<button onclick="activateLasers()">
    Activer les lasers
</button>
````
est légèrement différent avec React:
````jsx
<button onClick={activateLasers}>
  Activer les lasers
</button>
````
**Exemple 2** :
````jsx
<button
onClick={() => {
console.log("Hello world");
}}
>
    Log !
</button>
````
=>Vous allez retrouver la plupart des évènements JavaScript que vous connaissez. Pour rappel, la notation sera du camelCase.

Autre différence importante en React, on **ne peut pas renvoyer false pour empêcher le comportement par défaut**. Vous devez appeler explicitement **preventDefault**.

Par exemple, en HTML, pour annuler le comportement par défaut des liens qui consiste à ouvrir une nouvelle page, vous pourriez écrire :
````html
<a href="#" onclick="console.log('Le lien a été cliqué.'); return false">
  Clique ici
</a>
````
En React, ça pourrait être :
````jsx
function ActionLink() {
function handleClick(e) {
e.preventDefault();
console.log('Le lien a été cliqué.');
}

return (
<a href="#" onClick={handleClick}>
Clique ici
</a>
);
}
````



--------------------------
Lien de vidéo
[007. React - Gérer les évènements ](https://youtu.be/6TR1-9MU6J4)