# Formulaire React 

La gestion des formulaires concerne la manière dont vous gérez les données lorsqu'elles changent de valeur ou sont soumises.

En HTML, les données de formulaire sont généralement gérées par le DOM.

Dans React, les données de formulaire sont généralement gérées par les composants.

Lorsque les données sont gérées par les composants, toutes les données sont stockées dans le composant state.

Vous pouvez contrôler les modifications en ajoutant des gestionnaires d'événements dans l'**onChange** attribut

**Exemple 1** :
````jsx
class MyForm extends React.Component {
render() {
return (
<form>
<h1>Hello</h1>
<p>Enter your name:</p>
<input
type="text"
/>
</form>
);
}
}
ReactDOM.render(<MyForm />, document.getElementById('root'));

````
**Exemple 2** :
````jsx
class MyForm extends React.Component {
constructor(props) {
super(props);
this.state = { username: '' };
}
myChangeHandler = (event) => {
this.setState({username: event.target.value});
}
render() {
return (
<form>
<h1>Hello {this.state.username}</h1>
<p>Enter your name:</p>
<input
type='text'
onChange={this.myChangeHandler}
/>
</form>
);
}
}

ReactDOM.render(<MyForm />, document.getElementById('root'));
````
## Soumettre des formulaires :

Vous pouvez contrôler l'action d'envoi en ajoutant un gestionnaire d'événements dans l'attribut onSubmit:

### Exemple :

Ajoutez un bouton d'envoi et un gestionnaire d'événements dans l'**onSubmit** attribut:
````jsx

class MyForm extends React.Component {
constructor(props) {
super(props);
this.state = { username: '' };
}
mySubmitHandler = (event) => {
event.preventDefault();
alert("You are submitting " + this.state.username);
}
myChangeHandler = (event) => {
this.setState({username: event.target.value});
}
render() {
return (
<form onSubmit={this.mySubmitHandler}>
<h1>Hello {this.state.username}</h1>
<p>Enter your name, and submit:</p>
<input
type='text'
onChange={this.myChangeHandler}
/>
<input
type='submit'
/>
</form>
);
}
}

ReactDOM.render(<MyForm />, document.getElementById('root'));
 
 ````
 
## Champs de saisie multiple : 

Vous pouvez contrôler les valeurs de plusieurs champs d'entrée en ajoutant un nameattribut à chaque élément.

Lorsque vous initialisez l'état dans le constructeur, utilisez les noms de champ.

Pour accéder aux champs du gestionnaire d'événements, utilisez la syntaxe event.target.nameet event.target.value.

Pour mettre à jour l'état dans la this.setState méthode, utilisez des crochets [notation entre crochets] autour du nom de la propriété.

### Exemple :
Rédigez un formulaire avec deux champs de saisie:
````jsx
class MyForm extends React.Component {
constructor(props) {
super(props);
this.state = {
username: '',
age: null,
};
}
myChangeHandler = (event) => {
let nam = event.target.name;
let val = event.target.value;
this.setState({[nam]: val});
}
render() {
return (
<form>
<h1>Hello {this.state.username} {this.state.age}</h1>
<p>Enter your name:</p>
<input
type='text'
name='username'
onChange={this.myChangeHandler}
/>
<p>Enter your age:</p>
<input
type='text'
name='age'
onChange={this.myChangeHandler}
/>
</form>
);
}
}

ReactDOM.render(<MyForm />, document.getElementById('root'));
````
## Textarea : 
L'élément textarea dans React est légèrement différent du HTML ordinaire.

En HTML, la valeur d'une zone de texte était le texte entre la balise de début 
**textarea** et la balise de fin **/textarea**, dans React la valeur d'une zone de texte est placée dans un attribut de valeur :

### Exemple : 

Une zone de texte simple avec du contenu initialisé dans le constructeur:
````jsx
class MyForm extends React.Component {
constructor(props) {
super(props);
this.state = {
description: 'The content of a textarea goes in the value attribute'
};
}
render() {
return (
<form>
<textarea value={this.state.description} />
</form>
);
}
}

ReactDOM.render(<MyForm />, document.getElementById('root'));
````
## Sélectionner :
Une liste déroulante, ou une zone de sélection, dans React est également un peu différente du HTML.

en HTML, la valeur sélectionnée dans la liste déroulante a été définie avec le selected attribut

### Exemple : 

HTML:
````html
<select>
  <option value="Ford">Ford</option>
  <option value="Volvo" selected>Volvo</option>
  <option value="Fiat">Fiat</option>
</select>
````
Dans React, la valeur sélectionnée est définie avec un value attribut sur la selectbalise:

Une boîte de sélection simple, où la valeur sélectionnée "Volvo" est initialisée dans le constructeur:
````jsx
class MyForm extends React.Component {
constructor(props) {
super(props);
this.state = {
mycar: 'Volvo'
};
}
render() {
return (
<form>
<select value={this.state.mycar}>
<option value="Ford">Ford</option>
<option value="Volvo">Volvo</option>
<option value="Fiat">Fiat</option>
</select>
</form>
);
}
}

ReactDOM.render(<MyForm />, document.getElementById('root'));
````


----------------- 

Lien de vidéo 
[012. React - Formulaire](https://youtu.be/DYISuMHXz6M)