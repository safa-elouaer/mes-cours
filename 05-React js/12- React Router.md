# React Router 

Le routage est la capacité d'afficher différentes pages à l'utilisateur. Cela signifie qu'il donne la possibilité de naviguer entre les différentes parties d'une application en entrant une URL ou en cliquant sur un élément.

Et comme vous le savez déjà, par défaut, React vient sans routage. Et pour l'activer, nous devons ajouter une bibliothèque nommée react-router.

Pour l'installer, vous devrez exécuter la commande suivante dans votre terminal:

Pour installer les routers, il faut juste tapper la CLI :

>npm install react-router-dom
> 
> ou
> 
> yarn add react-router-dom

Après lancer le server par la Cli
> npm run start 

## Router 
Le type Route est un objet qui a 5 attributs :

- **name** qui est une chaîne de caractères obligatoire. C’est un alias qui nous servira plus tard pour récupérer une route.
- **path** qui est une chaîne de caractères obligatoire. C’est le chemin d’accès au composant.
- **component** est un composant React qui est facultatif.
- **roles** est un tableau de chaînes de caractères facultatif. Cela représente les rôles nécessaires pour afficher le composant.
- **routes** est un tableau de Route et il est facultatif. Cela permet de renseigner des sous-routes à notre route principale.

## Configuration d'un Router 

Le paquet React Router a un composant pratique appelé **BrowserRouter**. Nous devons d'abord l'importer depuis **react-router-dom** afin de pouvoir utiliser le routage dans notre application.

Dans App.js :

````jsx
import React, { Fragment } from "react"
import "./index.css"

import { BrowserRouter as Router } from "react-router-dom"

export default function App() {
return (
<Router>
<main>
<nav>
<ul>
<li>
<a href="/">Home</a>
</li>
<li>
<a href="/about">About</a>
</li>
<li>
<a href="/contact">Contact</a>
</li>
</ul>
</nav>
</main>
</Router>
)
}
````
Il doit contenir tout les éléments de notre application où le routage est nécessaire. Cela signifie que si nous avons besoin du routage dans l'ensemble de notre application, nous devons envelopper notre composant le plus haut avec BrowserRouter.

=>Un routeur seulement, ne fait pas grand-chose, il est temps d'ajouter une route dans la section suivante.

## Créer une Route 

Pour créer un itinéraire, nous devons importer Route à partir du package du routeur.

Dans App.js

````jsx
import React, { Fragment } from "react"
import "./index.css"

import { BrowserRouter as Router, Route } from "react-router-dom"

export default function App() {
  return (
    <Router>
      <main>
        <nav>
          <ul>
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/about">About</a>
            </li>
            <li>
              <a href="/contact">Contact</a>
            </li>
          </ul>
        </nav>
        <Route path="/" render={() => <h1>Welcome!</h1>} />
      </main>
    </Router>
  )
}
````

Ajoutez-le ensuite là où nous voulons afficher le contenu. Le composant Route a plusieurs propriétés. Mais ici, nous avons juste besoin de path et de render.

- **path** : c'est le chemin à charger lorsque l'itinéraire est atteint. Ici, nous utilisons / pour accéder à la page d'accueil.
  
- **render** : il restituera le contenu de la route. Ici, nous allons afficher un message de bienvenue à l'utilisateur.

Dans certains cas, desservir des itinéraires comme celui-ci est tout à fait correct, mais imaginez lorsque nous devons traiter un composant réel. Utiliser render ne serait pas une bonne solution.

Alors, comment pouvons-nous faire pour afficher quelque chose sans les accessoires de render ? Eh bien, le composant Route a une autre propriété nommée component.

Nous devons mettre à jour notre exemple pour le voir en action.

Dans App.js:
````jsx

import React, { Fragment } from "react"
import "./index.css"

import { BrowserRouter as Router, Route } from "react-router-dom"

export default function App() {
return (
<Router>
<main>
<nav>
<ul>
<li>
<a href="/">Home</a>
</li>
<li>
<a href="/about">About</a>
</li>
<li>
<a href="/contact">Contact</a>
</li>
</ul>
</nav>

        <Route path="/" component={Home} />
      </main>
    </Router>
)
}

const Home = () => (
<Fragment>
<h1>Home</h1>
<FakeText />
</Fragment>
)
````

=> Maintenant, au lieu d'afficher un message, notre route chargera le composant Home.

Pour obtenir toute la puissance de React Router, nous devons avoir plusieurs pages et liens avec lesquels jouer. Nous avons déjà des pages, ajoutons maintenant quelques liens pour pouvoir aller sur d'autres pages.

## Ajouter des liens 

Pour ajouter des liens à notre projet, nous allons utiliser à nouveau la bibliothèque React Router pour le faire.

Dans App.js : 
````jsx
import React, { Fragment } from "react"
import "./index.css"

import { BrowserRouter as Router, Route, Link } from "react-router-dom"

export default function App() {
return (
<Router>
<main>
<nav>
<ul>
<li>
<Link to="/">Home</Link>
</li>
<li>
<Link to="/about">About</Link>
</li>
<li>
<Link to="/contact">Contact</Link>
</li>
</ul>
</nav>

        <Route path="/" exact component={Home} />
        <Route path="/about" component={About} />
        <Route path="/contact" component={Contact} />
      </main>
    </Router>
)
}

const Home = () => (
<Fragment>
<h1>Home</h1>
<FakeText />
</Fragment>
)

const About = () => (
<Fragment>
<h1>About</h1>
<FakeText />
</Fragment>
)

const Contact = () => (
<Fragment>
<h1>Contact</h1>
<FakeText />
</Fragment>
)
````
Après avoir importé Link, nous devons mettre à jour notre barre de navigation. Au lieu d'utiliser a ethref, React Router utilise Link et to pour pouvoir basculer entre les pages sans recharger la page.

Ensuite, nous devons ajouter deux nouveaux itinéraires About et Contact pour pouvoir basculer entre les pages.

Maintenant, nous pouvons accéder à différentes parties de notre application via des liens. Mais il y a un problème avec notre router, le composant Home est toujours affiché même si nous passons à d'autres pages.

La raison est que le React Router vérifie si le path défini commence par / si c'est le cas, il rendra le composant. Et ici, notre premier itinéraire commence par / donc Home sera affiché.

Mais nous pouvons toujours changer le comportement par défaut en ajoutant un nouveau accessoire à Route: la propriété exact.

Dans App.js : 
````jsx
<Route path="/" exact component={Home} />
````
En mettant à jour l'itinéraire Home avecexact, maintenant, il ne sera rendu que s'il correspond au chemin complet.

Nous pouvons encore l'améliorer, en enveloppant nos routes avec Switch pour dire à React Router de ne charger qu'une seule route à la fois.

Dans App.js :
````jsx
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom"
;<Switch>
  <Route path="/" exact component={Home} />
  <Route path="/about" component={About} />
  <Route path="/contact" component={Contact} />
</Switch>
````
Maintenant que nous avons de nouveaux liens, il est temps de passer des paramètres entre les pages.

## Passage des paramètres entre les routes

Pour transmettre des données entre les pages, nous devons mettre à jour notre exemple.

Dans App.js : 
````jsx

import React, { Fragment } from "react"
import "./index.css"

import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom"

export default function App() {
const name = "John Doe"
return (
<Router>
<main>
<nav>
<ul>
<li>
<Link to="/">Home</Link>
</li>
<li>
<Link to={`/about/${name}`}>About</Link>
</li>
<li>
<Link to="/contact">Contact</Link>
</li>
</ul>
</nav>
<Switch>
<Route path="/" exact component={Home} />
<Route path="/about/:name" component={About} />
<Route path="/contact" component={Contact} />
</Switch>
</main>
</Router>
)
}
const Home = () => (
<Fragment>
<h1>Home</h1>
<FakeText />
</Fragment>
)

const About = ({
match: {
params: { name },
},
}) => (
// props.match.params.name
<Fragment>
<h1>About {name}</h1>
<FakeText />
</Fragment>
)

const Contact = () => (
<Fragment>
<h1>Contact</h1>
<FakeText />
</Fragment>
)
````

Ici, nous commençons par déclarer une nouvelle constante name qui sera passée en paramètre à la page About. Et, nous ajoutons name au lien About.

Avec cela, nous devons mettre à jour la route About, en ajustant son chemin pour pouvoir recevoir le nom en tant que paramètre path="/about/:name".

Maintenant, le paramètre sera reçu sous forme d'accessoires dans le composant About et la seule chose que nous devons faire maintenant est de déstructurer les accessoires et de récupérer la propriété name. En passant, {match: {params: {name}}} est la même chose que props.match.params.name.

Jusqu'à présent, nous avons fait beaucoup de choses, cependant, dans certains cas, nous n'allons pas utiliser les liens pour naviguer entre les pages.

Parfois, nous devons attendre la fin d'une opération avant de passer à la page suivante.


----------------------------------------------------
Lien de vidéo 

[013. React - Router](https://youtu.be/JSQbx5vKOOo)