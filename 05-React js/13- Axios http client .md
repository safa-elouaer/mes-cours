# Axios http client
## Axios 
**Axios** est une bibliothèque JavaScript fonctionnant comme un client HTTP. Elle permet de communiquer avec des API en utilisant des requêtes. Comme avec les autres clients HTTP, il est possible de créer des requêtes avec la méthode POST.

=>Axios est basé sur **Promise**, ce qui vous permet de profiter des avantages d’async de JavaScript et await pour un code asynchrone plus lisible.

## La première étape 
- Ajouter Axios au projet en composant la CLI : 
> npm install axios

## Étape 2 :
- Faire une requête GET. 
  
Dans cet exemple, vous créez un nouveau composant et vous y importez Axios pour envoyer une demande GET.

1- Dans le dossier src de votre projet React, créez un nouveau composant nommé PersonList.js 

2- Ajoutez le code suivant au composant
````jsx
import React from 'react';

import axios from 'axios';

export default class PersonList extends React.Component {
state = {
persons: []
}

componentDidMount() {
axios.get(`https://jsonplaceholder.typicode.com/users`)
.then(res => {
const persons = res.data;
this.setState({ persons });
})
}

render() {
return (
<ul>
{ this.state.persons.map(person => <li>{person.name}</li>)}
</ul>
)
}
}
````
Tout d'abord, vous importez React et Axios afin que les deux puissent être utilisés dans le composant. Ensuite, vous vous branchez sur le componentDidMount lifecycle hook et effectuez une demande GET.

Vous utilisez axios.get(url) avec une URL provenant d'un point final de l'API pour obtenir une promesse qui renvoie un objet de réponse. À l'intérieur de l'objet de réponse, il y a des données auxquelles on attribue la valeur person.

Vous pouvez également obtenir d'autres informations sur la demande, telles que le code de statut sous res.status ou plus d'informations à l'intérieur de res.request.

## Etape 3 : 
- Faire une demande POST
  Dans cette étape, vous utiliserez Axios avec une autre méthode de requête HTTP appelée POST.

Supprimer le code précédent dans PersonList et ajoutez les éléments suivants pour créer un formulaire qui permet à l'utilisateur d'entrer des données et ensuite POSTer le contenu à une API :
````jsx
import React from 'react';
import axios from 'axios';

export default class PersonList extends React.Component {
state = {
name: '',
}

handleChange = event => {
this.setState({ name: event.target.value });
}

handleSubmit = event => {
event.preventDefault();

    const user = {
      name: this.state.name
    };

    axios.post(`https://jsonplaceholder.typicode.com/users`, { user })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
}

render() {
return (
<div>
<form onSubmit={this.handleSubmit}>
<label>
Person Name:
<input type="text" name="name" onChange={this.handleChange} />
</label>
<button type="submit">Add</button>
</form>
</div>
)
}
}
````
Dans la fonction handleSubmit, vous empêchez l'action par défaut du formulaire. Ensuite, vous mettez à jour l'état en entrée de l'utilisateur.

Utiliser POST vous donne le même objet de réponse avec des informations que vous pouvez utiliser dans un appel then.

Pour compléter la demande POST, vous devez d'abord saisir l'entrée de l'utilisateur. Ensuite, vous ajoutez les données en même temps que la demande POST, qui vous donnera une réponse. Vous pouvez alors utiliser console.log pour la réponse, qui devrait montrer l'entrée user dans le formulaire.

## Étape 4 
— Faire une demande DELETE
Dans cet exemple, vous verrez comment supprimer des éléments d'une API en utilisant axios.delete et en passant une URL comme paramètre.

Changez le code du formulaire de l'exemple POST pour supprimer un utilisateur au lieu d'en ajouter un nouveau :
````jsx
import React from 'react';
import axios from 'axios';

export default class PersonList extends React.Component {
state = {
id: '',
}

handleChange = event => {
this.setState({ id: event.target.value });
}

handleSubmit = event => {
event.preventDefault();

    axios.delete(`https://jsonplaceholder.typicode.com/users/${this.state.id}`)
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
}

render() {
return (
<div>
<form onSubmit={this.handleSubmit}>
<label>
Person ID:
<input type="text" name="id" onChange={this.handleChange} />
</label>
<button type="submit">Delete</button>
</form>
</div>
)
}
}
````
Là encore, l'objet res vous fournit des informations sur la demande. Vous pouvez ensuite réintroduire ces informations dans le fichier console.log après l'envoi du formulaire.

## Étape 5
— Utiliser une instance de base dans Axios
Dans cet exemple, vous verrez comment vous pouvez mettre en place une instance de base dans laquelle vous pouvez définir une URL et tout autre élément de configuration.

1- Créez un fichier séparé nommé api.js

2- Exportez une nouvelle instance axios avec ces valeurs par défaut
````jsx
import axios from 'axios';

export default axios.create({
baseURL: `http://jsonplaceholder.typicode.com/`
});
````
Une fois que l'instance par défaut est configurée, elle peut être utilisée dans le composant PersonList. Vous importez la nouvelle instance comme ceci :
````jsx
import React from 'react';
import axios from 'axios';

import API from '../api';

export default class PersonList extends React.Component {
handleSubmit = event => {
event.preventDefault();

    API.delete(`users/${this.state.id}`)
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
}
}
````
Parce que http://jsonplaceholder.typicode.com/ est maintenant l'URL de base, vous n'avez plus besoin de taper l'URL complète chaque fois que vous voulez atteindre un point d'extrémité différent sur l'API.

## Étape 6 
— Utilisation de async et de await
Dans cet exemple, vous verrez comment vous pouvez utiliser async et await pour travailler avec des Promises.

Le mot-clé await résout la Promise et renvoie la value. La value peut alors être attribuée à une variable.
````jsx
handleSubmit = async event => {
event.preventDefault();

//
const response = await API.delete(`users/${this.state.id}`);

console.log(response);
console.log(response.data);
};
````
=>Dans cet exemple de code, le .then() est remplacé. La Promise est résolue, et la value est stockée dans la variable de response.

## Exemple de cours :
````jsx

import {Component} from "react";

import axios from 'axios';

class Users extends Component {

    state = {
        users: []
    };

    componentDidMount() {
        // La récupération des users
        axios.get('http://localhost:8080/users?tri=name', {
            params: {
                sort: 'name'
            }
        }).then((response) => {
            console.log('response :', response.data);
            this.setState({users: response.data});
        });
        this.store();
    }

    store() {
        const data = {
            firstName: "Jean",
            lastName: "Saunie",
            email: "jean.saunie@gmail.com"
        };
        axios.post('http://localhost:8080/users', data).then((response) => {
            console.log(response.data.message);
        });
    }

    delete(id) {
        axios.delete('http://localhost:8080/users/' + id)
            .then((response) => {
                console.log("delete :", response);
                const users = [...this.state.users];
                const idx = users.findIndex((user) => user.id === id);
                users.splice(idx, 1);
                this.setState({users: [...users]});
            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        const users = this.state.users.map((user) => {
            return <div key={user.id} onClick={() => this.delete(user.id)}>{user.firstName}</div>
        });
        return (
            <>
                {users}
            </>
        );
    }
}

export default Users;
````

-------------------------
Lien de vidéo 

[014. React - Axios http client ]( https://youtu.be/9o_xq_bIHSw)

[015. React - Axios http client](https://youtu.be/h_Z7DnsGC5w)