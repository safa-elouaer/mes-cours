# Create React app
## 1- creation une application React :

1- Pour créer une application React il fuat commencer par tapper la ligne de commande cli :

> **npx create-react-app mon-app** =>c'est la commande conseillée par Create React App pour initialiser un projet, sans avoir à installer CRA globalement, ou localement
> 
> 
>**cd mon-app** => On rentre dans le projet avec celle cli

=> Cette commande va créer un dossier my-app dans le répertoire où nous avons lancé celle-ci.

Voici l’arborescence que la commande va générer :
![img](../image/img_24.png)

2- Pour lancer notre application il suffit d’utiliser la commande suivante :
>npm run start

## 2-Découvrir les fichiers : 

**node_modules** : c’est là que sont installées toutes les dépendances de notre code. Ce dossier peut vite devenir très volumineux.

**public** : dans ce dossier, vous trouverez votre fichier index.html et d’autres fichiers relatifs au référencement web de votre page.

**src** : vous venez de rentrer dans le cœur de l’action. L’essentiel des fichiers que vous créerez et modifierez seront là.

**package.json** : situé à la racine de votre projet, il vous permet de gérer vos dépendances (tous les outils permettant de construire votre projet), vos scripts qui peuvent être exécutés.
## 3- Les dossiers les plus importants dans notre projet : 

Dans **public**, vous trouvez **index.html** : Il s'agit du **template** de votre application. Il y a plein de lignes de code, avec la div  **<div id="root"></div>** 

Dans **src**, il y a **index.js** qui permet d’initialiser notre app React ;

## 4 -Résumé : 

Un projet initialisé avec CRA possède toujours :

- un fichier **index.html** qui est le **template** où vivra notre app React ;

- un **package.json** qui liste les dépendances et les scripts ;

- un **fichier index.js** dans lequel notre app React est initialisée, et greffée au HTML.

---------------------
Lien de vidéo 

[011. React - Create react app](https://youtu.be/FOk7T9D-5-o)

et enfin, dans  /src, vous trouvez  App.js  qui est notre premier composant React.
