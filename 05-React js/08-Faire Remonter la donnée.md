# Faire remonter les données 

Dans React on qualifie de composant **enfant** tout composant défini dans le **render()** du composant parent, quel que soit son niveau de profondeur.

=>On dit donc que le composant qui fournit le **render()** est le parent, et que tout composant figurant dans ce **render()** est un enfant.

- Deux règles sont importantes :

*Une prop est toujours passée par un composant parent à son composant enfant : c’est le seul moyen normal de transmission

*Une prop est considérée en lecture seule dans le composant qui la reçoit

=>Avec React, partager l’état est possible en le déplaçant dans le plus proche ancêtre commun. On appelle ça « faire remonter l’état ».

### Exemple :
````jsx
class Counter  extends React.Component {
    constructor(props) {
        super(props);
        this.removeCounter=props.onClick;
        this.onNbChange=props.onNbChange;
        this.state={
            nb:props.defaultValue,
        };
    }
    increment(){
        const nb=this.state.nb+1;
        this.onNbChange(nb);
        this.setState({nb});

        this.setState((state)=>{{nb:state.nb+1 }});
    };
    reset(){
        this.onNbChange(0);
        this.setState(()=>{
            return {nb:0};
        })
    }
    decrement (){
        this.setState((state)=>{
           if (state.nb>=1){
               const nb=state.nb-1;
               this.onNbChange(nb);
               return {nb};
           }
           return 0;
        })
    }
   render() {
       let bntReset;
       if (this.state.nb>0){
           bntReset=<button>Réinitialiser</button>
       }
       return <div>
           <h1> {this.props.name}</h1>
       <p>{this.state.nb}</p>
           { (this.state.nb>=1)&& ( <button onClick={()=>this.reset()}>Décrementer</button>)}
       <button onClick={()=>this.reset()}>Incrementer</button>
           {bntReset}
       </div>
   }
}
   class Acceuil extends React.Component{
    state={
        counters:[ ],
    };
    addCounter(){
        let counters = [...this.state.counters];
        let randomNB=Math.round(Math.random()*10);
        counters.push({ name:'compteur', defaultValue});
        this.setState({ counters:[...counters]});

    }
    removeCounter(position){
        let counters=[...this.state.counters];
        counters.splice(position,1);
        this.setState({counters:[...counters]});
    }
    getTotal(){
        return this.state.counters.reduce((accumulator,counter)=>{
            return accumulator+counter.nb;
    },0)
        handlerNbChange(value)
        {
          console.log('handlerNbChange:', value)
        }
         }
    render(){
        let list =this.state.counters.map((counter,index) => {
          return<Counter name={counter.name +'-'+index} defaultValue={counter.defaultValue} key={index} onClick={this.removeCounter.bind(this)

          }
          onNbChange={this.handelerNbChange.bind(this)}/>

        });
      return <div>
          <h1>Total:{this.getTotal()}</h1>
       <button onClick={()=>this.addCounter()}>ajouter un compteur</button>
            {list}
            </div>

    }
   }

ReactDOM.render(<Acceuil/> ,<Counter/>, document.querySelector('#app'));
````
---------------------

Lien vidéo 

[010. React - Faire remonter la donnée]( https://youtu.be/r8CCKLIFPKk)