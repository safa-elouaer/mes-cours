## Création de premier élément :

Pour créer le premier élément l'aide de la ligne de code **React.createElement(…)**
````javascript
  const title = React.createElement('title', {}, 'Hello Word')

````
=> **React.createElement** a pour argument :
- le premier argument c'est le nom du composant (dans notre exemple c'est le title)

- le deuxième argument c'est optionnel 
  
- les autres arguments sont les enfants ( ici un simple texte)
  ### Création de Dom 
  
  Pour afficher un DOM virtuel React dans une page web, on utilise **ReactDOM.render(…)**
````javascript
ReactDOM.render(title,document.body)
console.log(title);
````
=> La console affiche une erreur car c'est déconseillé de mettre body.

=>Alors dans ce cas on revient dans notre **index.html**, on ajoute une balise div dans laquelle on met
````html
<div id="app"></div>
````
Et on, change dans notre fichier js :
````javascript
ReactDom.render(title,document.querySelector('#app'))
````
Pour ajouter une chose interactive on met ce code :
````javascript
let n = 1;
function render() {
const title = React.createElement('h1', {}, 
 'Redirection dans :',
 React.createElement('span', {}, n)
);

ReactDOM.render(title, document.querySelector('#app'));
}
render();

setInterval(() =>{
  n++;
  render();
}, 1000)
````
![img](../image/img_71.png)

-----------------------------------
Lien de vidéo :

[002. React - Premier element](https://youtu.be/HCPBKUoZoHo)