# Syntaxe jsx
## La syntaxe Jsx : 
**JSX** :c’est une extension syntaxique de JavaScript.

Nous recommandons de l’utiliser avec React afin de décrire à quoi devrait ressembler l’interface utilisateur (UI). 

JSX vous fait sûrement penser à un langage de balisage, mais il recèle toute la puissance de JavaScript.

=> Donc **React** est généralement utilisé avec la syntaxe JSX, une extension à JavaScript qui ressemble un peu à du XML au sein de JavaScript.

Jsx va nous permettre de décrire react  plus simplement et d'éviter d'utiliser la méthode **React.createElement()**
- Le problème que jsx est une syntaxe propre qui n'est pas forcément supporté par les navigateurs, donc il nous faut un outil qui va nous permettre de convertir cette syntaxe en js classique.
=> Cet outil s'appelle **Babel**.
  ### Babel : 
transpiler le code (transformer/compiler) 
## Installation de Babel : 
Pour installer Babel il faut :
- Aller dans la documentation **React à un site web** 
- Aller dans la partie **Essayer jsx en un clein d'oeil** 
- copier la ligne de script et la mettre dans le fichier **index.html** dans la balise body.
````html
<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
````
Et je change le nom du fichier js par jsx et ajouter dans la balise app.jsx le type :
````html
<script src="app.jsx" type="text/babel"></script>
````
#### Exemple : 
````javascript
let n=1;
function getstring(){
    return n+'fois'  ;
}
function render(){
    let tags=[
        'Tag 1',
        'Tag 2',
        'Tag 3'
    ];
    const tagsEl=tags.map(( )=>{
        return <li>{tag}</li>
    });

    const title= <React.Fragment>
        <h1>compteur:<span>{n}</span></h1>
        <ul>
            {tagsEl}
        </ul>
    </React.Fragment>
    ReactDOM.render(title,document.querySelector("#app"));

};
render();
setInterval( () =>{
    n++;
    render()
},1000);

````

-------------------------
Lien de Vidéo
[003. React - Syntaxe JSX ]( https://youtu.be/_NdeKIm3QkQ)