# React 

## 1 - Introduction  :
- **React** Une **bibliothèque** JavaScript pour créer des interfaces utilisateurs(IU).

- **React** est un **projet open-source**, désormais distribué sous la très populaire licence MIT et piloté par Facebook, dont tous les produits web et mobiles reposent sur cette technologie.

- **React** c'est un projet open-source créé par Facebook.

- **React** est la couche de vue d'une application MVC (Model View Controller)

=>L'un des aspects les plus importants de React est le fait que vous pouvez créer des composants , qui ressemblent à des éléments HTML personnalisés et réutilisables, pour créer rapidement et efficacement des interfaces utilisateur.

## 2-Installation :
Il existe plusieurs façons de configurer React. Dans ce cours on va utiliser la méthode la plus simple :

 1-Commençons par créer un **index.html** fichier de base . 

 2-Nous allons copier le lien CDN et le mettre à la fin de la balise body 
````html
<script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
````
=>React - l'API de premier niveau React
  React DOM - ajoute des méthodes spécifiques au DOM





---------------
Lien de vidéo

[001. React - Presentation]( https://youtu.be/Bd06YvcdbmI)
  

