# Interpolation property binding

L'intérêt d'utiliser Angular est de pouvoir gérer le DOM (Document Object Model : les éléments HTML affichés par le navigateur) de manière dynamique, et pour cela, il faut utiliser la liaison de données, ou "databinding".

## Le databinding :

C'est la communication entre votre code TypeScript et le template HTML qui est montré à l'utilisateur.
Cette communication est divisée en deux directions :

1- **les informations venant de votre code qui doivent être affichées dans le navigateur**, comme par exemple des informations que votre code a calculé ou récupéré sur un serveur. 

Les deux principales méthodes pour cela sont le **"string interpolation"** et le **"property binding"** ;

2- **les informations venant du template qui doivent être gérées par le code** : l'utilisateur a rempli un formulaire ou cliqué sur un bouton, et il faut réagir et gérer ces événements.  On parlera de "event binding" pour cela.

=>Il existe également des situations comme les formulaires, par exemple, où l'on voudra une communication à double sens : on parle donc de **"two-way binding"**.

Dans ce cours on va s'intéresser à l'interpolation et la propriété binding.

##1- L'interpolation :

**L'interpolation** est la manière la plus basique d'émettre des données issues de votre code TypeScript.

=>La manière la plus simple d’afficher la propriété d’un composant dans son template est d’utiliser le mécanisme de l’interpolation. Avec l’interpolation, nous pouvons afficher la valeur d’une propriété dans la vue d’un template, entre deux accolades, comme ceci : **{{ maPropriete }}**.

>**Mais l'interpolation, c'est quoi exactement ?**
>
>C'est un mécanisme qui va, dans un premier temps, analyser vos templates pour retrouver vos {{interpolations}}, afin de placer, dans un second temps, des écouteurs de changement sur les propriétés de votre component qui sont liées par leurs noms. Une fois cette résolution de liaisons faite, Angular 2 va valoriser vos interpolations avec ces propriétés lors d'un changement de ces dernières.

### Syntaxe : 
 >les doubles accolades :{{ }}

![img](../image/img_5.png)


### Exemple : 

Imaginez une application qui vérifie l'état de vos appareils électriques à la maison pour voir s'ils sont allumés ou non.

1- La première étape est : Créez maintenant un nouveau component  **AppareilComponent**  avec la commande suivante :
>ng generate component appareil

2- Ouvrez ensuite **appareil.component.html** (dans le nouveau dossier **appareil** créé par le CLI), supprimez le contenu, et entrez le code ci-dessous:
````html
<li class="list-group-item">
  <h4>Ceci est dans AppareilComponent</h4>
</li>
````
3- Ensuite, ouvrez **app.component.html** , et remplacez tout le contenu comme suit :
````html
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h2>Mes appareils</h2>
      <ul class="list-group">
        <app-appareil></app-appareil>
        <app-appareil></app-appareil>
        <app-appareil></app-appareil>
      </ul>
    </div>
  </div>
</div>
````
Maintenant votre navigateur devrait vous montrer quelque chose comme cela :

![img](../image/img_2.png)

4- Vous allez maintenant utiliser l'interpolation pour commencer à dynamiser vos données.  Modifiez **appareil.component.html** ainsi :
````html
<li class="list-group-item">
  <h4>Appareil : {{ appareilName }}</h4>
</li>
````
=> Ici, vous trouvez la syntaxe pour l'interpolation :

Les doubles accolades {{ }}. Ce qui se trouve entre les doubles accolades correspond à l'expression TypeScript que nous voulons afficher, l'expression la plus simple étant une variable. 

D'ailleurs, puisque la variable **appareilName** n'existe pas encore, votre navigateur n'affiche rien à cet endroit pour l'instant.
5- Ouvrez maintenant appareil.component.ts :
//Voici notre appareil.component.ts:
````typescript
import { Component, OnInit } from '@angular/core';

@Component({
selector: 'app-appareil',
templateUrl: './appareil.component.html',
styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

constructor() { }

ngOnInit() {
}

}
````
6- Ajoutez maintenant la ligne de code suivante en haut de la déclaration de class:
````typescript
export class AppareilComponent implements OnInit {

appareilName: string = 'Machine à laver';

constructor() { }
}
````
7- Une fois le fichier enregistré, votre navigateur affiche maintenant :
![img](../image/img_3.png)

=>Voilà ! Vous avez maintenant une communication entre votre code TypeScript et votre template HTML

Par exemple, on peut ajouter maintenant une nouvelle variable dans notre **AppareilComponent** :

````typescript
appareilName: string = 'Machine à laver';
appareilStatus: string = 'éteint';
````
Puis intégrez cette variable dans le template:
````html
<li class="list-group-item">
  <h4>Appareil : {{ appareilName }} -- Statut : {{ appareilStatus }}</h4>
</li>
````
Dans ce cas notre navigateur va afficher :
![img](../image/img_4.png)

### Ajouter une méthode  :
On peut utiliser toute expression TypeScript valable pour l'interpolation.

1- Pour démontrer cela, ajouter une méthode au fichier **AppareilComponent** :
````typescript
import { Component, OnInit } from '@angular/core';

@Component({
selector: 'app-appareil',
templateUrl: './appareil.component.html',
styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

appareilName: string = 'Machine à laver';
appareilStatus: string = 'éteint';

constructor() { }

ngOnInit() {
}

getStatus() {
return this.appareilStatus;
}

}
````
2- Modifiez maintenant le template pour prendre en compte ce changement :
````html
<li class="list-group-item">
  <h4>Appareil : {{ appareilName }} -- Statut : {{ getStatus() }}</h4>
</li>
````
=>On doit avoir le même résultat visuel dans le navigateur.

##2- Property binding : 

La liaison par propriété ou "property binding" est une autre façon de créer de la communication dynamique entre votre TypeScript et votre template : plutôt qu'afficher simplement le contenu d'une variable, vous pouvez modifier dynamiquement les propriétés d'un élément du DOM en fonction de données dans votre TypeScript.

### Exemple :
Pour notre application des appareils électriques, imaginons que si l'utilisateur est authentifié, on lui laisse la possibilité d'allumer tous les appareils de la maison. 

Puisque l'authentification est une valeur globale,
1- La première étape est :ajouter une variable boolean dans **AppComponent** ,notre component de base  :
````typescript
import { Component } from '@angular/core';

@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.scss']
})
export class AppComponent {
isAuth = false;
}
````
2- Ajoutez maintenant un bouton au template global **app.component.html**, en dessous de la liste d'appareils :
````html
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h2>Mes appareils</h2>
      <ul class="list-group">
        <app-appareil></app-appareil>
        <app-appareil></app-appareil>
        <app-appareil></app-appareil>
      </ul>
      <button class="btn btn-success" disabled>Tout allumer</button>
    </div>
  </div>
</div>
````
=>La propriété **disabled** permet de désactiver le bouton.  Afin de lier cette propriété au TypeScript, il faut le mettre entre crochets  []  et l'associer à la variable ainsi :
````html
<button class="btn btn-success" [disabled]="!isAuth">Tout allume</button>
//Le point d'exclamation fait que le bouton est désactivé lorsque  isAuth === false
````
3- Pour montrer que cette liaison est dynamique, créez une méthode  constructor  dans  AppComponent , dans laquelle vous créerez une timeout qui associe la valeur  true  à  isAuth  après 4 secondes (pour simuler, par exemple, le temps d'un appel API) :
````typescript
export class AppComponent {
isAuth = false;

constructor() {
setTimeout(
() => {
this.isAuth = true;
}, 4000
);
}
}
````
=>Pour en voir l'effet, rechargez la page dans votre navigateur et observez comment le bouton s'active au bout de quatre secondes.  Pour l'instant le bouton ne fait rien : vous découvrirez comment exécuter du code lorsque l'utilisateur cliquera dessus avec la liaison des événements, ou "event binding".

------------------------
Lien de vidéo :
[Interpolation property binding](https://youtu.be/WvqbK-_L160)