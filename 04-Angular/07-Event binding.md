# Event binding

La liaison d'événement vous permet d'écouter et de répondre aux actions de l'utilisateur telles que les frappes au clavier, les mouvements de souris, les clics et les touches.

=>On va voir avec les events binding comment réagir dans votre code TypeScript aux événements venant du template HTML.
##1- Lier aux évènements :
Pour vous lier à un événement, vous utilisez la syntaxe de liaison d'événement Angular. Cette syntaxe se compose d'un nom d'événement cible entre parenthèses à gauche d'un signe égal et d'une instruction de modèle entre guillemets à droite. Dans l'exemple suivant, le nom de l'événement cible est clicket l'instruction de modèle est onSave().

><button (click)="onSave()">Save</button>

### Exemple : 

Ajoutez la liaison suivante à votre bouton dans le template HTML :
````html

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h2>Mes appareils</h2>
      <ul class="list-group">
        <app-appareil></app-appareil>
        <app-appareil></app-appareil>
        <app-appareil></app-appareil>
      </ul>
      <button class="btn btn-success" 
              [disabled]="!isAuth" 
              (click)="onAllumer()">Tout allumer</button>
    </div>
  </div>
</div>
````

Comme vous pouvez le constater:
- on utilise les parenthèses () pour créer une liaison à un événement.
-  Pour l'instant, la méthode onAllumer() n'existe pas, donc je vous propose de la créer maintenant dans **app.component.ts**, en dessous du constructeur.

La méthode affichera simplement un message dans la console dans un premier temps :
````typescript
 onAllumer() {
 console.log('On allume tout !');
}
````
=>Même si cela reste une fonction très simple pour l'instant, cela vous montre comment lier une fonction TypeScript à un événement venant du template.  De manière générale, vous pouvez lier du code à n'importe quelle propriété ou événement des éléments du DOM.

------------------
lien de vidéo
[Event binding]( https://youtu.be/27ZaLRwBsOc)