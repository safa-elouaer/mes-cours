# Présentation de structure de projet  
## L'arborescence d'un projet Angular :

À la racine du projet, on retrouve un ensemble de fichiers de configuration :

### 1-e2e :end to end
 contenant les fichiers permettant de tester l’application

### 2-node module : 
contenant les fichiers necessaires de la librairie nodeJS pour un projet Angular

### 3-src : 
contenant les fichiers sources de l'application.

à la racine du répertoire src, on retrouve les fichiers classiques index.html, favicon.ico, styles.css, mais également le main.ts (bootstrap d'Angular), le fichier de configuration de la compilation TypeScript tsconfig.json, un fichier de définition TypeScrit typings.d.ts, et un ensemble de polyfills utiles à Angular,

####Que contient src?

**assets** : l’unique dossier accessible aux visiteurs et contenant les images, les sons...

**index.html** : l’unique fichier HTML d’une application Angular

**styles.css** : la feuille de style commune de tous les composants web de l’application

**favicon.ico** : le logo d’Angular

**app** : on retrouve les sources de notre premier projet, dont notre nouveau Component : AppComponent.
contient initialement les 5 fichiers du module principale:

- **app.module.ts** : la classe correspondante au module principale
- **app.component.ts** : la classe associe au composant web
- **app.component.html** : le fichier contenant le code HTML associe au composant web 
- **app.component.css** : le fichier contenant le code CSS associe au composant web
- **app.component.spec.ts** : le fichier de test du composant web

**src/environments** : les fichiers contenus dans ce dossier permettent de définir les variables spécifiques à chaque environnement d'exécution (prod, dev, integration). Par défaut, l'environnement de dev sera utilisé (fichier environment.ts). Si l'on souhaite utiliser le fichier de production, il est nécessaire d'ajouter le paramètre -env=prod lors de l'appel de la commande **ng build**.

### 4-package.json : 
contenant l’ensemble de dépendance de l’application.

### 5- angular-cli.json (ou angular.json depuis la version 6): 
contenant les donnees concernant la configuration du projet (l’emplacement des fichiers de demarrage...)

###6- tslint.json : 
contenant les donnees sur les règles à respecter par le developpeur (nombre de caractères max par ligne, l’emplacement des espaces...)

###7- tsconfig.json :
contenant les données de configuration de TypeScript.

------------------
Lien de vidéo 
[ Angular - Presentation de la structure du projet]( https://youtu.be/S6IHK2iFZcs)