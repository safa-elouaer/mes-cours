# Angular présentation


## Angular :

- Framework JavaScript, open source, presenté par Google en 2009.

-  Permettant de créer des applications web et mobile
- Permet de créer la partie front end des applications web de type **SPA** (Single Page Application Reactive).

##SPA :

C'est une application qui contient une seule page HTML (index HTML) récupérer coté serveur.
## Quels langage utilise Angular :

- HTML pour les vues.
- CSS pour les styles.
- TypeScript pour les scripts 
## Inconvénient du Angular :

L’impossibilité de changer de framework en cours de route : **une application doit être développée en Angular du début à la fin.**

--------------------
Lien de Vidéo:

[Angular - Culture]( https://youtu.be/AFyW-uGxHlc)