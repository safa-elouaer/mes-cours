# La programmation réactive avec Rxjs
## 1- Rxjs :
est une bibliothèque pour la composition de programmes asynchrones et basés sur des événements à l'aide de séquences observables. Il fournit un type de noyau, l' Observable , des types de satellites (Observer, Schedulers, Subjects) et des opérateurs inspirés des extras Array # (mapper, filtrer, réduire, tous, etc.) pour permettre de gérer les événements asynchrones comme des collections.

RxJS combine **des Subjects**,  **des Observables**, **des Operateurs**, et **des Observers**.

- **Les Observables** :un flux des données=> représente l'idée d'une collection invocable de valeurs ou d'événements futurs=>envoient des notifications,
  
- **Observer**: est une collection de callbacks qui sait écouter les valeurs fournies par l'Observable.
  
-  **les Opérateurs** les transforment, tandis que **les Observeurs** les reçoivent

## Résumé :

=> l'objet Observer qui se charge de recevoir **les notifications de l'Observable**.

Il s'agit d'un simple objet qui possède trois méthodes : **next()**, **error()** et **complete()**
- **next()** est appelé à l'arrivée d'un élément
- **error()** est appelé lorsqu'une erreur survient
- **complete()** est appelé en fin de séquence
  
  Les Observers doivent se lier à une source de données et retourner une méthode permettant de détruire ce lien.
  
Ils ont pour mission de pousser les données via l'appel de la méthode **next()**. Une fois leurs missions terminées, ils appellent la méthode **complète()**.

=>Les Observers proposent un certain nombre de garanties :

- Ils empêchent l'appel à **next()** avec **complete()** ou **error()**
- Ils n'autorisent plus d'appel avec la désinscription
Les appels a **complete()** ou **error()**  font appel à la désinscription
  
Si l'une des trois méthodes lance une exception, le mécansime de désincription d'execute.

 ## 2- Créer un observable et programmer avec Rxjs :

Pour créer un cas concret simple, vous allez créer un Observable dans  AppComponent  qui enverra un nouveau chiffre toutes les secondes.  Vous allez ensuite observer cet Observable et l'afficher dans le DOM : de cette manière, vous pourrez dire à l'utilisateur depuis combien de temps il visualise l'application.

**Les étapes** à faire c'est : 
1- Importer la librairie rxjs dans le fichier Index html par une balise script qu'on doit la mettre à la fin de la balise body :
````html
<script src "https://unpkg.com/rxjs/bundles/rxjs.umd.min.js"></script>
````
2- Pour avoir accès aux Observables et aux méthodes que vous allez utiliser, il faut ajouter deux imports :
````typescript
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
````
=>Le premier import sert à **rendre disponible le type Observable**

=>le deuxième vous **donne accès à la méthode que vous allez utiliser** : la méthode  interval() , qui crée un Observable qui émet un chiffre croissant à intervalles réguliers et qui prend le nombre de millisecondes souhaité pour l'intervalle comme argument.

3- Implémentez  OnInit  et créez l'Observable dans  ngOnInit()  :

````typescript
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  ngOnInit() {
    const counter = Observable.interval(1000);
  }
}
````
=> Maintenant que vous avez un Observable, il faut l'observer !  Pour cela, vous utiliserez la fonction  subscribe() , qui prendra comme arguments entre une et trois fonctions anonymes pour gérer les trois types d'informations que cet Observable peut envoyer : des données, une erreur ou un message  complete

4- Créez une variable secondes dans **AppComponent** et affichez-la dans le **template** :
````typescript
export class AppComponent implements OnInit {

secondes: number;

ngOnInit() {
const counter = Observable.interval(1000);
}
}
````
````html
<ul class="nav navbar-nav">
    <li routerLinkActive="active"><a routerLink="auth">Authentification</a></li>
    <li routerLinkActive="active"><a routerLink="appareils">Appareils</a></li>
</ul>
<div class="navbar-right">
    <p>Vous êtes connecté depuis {{ secondes }} secondes !</p>
</div>
````
5- Maintenant vous allez souscrire à l'Observable et créer trois fonctions : la première va se déclencher à chaque émission de données par l'Observable et va attribuer cette valeur à la variable "secondes" ; la deuxième gèrera toute erreur éventuelle ; et la troisième se déclenchera si l'Observable s'achève :
````typescript
ngOnInit()
{
const counter = Observable.interval(1000);
counter.subscribe(
(value) => {
this.secondes = value;
},
(error) => {
console.log('Uh-oh, an error occurred! : ' + error);
},
() => {
console.log('Observable complete!');
}
);
}
````
=>Dans le cas actuel, l'Observable que vous avez créé ne rendra pas d'erreur et ne se complétera pas
=>Le compteur dans le DOM fonctionne, et continuera à compter à l'infini !  
=> Pour éviter tout bugs on la fonction subscribe
## 3- Subscriptions : 
La fonction **subscribe()** prend comme arguments trois fonctions anonymes :

- la première se déclenche à chaque fois que l'Observable émet de nouvelles données, et reçoit ces données comme argument ;

- la deuxième se déclenche si l'Observable émet une erreur, et reçoit cette erreur comme argument ;

- la troisième se déclenche si l'Observable s'achève, et ne reçoit pas d'argument.

=> Afin d'éviter tout problème, quand vous utilisez des Observables personnalisés, il est vivement conseillé de stocker la souscription dans un objet Subscription (à importer depuis rxjs/Subscription) :

````typescript 
export class AppComponent implements OnInit {

  secondes: number;
  counterSubscription: Subscription;

  ngOnInit() {
    const counter = Observable.interval(1000);
    this.counterSubscription = counter.subscribe(
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!');
      }
    );
  }
}
````
2- Vous allez implémenter une nouvelle interface, **OnDestroy** (qui s'importe depuis **@angular/core** ), avec sa fonction **ngOnDestroy()** qui se déclenche quand un component est détruit 
````typescript
export class AppComponent implements OnInit, OnDestroy {

secondes: number;
counterSubscription: Subscription;

ngOnInit() {
const counter = Observable.interval(1000);
this.counterSubscription = counter.subscribe(
(value) => {
this.secondes = value;
},
(error) => {
console.log('Uh-oh, an error occurred! : ' + error);
},
() => {
console.log('Observable complete!');
}
);
}

ngOnDestroy() {
this.counterSubscription.unsubscribe();
}
}
````
=>La fonction  unsubscribe()  détruit la souscription et empêche les comportements inattendus liés aux Observables infinis, donc n'oubliez pas de **unsubscribe** !

## 4- Les opérateurs : 
Un opérateur est une fonction qui se place entre l'Observable et l'Observer (la Subscription, par exemple), et qui peut filtrer et/ou modifier les données reçues avant même qu'elles n'arrivent à la Subscription.  Voici quelques exemples rapides :

- **map()** : modifie les valeurs reçues — peut effectuer des calculs sur des chiffres, transformer du texte, créer des objets…

- **filter()** : comme son nom l'indique, filtre les valeurs reçues selon la fonction qu'on lui passe en argument.

- **throttleTime()** : impose un délai minimum entre deux valeurs — par exemple, si un Observable émet cinq valeurs par seconde, mais ce sont uniquement les valeurs reçues toutes les secondes qui vous intéressent, vous pouvez passer  **throttleTime(1000)**  comme opérateur.

- **scan()** et **reduce()** : permettent d'exécuter une fonction qui réunit l'ensemble des valeurs reçues selon une fonction que vous lui passez — par exemple, vous pouvez faire la somme de toutes les valeurs reçues.
  
La différence basique entre les deux opérateurs : **reduce()** vous retourne uniquement la valeur finale, alors que **scan()** retourne chaque étape du calcul.

--------------------------

Lien de vidéo 

[ Programmation réactive avec la librairie RxJs ](https://youtu.be/Vkp6_ZrF0W4)