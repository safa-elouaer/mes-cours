#Partage des composants entre parents et enfants

Un modèle courant dans Angular consiste à partager des données entre un composant parent et un ou plusieurs composants enfants. Vous pouvez implémenter ce modèle à l'aide des directives et .@Input()@Output()

Considérez la hiérarchie suivante:
````
 <parent-component>
 <child-component></child-component>
 </parent-component>
````
=>Le <parent-component>sert de contexte pour le <child-component>.

=>@Input()et donnez à un composant enfant un moyen de communiquer avec son composant parent. permet à un composant parent de mettre à jour les données du composant enfant. Inversement, permet à l'enfant d'envoyer des données à un composant parent.@Output()@Input()@Output()

## 1- Envoi des données de parents à un composant enfant :
Le décorateur dans un composant enfant ou une directive signifie que la propriété peut recevoir sa valeur de son composant parent.@Input()

### Exemple : 
- parent.component.ts:
````typescript

import {Component} from '@angular/core';

@Component({
selector: 'parent-example',
templateUrl: 'parent.component.html',
})

export class ParentComponent {
mylistFromParent = [];

add() {
this.mylistFromParent.push({"itemName":"Something"});
}

}
````
- parent.component.html:
````html
<p> Parent </p>
  <button (click)="add()">Add</button>

<div>
  <child-component [mylistFromParent]="mylistFromParent"></child-component>
</div>
````
- child.component.ts:
````typescript
import {Component, Input } from '@angular/core';

@Component({
selector: 'child-component',
template: `
<h3>Child powered by parent</h3>
{{mylistFromParent | json}}
`,
})

export class ChildComponent {
@Input() mylistFromParent = [];
}
````
## 2- Envoi de données d'un composant enfant à un composant parent : 

@Output() marque une propriété dans un composant enfant comme une porte à travers laquelle les données peuvent voyager de l'enfant au parent.

Le composant enfant utilise la propriété pour déclencher un événement afin de notifier le parent de la modification. Pour déclencher un événement, un doit avoir le type de , qui est une classe que vous utilisez pour émettre des événements personnalisés.@Output()@Output()EventEmitter@angular/core

### Exemple : 
- event-emitter.component.ts
````typescript
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
selector: 'event-emitting-child-component',
template: `<div *ngFor="let item of data">
<div (click)="select(item)">
{{item.id}} = {{ item.name}}
</div>
</div>
`
})

export class EventEmitterChildComponent implements OnInit{

data;

@Output()
selected: EventEmitter<string> = new EventEmitter<string>();

ngOnInit(){
this.data = [ { "id": 1, "name": "Guy Fawkes", "rate": 25 },
{ "id": 2, "name": "Jeremy Corbyn", "rate": 20 },
{ "id": 3, "name": "Jamie James", "rate": 12 },
{ "id": 4, "name": "Phillip Wilson", "rate": 13 },
{ "id": 5, "name": "Andrew Wilson", "rate": 30 },
{ "id": 6, "name": "Adrian Bowles", "rate": 21 },
{ "id": 7, "name": "Martha Paul", "rate": 19 },
{ "id": 8, "name": "Lydia James", "rate": 14 },
{ "id": 9, "name": "Amy Pond", "rate": 22 },
{ "id": 10, "name": "Anthony Wade", "rate": 22 } ]
}

select(item) {
this.selected.emit(item);

}

}
````
- event-receiver.component.ts:
````typescript

import { Component } from '@angular/core';

@Component({
selector: 'event-receiver-parent-component',
template: `<event-emitting-child-component (selected)="itemSelected($event)">
</event-emitting-child-component>
<p *ngIf="val">Value selected</p>
<p style="background: skyblue">{{ val | json}}</p>`
})

export class EventReceiverParentComponent{
val;

itemSelected(e){
this.val = e;
}
}
````

-------------------
Lien des vidéos
[Angular - Partager des données à un composant enfant - Input decorator ](https://youtu.be/GrWJZYdpnfA)
[Angular - Émettre des données au composant parent](https://youtu.be/tzPiI8_dAoc)