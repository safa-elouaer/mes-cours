# Cycle de vie d'un composant 

Chaque composant a un cycle de vie géré par Angular lui-même.

Le composent possède un cycle de vie géré par Angular. En effet Angular :
- crée le composant
- l’affiche
- crée et affiche ses composants fils
- vérifie quand les données des propriétés changent(écoute les changements de propriétés)
- détruit les composants, avant de les retirer du DOM quand cela est nécessaire

=>Angular nous offre la possibilité d’agir sur ces moments clefs quand ils se produisent, en implémentant une ou plusieurs interfaces, pour chacun des événements disponibles. Ces interfaces sont disponibles dans la librairie core d’Angular.

=>Chaque interface permettant d’interagir sur le cycle de vie d’un composant fournit une et une seule méthode, dont le nom est le nom de l’interface **préfixé par ng**.

*Par exemple* : **l’interface OnInit** fournit la méthode **ngOnInit**, et permet de définir un comportement lorsque le composant est initialisé.

![img](../image/img_14.png)

## Les lifecycle hooks disponibles

Les **lifecycle hooks** sont utilisables sur les composants, mais aussi sur les directives, à l'exception de certains hooks qui sont exclusifs aux composants.

### le constructeur :

Le constructeur **n'est pas un hook**, mais qui fait tout de même partie du cycle de vie du composant : sa création. Il est logiquement appelé en premier, et c'est à ce moment que les dépendances (services) sont injectées dans le composant par Angular.

=> Le constructeur a pour principal objectif la création du composant, sans complexité, qui n'a ni besoin d'être asynchrone, ni besoin des inputs.

Voici ci-dessous la liste des méthodes disponibles pour interagir avec le cycle de vie d’un composant, dans l’ordre chronologique du moment où elles sont appelées par Angular:

### 1- ngOnChanges:

C’est la méthode appelée en premier lors de la création d’un composant, avant même ngOnInit, et à chaque fois que Angular détecte que les valeurs d’une propriété du composant sont modifiées. La méthode reçoit en paramètre un objet représentant les valeurs actuelles et les valeurs précédentes disponibles pour ce composant.

=> cette méthode est appelée à la création du composant, puis à chaque changement de valeur d’un attribut scalaire décoré par @Input()
#### Exemple :

*Exemple 1* : 
````typescript
import {
Component,
OnChanges,
Input
}
from '@angular/core';
@Component({
selector: 'so-onchanges-component',
templateUrl: 'onchanges-component.html',
styleUrls: ['onchanges-component.']
})
class OnChangesComponent implements OnChanges {
    //implémenter l'interface avant d'utiliser la méthode
@Input() name: string;
message: string;
ngOnChanges(changes: SimpleChanges): void {
console.log(changes);
}
}
````
*Exemple 2* :
Il est appelé lorsqu'une propriété input est définie ou bien modifiée. Un objet est passé en paramètre et possède des informations sur le changement de la propriété. Par exemple, avec un input nommé prop1 de type number, qui passe de 1 à 2, nous aurions l'objet suivant 

![img](../image/img_15.png)

=>Il est important de savoir que ngOnChanges est levé uniquement lorsque c'est l'appelant du composant qui modifie les inputs. Si le composant lui-même modifie son input, la méthode n'est pas levée.

### 2- ngOnInit: 

Cette méthode est appelée juste après le premier appel à ngOnChanges, et elle initialise le composant après qu’Angular ait initialisé les propriétés du composant.

=> cette méthode est appelée lors de la création du composant (juste après le premier appel de ngOnChanges()).

=> ngOnInit(): déclenchée après l’exécution du constructeur. Elle permet d’initialiser le composant avec le 1er affichage des données de la vue ayant un binding avec des propriétés de la classe du composant. Cette callback est déclenchée une seule fois à l’initialisation du composant même si ngOnChanges() n’est pas déclenchée.

#### Exemple : 
*Exemple 1* : 
![img](../image/img_16.png)

*Exemple 2* :
1- La première chose à faire est d’importer l’interface que nous allons utiliser. Dans notre cas, il s’agit d’OnInit :

![img](../image/img_17.png)
2- Ensuite, il faut implémenter cette interface pour le composant :
![img](../image/img_18.png)

3- Enfin nous devons ajouter la méthode ngOnInit fournie par cette interface, dans notre composant :

![img](../image/img_19.png)
### 3- ngDoCheck :
On peut implémenter cette interface pour étendre le comportement par défaut de la méthode ngOnChanges, afin de pouvoir détecter et agir sur des changements qu’Angular ne peut pas détecter par lui-même.

=>cette méthode est mise en œuvre pour connaître les changements des valeurs internes d’objets ou de listes, changements de valeurs non identifiables par le hook ngOnChanges().

=>permet d’indiquer des changements si Angular ne les a pas détecté.

=>Appelé lors de chaque exécution de détection de changement, immédiatement après ngOnChanges()et ngOnInit().
#### Exemple :
````typescript
export class lifecycleComponent implements DoCheck{
//Toujours implémenter les interfaces avant d'utiliser les méthodes


constuctor() {
}

ngOnCheck (): void {
console.log('Do Check - Lifecycle :');
}
}
````
### 4- ngAfterContentInit : 

Le lifecycle hook ngAfterContentInit est appelé tout juste après le **premier ngDoCheck**, qui lui-même a suivi **ngOnInit**.

Concrètement, ce hook est levé une fois que le contenu externe est bien projeté dans le composant. On parle ici de transclusion.

=>Pour résumer rapidement, il est possible de passer du contenu HTML et donc un composant, à un composant qui possède une balise ng-content. 

#### Exemple: 
````typescript
export class lifecycleComponent implements AfterContentInit{
//Toujours implémenter les interfaces avant d'utiliser les méthodes


constuctor() {
}

ngAfterContentInit (): void {
console.log('After Content Init - Lifecycle :');
}
}
`````
### 5- ngAfterContentChecked() : 

Dans la même idée que le hook précédent, le hook ngAfterContentChecked est appelé cette fois-ci après chaque vérification du contenu projeté. La méthode est appelée après ngAfterContentInit, puis après chaque ngDoCheck.

=>Appelé après le **ngAfterContentInit()** et tous les suivants ngDoCheck().

=> déclenchée après la détection de changement dans le contenu projeté. Cette callback est déclenchée même s’il n’y a pas de projection de contenu.

#### Exemple : 

````typescript

export class lifecycleComponent implements AfterContentChecked{
//Toujours implémenter les interfaces avant d'utiliser les méthodes 


    constuctor() {
    }

    ngAfterContentChecked (): void {
        console.log('After Content Checked - Lifecycle :');
    }
}

````
### 6- ngAfterViewInit

Une fois que la vue du composant et celle de tous ces composants enfants (inclus dans le template du composant) sont chargées, le lifecycle hook ngAfterViewInit est appelé. Cela arrive après le premier ngAfterContentChecked.

Très similaire au ngAfterContentInit, Le ngAfterViewInit permet de récupérer les instances et références vers les éléments HTML aussi, mais cette fois-ci uniquement ceux qui sont présents dans le template du composant lui-même. Il y a donc une séparation entre le contenu direct du composant et celui qu'on lui projette via transclusion.

=>Appelé une fois après le premier **ngAfterContentChecked()**.
=> Petit changement : c'est le décorateur @ViewChild qu'il faut utiliser !
=>déclenchée après l’initialisation de la vue du composant et après l’initialisation de la vue des composants enfant

#### Exemple : 
![img](../image/img_20.png)
 *Exemple 2* :
````typescript
export class lifecycleComponent implements AfterViewInit{
    
constuctor() {
}

ngAfterViewInit (): void {
console.log('After View Init - Lifecycle :');
}
}
````
### 7- ngAfterViewChecked : 

Ce lifecycle hook est exécuté une fois que les bindings du composant et celui de ses composants enfants ont été vérifiés, et cela même si rien n'a changé. De manière générale, ngAfterViewChecked permet de gérer les cas où le composant parent attend une modification du composant enfant.

Il est appelé après ngAfterViewInit, puis après chaque ngAfterContentChecked.
#### Exemple : 
````typescript
export class lifecycleComponent implements AfterViewChecked{
    
constuctor() {
}
ngOnInit (): void {
console.log('After View Checked - Lifecycle :');
}
}
````
### 8- ngOnDestroy : 

Comme son nom l'indique, ngOnDestroy permet de s'attacher à la destruction du composant. Il est exécuté juste avant la destruction d'un composant et permet alors de réaliser le nettoyage adéquat de son composant. C'est ici qu'on veut se désabonner des Observables ainsi que des events handlers sur lesquels le composant s'est abonné.
=>Appelée en dernier, cette méthode est appelée avant qu’Angular ne détruise et ne retire du DOM le composant. Cela peut se produire lorsqu’un utilisateur navigue d’un composant à un autre par exemple. Afin d’éviter les fuites de mémoire, c’est dans cette méthode que nous effectuerons un certain nombre d’opérations afin de laisser l’application « propre » (nous détacherons les gestionnaires d’événements par exemple).

#### Exemple : 
````typescript
export class lifecycleComponent implements OnDestroy{

constuctor() {
}

ngOnDestroy (): void {
console.log('On Destroy - Lifecycle :');
}
}
````
*Exemple 2* :

Par exemple, supposons qu'un composant soit abonné à une Observable.timer. Il faut se désabonner soit même lors de la destruction du composant. Cela se fait en récupérant la Subscription renvoyée par la méthode subscribe(), puis en exécutant la méthode unsubscribe() dessus. 
![img](../image/img_21.png)

## Résumé : 
- **ngOnChanges** : il est appelé lorsqu'un input est défini ou modifié de l'extérieur. L'état des modifications sur les inputs est fourni en paramètre

- **ngOnInit** : il est appelé une seule fois et permet de réaliser l'initialisation du composant, qu'elle soit lourde ou asynchrone (on ne touche pas au constructeur pour ça)

- **ngDoCheck** : il est appelé après chaque détection de changements

- **ngAfterContentInit** : il est appelé une fois que le contenu externe est projeté dans le composant (transclusion)


- **ngAfterViewInit** : il est appelé dès lors que la vue du composant ainsi que celle de ses enfants sont initialisés

**ngAfterViewChecked** : il est appelé après chaque vérification des vues du composant et des vues des composants enfants.

**ngOnDestroy** : il est appelé juste avant que le composant soit détruit par Angular.

=>A noter, les méthodes ngAfterContentInit, ngAfterContentChecked, ngAfterViewInit et ngAfterViewChecked sont exclusives aux composants, tandis que toutes les autres le sont aussi pour les directives.

![img](../image/img_22.png)

![img](../image/img_23.png)
=>Ce schéma permet de montrer que certains éléments d’un composant sont mis à jour de façon concomitante avec des traitements du composant parent.

----------------------
Liens de vidéo :

1- [Component lifecycle](https://youtu.be/rEu4Ct8MrWY)
