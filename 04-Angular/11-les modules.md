#Les modules

## 1- Module : 
Un module Angular est un mécanisme permettant de :
- regrouper des composants (mais aussi des services, directives, pipes etc...),
- définir leurs dépendances,
- définir leur visibilité.

=>Les modules aident à organiser toute application Angular  en blocs de fonctionnalités cohérentes; les modules permettent d' encapsuler des components (composants), des pipes, des directives et des services. Les modules permettent aux développeurs  de structurer l'ergonomie des applications.

=>Les applications Angular sont généralement modulaires.

![img](../image/img_8.png)


##2- L’AppModule :
Chaque application Angular comporte au moins un module, le module racine, appelé conventionnellement **AppModule**.

Le **module racine** peut être le seul module d'une petite application, mais la plupart des applications ont beaucoup plus de modules. c'est au  développeur,  de décider comment utiliser les modules.


Un module Angular est défini simplement avec une classe (généralement vide) et le décorateur **NgModule**.

Exemple: 
````typescript
import { NgModule } from '@angular/core';

import { PictureModule } from '../picture/picture.module';
import { BookPreviewComponent } from './book-preview/book-preview.component';

@NgModule({//décorateur
declarations: [//Définit la liste des composants (ou directives, pipes etc...) contenus dans ce module
    
BookPreviewComponent
],
exports: [//Définit la liste des composants pouvant être utilisés par les modules qui importent celui-ci.
BookPreviewComponent
],
imports: [//Définit la liste des dépendances du module. Il s'agit généralement de la liste des modules contenant les composants utilisés par les composants de la section declarations
HttpModule,
PictureModule
]
})
export class BookModule {
}
````
Le décorateur **@NgModule ()** est une fonction prenant un seul objet de métadonnées, dont les propriétés décrivent le module. Les propriétés les plus importantes  sont :


**declarations**: ici sont réunis Les composants, les directives et les pipes appartenant à ce NgModule.

**exports**: Le sous-ensemble de déclarations qui devront  être visibles et utilisables dans les templates de composant  d'autres NgModules.

**imports**: Modules dont les classes sont nécessaires au composant de ce module.

**providers**: Services présents dans l'un des modules devant être utilisés dans les autres modules ou composants. Une fois qu'un service est inclus dans providers, il devient accessible dans toutes les parties de cette application.

**bootstrap**: Le composant racine qui est la vue principale de l'application. Ce module racine ne possède que cette propriété et indique le composant à initialiser.
