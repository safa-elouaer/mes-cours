# Installation de Angular et création de projet

##1- Installation :
- Aller sur le site  https://angular.io/guide/setup-local
- créer un nouveau projet angular sur webstorm et ouvrir le terminal
- Lancer la commande npm install -g @angular/cli : permet d’installer la dernière version disponible d’Angular.

- Pour verifier l’installation, exécuter la commande **ng version**.

- Pour explorer la liste des commandes Angular disponibles, exécuter la commande **ng**

##2- Création du projet : 
- Lancer la commande **ng new PROJECT-NAME** (par exemple ng new my-app) et répondre aux questions suivantes:
    - Do you want to enforce stricter type checking and stricter bundle budgets in the workspace ? ? (Yes)
    - Would you like to add Angular routing ? (Yes)
    - Which stylesheet format would you like to use ? (SCSS)
    
- Se déplacer dans le projet par la commande **cd PROJECT-NAME**
- Lancer le projet **ng serve**
  
 - Accédez à http: // localhost: 4200 /. L'application sera automatiquement rechargée

    -------------------
Lien de vidéo :
[ Angular - Setup](https://youtu.be/Hhg5iKRv9pw)
    
