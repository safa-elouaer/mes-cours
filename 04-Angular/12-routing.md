# Routing
Afin de permettre aux utilisateurs de garder leurs habitudes de navigation en visitant une Single Page Application ou une Progressive Web Application, il est nécessaire d'utiliser un système de "Routing".
Grâce au système de "Routing", les utilisateurs peuvent :

- utiliser l'historique de leur navigateur (e.g. les boutons back et next),
- partager des liens,
- ajouter une vue à leurs favoris,
- ouvrir une vue dans une nouvelle fenêtre via le menu contextuel,

=>Angular fournit nativement un module de "Routing" pour répondre à ce besoin.

![img](../image/img_7.png)
Dans ce cours on va prendre comme exemple concret L'application des appareils électriques n'a que la view des appareils à afficher pour le moment ; je vous propose de créer un component pour l'authentification (qui restera simulée pour l'instant) et vous créerez un menu permettant de naviguer entre les views.

1- Tout d'abord, créez le component avec le CLI :

>ng g c auth

2- Vous allez également devoir modifier un peu l'organisation actuelle afin d'intégrer plus facilement le routing : vous allez créer un component qui contiendra toute la view actuelle et qui s'appellera  AppareilViewComponent  :

>ng g c appareil-view

3- Ensuite, coupez tout le contenu de la colonne dans  app.component.html , enregistrez-le dans  appareil-view.component.html , et remplacez-le par la nouvelle balise  <app-appareil-view>  :
````html
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <app-appareil-view></app-appareil-view>
    </div>
  </div>
</div>
````
4-Il faudra également déménager la logique de cette view pour que tout re-marche : injectez  AppareilService , créez l'array  appareils , intégrez la logique  ngOnInit  et déplacez les fonctions  onAllumer()  et  onEteindre()  :
````typescript
import { Component, OnInit } from '@angular/core';
import { AppareilService } from '../services/appareil.service';

@Component({
selector: 'app-appareil-view',
templateUrl: './appareil-view.component.html',
styleUrls: ['./appareil-view.component.scss']
})
export class AppareilViewComponent implements OnInit {

appareils: any[];

lastUpdate = new Promise((resolve, reject) => {
const date = new Date();
setTimeout(
() => {
resolve(date);
}, 2000
);
});

constructor(private appareilService: AppareilService) { }

ngOnInit() {
this.appareils = this.appareilService.appareils;
}

onAllumer() {
this.appareilService.switchOnAll();
}

onEteindre() {
if(confirm('Etes-vous sûr de vouloir éteindre tous vos appareils ?')) {
this.appareilService.switchOffAll();
} else {
return null;
}
}

}
````
5- Vous pouvez faire le ménage dans  AppComponent , en retirant tout ce qui n'y sert plus.  Créez également une boolean  isAuth  dans  AppareilViewComponent , et déclarez-la comme  false , car vous allez intégrer un service d'authentification pour la suite.

Ajoutez la barre de navigation suivante à  AppComponent
````html
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a href="#">Authentification</a></li>
        <li class="active"><a href="#">Appareils</a></li>
      </ul>
    </div>
  </div>
</nav>
````
=>Maintenant, tout est prêt pour créer le routing de l'application.

##1- Créer des routes dans Angular :
Puisque le routing d'une application est fondamentale pour son fonctionnement, on déclare les routes dans  app.module.ts .
1- On crée une constante de type **Routes** (qu'on importe depuis  @angular/router ) qui est un array d'objets JS qui prennent une certaine forme :
###Exemple :
````typescript
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MonPremierComponent } from './mon-premier/mon-premier.component';
import { AppareilComponent } from './appareil/appareil.component';
import { FormsModule } from '@angular/forms';
import { AppareilService } from './services/appareil.service';
import { AuthComponent } from './auth/auth.component';
import { AppareilViewComponent } from './appareil-view/appareil-view.component';
import { Routes } from '@angular/router';

const appRoutes: Routes = [
{ path: 'appareils', component: AppareilViewComponent },//Ne pas ajouter de slash au début de la propriété  path .
{ path: 'auth', component: AuthComponent },
{ path: '', component: AppareilViewComponent }
];
````
=>Le **path** correspond au string qui viendra après le  /  dans l'URL : sur votre serveur local, le premier path ici correspond donc à  localhost:4200/appareils .

2- Ensuite, le **component** correspond au component que l'on veut afficher lorsque l'utilisateur navigue au **path** choisi.

**J'ai ajouté un  path  vide, qui correspond tout simplement à  localhost:4200  (ou à la racine de l'application seule), car si on ne traite pas le  path  vide, chaque refresh de l'application la fera planter.**

=>Les routes sont maintenant créées, mais il faut les enregistrer dans votre application.

3-  vous allez importer  RouterModule  depuis  @angular/router  et vous allez l'ajouter à l'array imports de votre  AppModule , tout en lui appelant la méthode  forRoot()  en lui passant l'array de routes que vous venez de créer :
````typescript
imports: [
BrowserModule,
FormsModule,
RouterModule.forRoot(appRoutes)
];
````
4- Maintenant que les routes sont enregistrées, il ne reste plus qu'à dire à Angular où vous souhaitez afficher les components dans le template lorsque l'utilisateur navigue vers la route en question.  On utilise la balise  <router-outlet>  :
````html
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <router-outlet></router-outlet>
    </div>
  </div>
</div>
````
=>Lorsque vous changez de route (pour l'instant, en modifiant l'URL directement dans la barre d'adresse du navigateur), la page n'est pas rechargée, mais le contenu sous la barre de navigation change.

##2- Naviguez avec les routerLink :
Il peut y avoir des cas où vous aurez besoin d'exécuter du code avant une navigation.  Par exemple, on peut avoir besoin d'authentifier un utilisateur et, si l'authentification fonctionne, de naviguer vers la page que l'utilisateur souhaite voir. 

1- on retire l'attribut  href  et on le remplace par l'attribut  routerLink  :
>Vous pourriez vous dire qu'il suffirait de marquer le  path  de vos routes directement dans l'attribut  href , et techniquement, cela permet d'atteindre les routes que vous avez créées.
>
>Alors pourquoi on ne fait pas comme ça ?

>Tout simplement parce que, si vous regardez bien, en employant cette technique, la page est rechargée à chaque clic !  On perd totalement l'intérêt d'une Single Page App !

````html
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
        <li><a routerLink="auth">Authentification</a></li>
        <li class="active"><a routerLink="appareils">Appareils</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <router-outlet></router-outlet>
    </div>
  </div>
</div>
````

=>Ainsi, les routes sont chargées instantanément : on conserve l'ergonomie d'une SPA.

2- Pour finaliser cette étape, il serait intéressant que la classe  active  ne s'applique qu'au lien du component réellement actif.  Heureusement, Angular fournit un attribut pour cela qui peut être ajouté au lien directement ou à son élément parent :
````html
<ul class="nav navbar-nav">
    <li routerLinkActive="active"><a routerLink="auth">Authentification</a></li>
    <li routerLinkActive="active"><a routerLink="appareils">Appareils</a></li>
</ul>
````

##3- Naviguez avec le Router : 

1- Tout d'abord, créez un nouveau fichier  auth.service.ts  dans le dossier services pour gérer l'authentification (n'oubliez pas de l'ajouter également dans l'array  providers  dans  AppModule ) :
````typescript
export class AuthService {

isAuth = false;

signIn() {
return new Promise(
(resolve, reject) => {
setTimeout(
() => {
this.isAuth = true;
resolve(true);
}, 2000
);
}
);
}

signOut() {
this.isAuth = false;
}
}
//La variable  isAuth  donne l'état d'authentification de l'utilisateur.  
// La méthode  signOut()  "déconnecte" l'utilisateur, 
// et la méthode  signIn()  authentifie automatiquement l'utilisateur au bout de 2 secondes, simulant le délai de communication avec un serveur.
````
2- Dans le component  AuthComponent , vous allez simplement créer deux boutons et les méthodes correspondantes pour se connecter et se déconnecter (qui s'afficheront de manière contextuelle : le bouton "se connecter" ne s'affichera que si l'utilisateur est déconnecté et vice versa) :
````typescript
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
selector: 'app-auth',
templateUrl: './auth.component.html',
styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

authStatus: boolean;

constructor(private authService: AuthService) { }

ngOnInit() {
this.authStatus = this.authService.isAuth
};

onSignIn() {
this.authService.signIn().then(
() => {
console.log('Sign in successful!')
this.authStatus = this.authService.isAuth
});
};

onSignOut() {
this.authService.signOut();
this.authStatus = this.authService.isAuth;
};
};
````
4- Puisque la méthode **signIn()** du service retourne une Promise, on peut employer une fonction callback asynchrone avec  .then()  pour exécuter du code une fois la Promise résolue.  Ajoutez simplement les boutons, et tout sera prêt pour intégrer la navigation :
````html
<h2>Authentification</h2>
<button class="btn btn-success" *ngIf="!authStatus" (click)="onSignIn()">Se connecter</button>
<button class="btn btn-danger" *ngIf="authStatus" (click)="onSignOut()">Se déconnecter</button>
````
5- Le comportement recherché serait qu'une fois l'utilisateur authentifié, l'application navigue automatiquement vers la view des appareils.  Pour cela, il faut injecter le  Router  (importé depuis  @angular/router ) pour accéder à la méthode  navigate()  :

>constructor(private authService: AuthService, private router: Router) { };

````typescript
onSignIn()
{
    this.authService.signIn().then(
      () => {
        console.log('Sign in successful!');
        this.authStatus = this.authService.isAuth;
        this.router.navigate(['appareils']);
      }
    );
}
````
->La fonction navigate prend comme argument un array d'éléments (ce qui permet de créer des chemins à partir de variables, par exemple) qui, dans ce cas, n'a qu'un seul membre : le  path  souhaité.
##4- Paramètres de rooting : 

Les routes qu'on vient de voir sont des routes  statiques, mais comment peut-on rendre une partie de la route dynamique ?

Deux solutions sont possibles :

- **Path param** pour **les paramètres requis** (exemple: /users/12345)
- **Query param** pour **les paramètres optionels** (exemple: /login?referrer=/profile)

Dans ce cours on va s'intéresser seulement au **Path param**
## Path param

Les path params se définissent à l’aide du préfix : suivi de son nom.

![img](../image/img_9.png)

=>Ce paramètre est alors récupérable via la route active.
````typescript
// Synchrone
this.id = this.route.snapshot.params['id'];
// Asynchrone
this.route.params.subscribe(params => this.id = params['id']);
````
### Exemple : 

(Dans le meme exemple des appareils) Imaginez qu'on souhaite pouvoir cliquer sur un appareil dans la liste d'appareils afin d'afficher une page avec plus d'informations sur cet appareil : on peut imaginer un système de routing de type  appareils/nom-de-l'appareil , par exemple.  Si on n'avait que deux ou trois appareils, on pourrait être tenté de créer une route par appareil.
Mais imaginez un cas de figure où l'on aurait 30 appareils, ou 300.
Imaginez qu'on laisse l'utilisateur créer de nouveaux appareils ; l'approche de créer une route par appareil n'est pas adaptée.

Dans ce genre de cas, on choisira plutôt de créer une route avec paramètre
1- Tout d'abord, vous allez créer la route dans **AppModule** :
````typescript
const appRoutes: Routes = [
{ path: 'appareils', component: AppareilViewComponent },
{ path: 'appareils/:id', component: SingleAppareilComponent },
{ path: 'auth', component: AuthComponent },
{ path: '', component: AppareilViewComponent }
];
````
=>L'utilisation des deux-points **:** avant un fragment de route déclare ce fragment comme étant un paramètre : tous les chemins de type  appareils/*  seront renvoyés vers  SingleAppareilComponent ,  que vous allez maintenant créer :
````typescript
import { Component, OnInit } from '@angular/core';
import { AppareilService } from '../services/appareil.service';

@Component({
selector: 'app-single-appareil',
templateUrl: './single-appareil.component.html',
styleUrls: ['./single-appareil.component.scss']
})
export class SingleAppareilComponent implements OnInit {

name: string = 'Appareil';
status: string = 'Statut';

constructor(private appareilService: AppareilService) { }

ngOnInit() {
}

}
````
````html
<h2>{{ name }}</h2>
<p>Statut : {{ status }}</p>
<a routerLink="/appareils">Retour à la liste</a>
````
2- Pour l'instant, si vous naviguez vers **/appareils/nom**, peu importe le nom que vous choisissez, vous avez accès à  SingleAppareilComponent .  Maintenant, vous allez y injecter  ActivatedRoute , importé depuis  @angular/router , afin de récupérer le fragment  id  de l'URL :
````ts
     constructor   (private appareilService: AppareilService, private route: ActivatedRoute) {}

````
3- Puis, dans **ngOnInit()**, vous allez utiliser l'objet **snapshot** qui contient les paramètres de l'URL  et, pour l'instant, attribuer le paramètre  **id**  à la variable  name  :

````typescript
ngOnInit()
{
this.name = this.route.snapshot.params['id'];
}
````
4- Ainsi, le fragment que vous tapez dans la barre d'adresse après **appareils/** s'affichera dans le template, mais ce n'est pas le comportement recherché.  Pour atteindre l'objectif souhaité, commencez par ajouter, dans  AppareilService , un identifiant unique pour chaque appareil et une méthode qui rendra l'appareil correspondant à un identifiant :
````typescript
appareils = [
{
id: 1,
name: 'Machine à laver',
status: 'éteint'
},
{
id: 2,
name: 'Frigo',
status: 'allumé'
},
{
id: 3,
name: 'Ordinateur',
status: 'éteint'
}
];
````
````typescript
getAppareilById(id =number)
{
const appareil = this.appareils.find(
(s) => {
return s.id === id;
}
);
return appareil;
}
````
5- Maintenant, dans**SingleAppareilComponent**, vous allez récupérer l'identifiant de l'URL et l'utiliser pour récupérer l'appareil correspondant :
````typescript
ngOnInit()
{
const id = this.route.snapshot.params['id'];
this.name = this.appareilService.getAppareilById(+id).name;
this.status = this.appareilService.getAppareilById(+id).status;
}
````
6- Pour finaliser cette fonctionnalité, intégrez l'identifiant unique dans  AppareilComponent  et dans  AppareilViewComponent , puis créez un  routerLink  pour chaque appareil qui permet d'en regarder le détail :
````angular
@Input() appareilName: string;
@Input() appareilStatus: string;
@Input() index: number;
@Input() id: number;
````
````html
<ul class="list-group">
  <app-appareil  *ngFor="let appareil of appareils; let i = index"
                 [appareilName]="appareil.name"
                 [appareilStatus]="appareil.status"
                 [index]="i" 
                 [id]="appareil.id"></app-appareil>
</ul>
````


`````html
<h4 [ngStyle]="{color: getColor()}">Appareil : {{ appareilName }} -- Statut : {{ getStatus() }}</h4>
<a [routerLink]="[id]">Détail</a>
`````
=>Ici, vous utilisez le format array pour **routerLink** en property binding afin d'accéder à la variable **id** .
## Router service :

**un service** permet de centraliser des parties de votre code et des données qui sont utilisées par plusieurs parties de votre application ou de manière globale par l'application entière. 

Les services permettent donc :

- de ne pas avoir le même code doublé ou triplé à différents niveaux de l'application ça facilite donc la maintenance, la lisibilité et la stabilité du code ;


- de ne pas copier inutilement des données si tout est centralisé, chaque partie de l'application aura accès aux mêmes informations, évitant beaucoup d'erreurs potentielles.

Nous allons rajouter une page dédiée aux services dans notre projet.
C'est dans cette page que nous ferons tous nos tests et modifications de services.

1- Tout d'abord créons la nouvelle page avec les commandes Angular CLI correspondantes.

![img](../image/img_10.png)

![img](../image/img_11.png)
dans src/app/modules/application/services/services.module.ts : 
![img](../image/img_12.png)
Dans src/app/app.module.ts : 
![img](../image/img_13.png)


