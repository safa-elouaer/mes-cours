# Premier composant

Les components sont les composantes de base d'une application Angular : une application est une arborescence de plusieurs components.
  
![img](../image/img_1.png)

#####Une petite explication pour mieux comprendre la structure du code
Dans la structure de notre code le dossier le plus important c'est le dossier **src**, où on trouvera tous les fichiers sources pour notre application.

Pour commencer à comprendre la structure d'une application Angular,on ouvre **index.html** :
````html
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>MonProjetAngular</title>
    <base href="/">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
    <app-root></app-root>
</body>
</html>
````
On peut constater, au lieu d'y voir tout le contenu que nous voyons dans le navigateur, il n'y a que cette balise vide  <app-root>  : il s'agit d'une balise Angular. 

Pour mieux comprendre , on va ouvrir le dossier app: 
![img](../image/Capture3.PNG)

Ce dossier contient le module principal de l'application et les trois fichiers du component principal  AppComponent  :
- son template en HTML,
- sa feuille de styles en SCSS, 
- et son fichier TypeScript, qui contiendra sa logique.

En ouvrant le fichier **app.component.html** on a tout le code HTML se trouvant dans le navigateur.

##### Donc la question qui se pose comment angular peut  injecter ce code dans la balise <app-root>?

En regardant le fichier **app.component.ts** on a :
````typescript
import { Component } from '@angular/core';

@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.scss']
})
export class AppComponent {
title = 'app';
}
````
**@Component** : pour declarer cette classe comme composant web=> c'est le **décorateur**

Dans ce décorateur **@Component()** on trouve un objet un objet qui contient les éléments suivants :

- **selector** : il s'agit du nom qu'on utilisera comme balise HTML pour afficher ce component, comme vous l'avez vu avec <app-root>.

- **templateUrl**: le chemin vers le code HTML à injecter ;

- **styleUrls** : un array contenant un ou plusieurs chemins vers les feuilles de styles qui concernent ce component.

=>Donc pour récapituler Quand Angular rencontre la balise **<app-root>** dans le document HTML, il sait qu'il doit en remplacer le contenu par celui du template  **app.component.html** , en appliquant les styles **app.component.scss** , le tout géré par la logique du fichier  **app.component.ts**.

## 1- Création d'un component :
###la première méthode c'est la méthode manuelle :

1- Dans le dossier **app** créer un nouveau dossier **component**
2- Dans ce dernier,créer un dossier app et dépalacer les fichiers components dans ce dossier (pas les modules,
  seulement components html, css, ts)

3-Créer un autre dossier dans lequel créer trois  fichiers component html, css et ts

4- Dans le fichier ts on note comme suit :
```typescript
import { Component } from '@angular/core';

@Component({
selector: 'app-home',
template: './home.component.html',
styleUrls: ['./home.component.scss']
})

export class HomeComponent {
title = 'my-app';
}
```
Dans le fihier app.module.ts ,on doit importer notre élément et après le déclarer dans @NgModule dans déclaration.Comme suit:

>import {HomeComponent} from './components/home.component';

````typescript

@NgModule({
déclarations: [
AppComponent,
HomeComponent
],
})
````
### La deuxième méthode :

Depuis le dossier principal de notre projet, on tape la commande suivante dans le terminal :

>ng generate component mon-premier 

=>Cette commande va nous permettre de créer automatiquement tous les fichiers liés à notre component et en mettant à jour le fichier le fichier **app.module.ts** et en déclarant le nouveau component créé.

------------------
Lien de vidéo 
[Notre premier composant]( https://youtu.be/KEqbZzY9H2Y)






