# Install Bootstrap

Pour installer Bootstrap voir le lien de la vidéo :

[ Install bootstrap](https://youtu.be/g-z_LSE7EBM)

- Il faut taper la ligne de commande dans le terminal le lien d'installation de bootstrap:

npm install bootstrap@3.3.7 
- Cette commande téléchargera Bootstrap et l'intégrera dans le **package.json** du projet. 
 -  Il nous reste une dernière étape pour l'intégrer à votre projet. Il faut Ouvrir le fichier **angular.json** du dossier source de notre projet.  
 -  Dans "architect/build/options", modifiez l'array  styles  comme suit :
    
    ````CSS
    "styles": [
    "../node_modules/bootstrap/dist/css/bootstrap.css",
    "styles.scss"
    ]

  Maintenant nous pouvons lancer le serveur de développement local (il faudra le couper avec Ctrl-C et le relancer s'il tournait déjà pour que les changements prennent effet) :

**ng serve**
