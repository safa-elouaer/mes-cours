# Two way databinding

La liaison bidirectionnelle donne aux composants de votre application un moyen de partager des données. Utilisez la liaison bidirectionnelle pour écouter les événements et mettre à jour les valeurs simultanément entre les composants parents et enfants.

=>La liaison bidirectionnelle combine la **liaison de propriété** avec la **liaison d'événement** :

- La liaison de propriété définit une propriété d'élément spécifique.
- La liaison d'événement écoute un événement de changement d'élément.

Ce mécanisme permet, à partir d'une même notation, de modifier le modèle à partir du DOM et de modifier le DOM à partir du modèle. Voici la syntaxe utilisée dans Angular pour déclarer un tel Data Binding :

><ma-taille [(taille)] ></ma-taille>

Il faut se souvenir que les parenthèses sont entre les crochets. Pour se souvenir de cela, cette notation se nomme "banana in a box"… Cette banane dans une boite rappelle ce que nous avons pu voir avec le Property Binding et l'Event Binding. Effectivement, ce que nous venons de voir aurait pu également s'écrire :
 ><input [taille]="maTaille" (tailleChange)="maTaille=$event">
 
 Nous pouvons dès lors en déduire que le code que notre component aura un **@Input** nommé taille et un **@output** nommé tailleChange.
 Cela constitue donc la règle du Data Binding 2-way : si l'on considère la notation [(x)], x est la propriété à setter et xChange est l'évènement lancé lors d'une modification de sa valeur. Cet évènement sera donc intercepté par son component parent afin de récupérer cette valeur.
## 1- Propriétés personnalisées : 
Il est possible de créer des propriétés personnalisées dans un component afin de pouvoir lui transmettre des données depuis l'extérieur.

### Exemple : 
Pour l'application des appareils électriques, il serait intéressant de faire en sorte que chaque instance  d'  AppareilComponent  ait un nom différent qu'on puisse régler depuis l'extérieur du code.  Pour ce faire, il faut utiliser le décorateur **@Input()**en remplaçant la déclaration de la variable  appareilName
````typescript
import { Component, Input, OnInit } from '@angular/core';

@Component({
selector: 'app-appareil',
templateUrl: './appareil.component.html',
styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

@Input() appareilName: string;

appareilStatus: string = 'éteint';

constructor() { }

ngOnInit() {
}

getStatus() {
return this.appareilStatus;
}

}
````
>N'oubliez pas d'importer **Input** depuis  @angular/core  en haut du fichier !

Ce décorateur, en effet, crée une propriété  appareilName  qu'on peut fixer depuis la balise  <app-appareil>  :
````html
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h2>Mes appareils</h2>
      <ul class="list-group">
        <app-appareil appareilName="Machine à laver"></app-appareil>
        <app-appareil appareilName="Frigo"></app-appareil>
        <app-appareil appareilName="Ordinateur"></app-appareil>
      </ul>
      <button class="btn btn-success"
              [disabled]="!isAuth"
              (click)="onAllumer()">Tout allumer</button>
    </div>
  </div>
</div>
````
C'est une première étape intéressante, mais ce serait encore plus dynamique de pouvoir passer des variables depuis  AppComponent  pour nommer les appareils (on peut imaginer une autre partie de l'application qui récupérerait ces noms depuis un serveur, par exemple).  Heureusement, vous savez déjà utiliser le property binding !

Créez d'abord vos trois variables dans  AppComponent  :  
````typescript
export class AppComponent {
isAuth = false;

appareilOne = 'Machine à laver';
appareilTwo = 'Frigo';
appareilThree = 'Ordinateur';

constructor() {
    
}}
````
Maintenant, utilisez les crochets  []  pour lier le contenu de ces variables à la propriété du component :
````html
<ul class="list-group">
    <app-appareil [appareilName]="appareilOne"></app-appareil>
    <app-appareil [appareilName]="appareilTwo"></app-appareil>
    <app-appareil [appareilName]="appareilThree"></app-appareil>
</ul>
````
Vous pouvez également créer une propriété pour régler l'état de l'appareil :
````
export class AppareilComponent implements OnInit {

@Input() appareilName: string;
@Input() appareilStatus: string;

constructor() {
````
````html
<ul class="list-group">
    <app-appareil [appareilName]="appareilOne" [appareilStatus]="'éteint'"></app-appareil>
    <app-appareil [appareilName]="appareilTwo" [appareilStatus]="'allumé'"></app-appareil>
    <app-appareil [appareilName]="appareilThree" [appareilStatus]="'éteint'"></app-appareil>
</ul>
//Notez bien que si vous employez les crochets pour le property binding et que vous souhaitez y passer un string directement, il faut le mettre entre apostrophes, car entre les guillemets, il doit y avoir un statement de TypeScript valable. Si vous omettez les apostrophes, vous essayez d'y passer une variable nommée  allumé  ou  éteint  et l'application ne compilera pas.
````
-----------------
lien de vidéo
[Angular - Two way data binding](https://youtu.be/dqIo328Rp2s)