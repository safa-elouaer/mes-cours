# Service et observable 



Les Observables sont un concept que l’on retrouve dans le framework Angular. Il est ainsi possible de créer des services Réactifs en utilisant les Observables pour propager plus facilement la mises à jour des données dans les composants qui consomment ces datas.

Les **Observables** peuvent aider à gérer les données asynchrones et quelques autres modèles utiles.

Les observables sont similaires aux promesses, mais avec quelques différences clés. Le premier est que les observables émettent
plusieurs valeurs au fil du temps. 

Par exemple, une promesse une fois appelée renverra toujours une valeur ou une erreur.

C'est génial jusqu'à ce que vous ayez plusieurs valeurs au fil du temps.

Les gestionnaires de données ou d'événements basés sur une socket Web / en temps réel peuvent émettre plusieurs valeurs à un moment donné.
C'est là que les Observables brillent vraiment.
Les observables sont largement utilisés dans Angular. Le nouveau service HTTP et le système EventEmitter sont tous basés sur Observable. Regardons un exemple où nous souscrivons à un observable.

Regardons un exemple où nous souscrivons à un observable.
````ts
todosService.todos.subscribe(updatedTodos => {
this.componentTodos = updatedTodos;
});

// OR if using the prefered async pipe
// https://angular.io/docs/ts/latest/guide/pipes.html
this.todos = todosService.todos;
````
Dans cet extrait, notre todos propriété sur notre service de données est un observable. Nous pouvons souscrire à cet
Observable dans notre composant. Chaque fois qu'une nouvelle valeur émise par notre Observable Angular met à jour la vue.
````html
<!-- Async pipe is used to bind an observable directly in our template -->
<div *ngFor="let todo of todos | async">
   <button (click)="deleteTodo(todo.id)">x</button>
</div>
````

Les observables sont traités comme des tableaux. Chaque valeur au fil du temps est un élément du tableau.
Cela nous permet d'utiliser le tableau comme méthodes appelées opérateurs sur notre Observable tels que map, flatmap,
reduce, ect. Dans notre service, nous utiliserons un type spécial d'observable appelé BehaviorSubject.
Un BehaviorSubject nous permet de pousser et d'extraire des valeurs vers l'Observable sous-jacent. Nous verrons comment cela nous
aidera à construire notre service.

Dans Angular, nous utilisons RxJS une bibliothèque polyfill / util pour les primitives Observables proposée dans la prochaine nouvelle version JavaScript. RxJS version 5 est une dépendance homologue avec Angular.
Un observable mince est utilisé dans le noyau angulaire. L'Observable mince n'a pas beaucoup d'opérateurs utiles qui rendent RxJS si productif.
L'Observable en Angular est mince pour garder le site d'octets de la bibliothèque vers le bas. Pour utiliser les opérateurs supplémentaires que nous les importer comme
si: import { map } from 'rxjs/operators';.

Dans notre exemple, nous aurons un fichier TodosService. Notre service todos aura des opérations CRUD de base et un
flux Observable auquel s'abonner. Dans cet exemple, nous utiliserons une API basée sur REST, mais elle pourrait être convertie
en API basée sur une socket en temps réel avec peu d'effort. Jetons ensuite un œil au constructeur de notre service.
````ts
@Injectable()
export class TodoService {
private _todos = new BehaviorSubject<Todo[]>([]);
private baseUrl = 'https://56e05c3213da80110013eba3.mockapi.io/api';
private dataStore: { todos: Todo[] } = { todos: [] }; // store our data in memory
readonly todos = this._todos.asObservable();

constructor(private http: HttpClient) {} // using Angular Dependency Injection

get todos() {
return this._todos.asObservable();
}
}
````
Dans notre service todo, nous avons quelques pièces mobiles. Nous avons d'abord un magasin de données privé. C'est là que nous stockons notre
liste de tâches en mémoire. Nous pouvons renvoyer cette liste immédiatement pour un rendu plus rapide ou hors ligne. Pour l'instant,
il tient simplement notre liste de choses à faire. Étant donné que les services dans Angular sont des singletons, nous pouvons les utiliser
pour contenir notre modèle / état de données que nous voulons partager.

Vient ensuite notre todosBehaviorSubject. Notre BehaviorSubject peut recevoir et émettre de nouvelles listes Todo.
Nous ne voulons pas que les abonnés de notre service puissent pousser de nouvelles valeurs à notre sujet sans passer
par nos méthodes CRUD. Pour éviter que les données ne soient modifiées en dehors du service, nous exposons le BehaviorSubject via
une propriété publique et le convertissons en Observable à l'aide de l' asObservableopérateur. Cela permettra aux composants de recevoir des mises à jour mais pas de pousser de nouvelles valeurs.
Nous verrons comment cela se fait plus loin dans l'exemple. Notez que a BehaviorSubjectest comme la Subjectclasse
mais nécessite une valeur de départ initiale.

Désormais, dans nos méthodes publiques, nous pouvons charger, créer, mettre à jour et supprimer des todos. Commençons par charger les todos.
````ts
@Injectable()
export class TodoService {
private _todos = new BehaviorSubject<Todo[]>([]);
private baseUrl = 'https://56e05c3213da80110013eba3.mockapi.io/api';
private dataStore: { todos: Todo[] } = { todos: [] };
readonly todos = this._todos.asObservable();

constructor(private http: HttpClient) {}

get todos() {
return this._todos.asObservable();
}

loadAll() {
this.http.get(`${this.baseUrl}/todos`).subscribe(
data => {
this.dataStore.todos = data;
this._todos.next(Object.assign({}, this.dataStore).todos);
},
error => console.log('Could not load todos.')
);
}
}
````

Notez qu'au lieu de ces méthodes renvoyant de nouvelles valeurs de notre liste de tâches, elles mettent à jour notre magasin de données interne.
Nous faisons une copie en utilisant Object.assign()donc nous renvoyons une nouvelle copie de todos et ne
passons pas accidentellement une référence au magasin de données d'origine.
````ts
// Push a new copy of our todo list to all Subscribers.
this._todos.next(Object.assign({}, this.dataStore).todos);
````
Une fois que le magasin de données de todos est mis à jour, nous poussons la nouvelle liste de todos avec notre _todosBehaviorSubject privé .
Désormais, chaque fois que nous appelons l'une de ces méthodes, tout composant abonné à notre todosflux public Observable
obtiendra une valeur poussée vers le bas et aura toujours la dernière version des données.
Cela permet de garder nos données cohérentes dans toute notre application.

Dans la méthode de notre composant, nous nous abonnons au flux de données puis appelons pour charger le dernier dans le flux. Le est l'endroit idéal pour charger des données. Vous pouvez lire les documents et les différentes raisons pour lesquelles il s'agit d'une bonne pratique.ngOnInit
todosload()
ngOnInit
````ts
// In our todo component
ngOnInit()
{
this.todos = this.todoService.todos; // subscribe to entire collection

// subscribe to only one todo
this.singleTodo$ = this.todoService.todos.pipe(
map(todos => todos.find(item => item.id === '1'))
);

this.todoService.loadAll();    // load all todos
this.todoService.load('1');    // load only todo with id of '1'
}
````
Nous pourrions ensuite appeler remove et notre flux obtiendra une nouvelle liste avec une tâche en moins.
Ajoutons donc le reste du code pour ajouter des opérations CRUD à nos tâches.
Regardons maintenant le service todos dans son intégralité.
````ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Todo {
id?: any;
createdAt?: number;
value: string;
}

@Injectable()
export class TodoService {
private _todos = new BehaviorSubject<Todo[]>([]);
private baseUrl = 'https://56e05c3213da80110013eba3.mockapi.io/api';
private dataStore: { todos: Todo[] } = { todos: [] };
readonly todos = this._todos.asObservable();

constructor(private http: HttpClient) {}

loadAll() {
this.http.get(`${this.baseUrl}/todos`).subscribe(
data => {
this.dataStore.todos = data;
this._todos.next(Object.assign({}, this.dataStore).todos);
},
error => console.log('Could not load todos.')
);
}

load(id: number | string) {
this.http.get<Todo>(`${this.baseUrl}/todos/${id}`).subscribe(
data => {
let notFound = true;

        this.dataStore.todos.forEach((item, index) => {
          if (item.id === data.id) {
            this.dataStore.todos[index] = data;
            notFound = false;
          }
        });

        if (notFound) {
          this.dataStore.todos.push(data);
        }

        this._todos.next(Object.assign({}, this.dataStore).todos);
      },
      error => console.log('Could not load todo.')
    );
}

create(todo: Todo) {
this.http
.post<Todo>(`${this.baseUrl}/todos`, JSON.stringify(todo))
.subscribe(
data => {
this.dataStore.todos.push(data);
this._todos.next(Object.assign({}, this.dataStore).todos);
},
error => console.log('Could not create todo.')
);
}

update(todo: Todo) {
this.http
.put<Todo>(`${this.baseUrl}/todos/${todo.id}`, JSON.stringify(todo))
.subscribe(
data => {
this.dataStore.todos.forEach((t, i) => {
if (t.id === data.id) {
this.dataStore.todos[i] = data;
}
});

          this._todos.next(Object.assign({}, this.dataStore).todos);
        },
        error => console.log('Could not update todo.')
      );
}

remove(todoId: number) {
this.http.delete(`${this.baseUrl}/todos/${todoId}`).subscribe(
response => {
this.dataStore.todos.forEach((t, i) => {
if (t.id === todoId) {
this.dataStore.todos.splice(i, 1);
}
});

        this._todos.next(Object.assign({}, this.dataStore).todos);
      },
      error => console.log('Could not delete todo.')
    );
}
}
````
## Aperçu :

![img](../image/img_41.png)


--------------------------------------
Lien de vidéo 

[022. Services et observables ]( https://youtu.be/m2gHSnI2CPc)