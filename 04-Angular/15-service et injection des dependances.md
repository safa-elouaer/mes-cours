# Les services et injection des dépendances 
## 1-Les services : 
Un service permet de centraliser des parties de votre code et des données qui sont utilisées par plusieurs parties de votre application ou de manière globale par l'application entière. 

=>Les services permettent donc :

- de ne pas avoir le même code doublé ou triplé à différents niveaux de l'application - ça facilite donc la maintenance, la lisibilité et la stabilité du code ;

- de ne pas copier inutilement des données - si tout est centralisé, chaque partie de l'application aura accès aux mêmes informations, évitant beaucoup d'erreurs potentielles.

 => Service est :
- classe TypeScript decor ´ ee par ´ @Injectable

=>pouvant avoir le role de :
- l’intermediaire avec la partie back-end ´
- un moyen de communication entre composants 
- injectable dans les classes ou on a besoin de l’utiliser `
-  pouvant utiliser un ou plusieurs autres services

## 2- Créer un service : 
Pour créer un service :

>**ng generate service nom-service** 
 
ou bien 
> **ng g s nom-service**

=> Le contenu de fichier nom.service.ts est le suivant :
 
````
import { Injectable } from ’@angular/core’;
@Injectable({
providedIn:’root’
} )
export class PersonneService {
constructor() { }
}
````
=> Ce paramétrage permet de configurer le service pour que le provider soit le module root. L’instance du service est un singleton injectable dans toute l’application.

Pour injecter un service, il faut:

- Quel sera le provider de ce service: le module root, module importé ou un composant. Suivant le choix du provider, une nouvelle instance du service ou un singleton sera utilisé à chaque injection.

- Suivant le provider choisi, il faut choisir la syntaxe à utiliser:
    - Décorateur **@Injectable()** avec le paramètre **providedIn**.
    - Paramètre **providers** dans le décorateur **@Component()** ou **@NgModule()** + décorateur **@Inject()**.
    - Paramètre **providers** dans le décorateur **@Component()** ou **@NgModule()** + décorateur **@Injectable()**.
    
## 3- Injection des dépendances : 

L'injection de dépendances (DI) est un modèle de conception par lequel des dépendances ou des services sont transmis aux objets ou aux clients qui en ont besoin. L'idée derrière ce modèle est d'avoir un objet séparé pour créer la dépendance requise et la transmettre au client. Cela permet à une classe ou à un module de se concentrer sur la tâche pour laquelle il est conçu et d'éviter les effets secondaires lors du remplacement de cette dépendance
### En utilisant le paramètre “providedIn” dans @Injectable()
- Le décorateur @Injectable() est utilisé pour indiquer qu’un objet est injectable en utilisant l’injection de dépendances. On précise son utilisation plus bas. Ce décorateur permet d’indiquer directement le provider d’un objet à injecter en utilisant le paramètre providedIn.

Pour déclarer en utilisant le paramètre providedIn, la déclaration est:
````typescript
@Injectable({
providedIn: 'root'
})
class Dependency {}
````
Dans ce cas, le provider sera le module root et l’instance injectée est un singleton.

Pour indiquer un module en tant que provider, on indique le type du module, la déclaration est du type:
````typescript
import { ItemModule } from '../item.module.ts';

@Injectable({
providedIn: ItemModule
})
class Dependency {}
````
### Exemples d’implémentation de l’injection : 
Pour implémenter l’injection, après avoir précisé le provider, il faut indiquer qu’un objet est injectable en utilisant les décorateurs:

@Inject() utilisable sur les paramètres du constructeur d’un composant ou

@Injectable() utilisable sur la classe de l’objet à injecter.