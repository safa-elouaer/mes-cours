# Reactive form 
Angular offre une approche originale et efficace nommée "Reactive Forms" présentant les avantages suivants :

- La logique des formulaires se fait dans le code TypeScript. Le formulaire devient alors plus facile à tester et à générer dynamiquement.
- Les "Reactive Forms" utilisent des Observables pour faciliter et encourager le "Reactive Programming".

Les formulaires réactifs sont des formulaires dans lesquels nous définissons la structure du formulaire dans la classe de composant.

Nous créons le modèle de formulaire avec des groupes de formulaires, des contrôles de formulaire et des tableaux de formulaires. Nous définissons également les règles de validation dans la classe de composant. Ensuite, nous le lions au formulaire HTML dans le modèle. Ceci est différent des formulaires basés sur des modèles, où nous définissons la logique et les contrôles dans le modèle HTML.
## Comment utiliser les formulaires réactifs
1- Importer ReactiveFormsModule

2- Créer un modèle de formulaire dans une classe de composants à l'aide de groupes de formulaires, de contrôles de formulaires et de tableaux de formulaires

3- Créez le formulaire HTML ressemblant au modèle de formulaire.

4- Lier le formulaire HTML au modèle de formulaire

## 1-Importer ReactiveFormsModule

Pour travailler avec des formulaires réactifs, nous devons importer le fichier ReactiveFormsModule. Nous l'importons généralement dans un module racine ou dans un module partagé. Le ReactiveFormsModulecontient toutes les directives et constructions de formulaire pour travailler avec des formes réactives angulaires.

````ts 
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
 
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
````
## 2- Modèle   
Dans l'approche basée sur les modèles, nous avons utilisé une **directive ngModel** & **ngModelGroup** sur les éléments HTML. Les instances & FormsModule créées à partir du modèle. Cela se produit dans les coulisses. FormGroup FormControl

Dans l'approche des formulaires réactifs, il est de notre responsabilité de construire le modèle en utilisant **FormGroup**, **FormControl**et **FormArray**.

Les **FormGroup**, **FormControl** & **FormArray** sont les trois éléments constitutifs des formes angulaires. Nous avons appris à leur sujet dans le didacticiel sur les formes angulaires.


**FormControl** encapsule l'état d'un seul élément de formulaire dans notre formulaire. Il stocke la valeur et l'état de l'élément de formulaire et nous aide à interagir avec eux à l'aide de propriétés et de méthodes.

**FormGroup** représente une collection de contrôles de formulaire. Il peut également contenir des groupes de formulaires et des tableaux.
En fait, une forme angulaire est un **FormGroup**.

Créons le modèle de notre formulaire.

Tout d'abord, nous avons besoin d'importer **FormGroup**, **FormControl** et **Validator** de la @angular/forms.
Ouvrez l'app.component.ts instruction d'importation et ajouter suivant.

````ts
import { FormGroup, FormControl, Validators } from '@angular/forms'
````
### FormGroup :
FormGroup permet de regrouper des "controls" afin de faciliter l'implémentation, récupérer la valeur groupée des "controls" ou encore appliquer des validateurs au groupe.

Le FormGroup est construit avec un "plain object" associant chaque propriété à un FormControl .
Il est alors possible d'accéder aux valeurs et aux "controls" via la propriété associée.

###### Syntaxe : 
````ts
contactForm = new FormGroup({})
````
Le FormGroup prend 3 arguments. Une collection d'un enfant FormControl, a validator et un asynchronous validator. Les validateurs sont facultatifs.

#### FormControl
Le premier argument de FormGroup est la collection de FormControl. 
Ils sont ajoutés en utilisant la FormControl méthode indiquée ci-dessous
````ts
contactForm = new FormGroup({
firstname: new FormControl(),
lastname: new FormControl(),
email: new FormControl(),
gender: new FormControl(),
isMarried: new FormControl(),
country: new FormControl()
})
````
Dans ce qui précède, nous avons créé une instance de a FormGroup et l'avons nommée contactForm.

**contactForm** est notre niveau le plus élevé FormGroup. Sous le **contactForm**, nous avons cinq **FormControl** instances représentant chacune les propriétés firstname lastname. email, gender, ismarriedEt country.

Les deux autres arguments FormGroup sont Sync Validator& Async Validator. Ils sont facultatifs.
 
## 3- Formulaire HTML

La tâche suivante consiste à créer un formulaire HTML. Ce qui suit est un HTML standard form. 

Nous l'enveloppons dans une <form>balise. Nous avons inclus deux entrées de texte (Prénom et Nom), un champ de courrier électronique, un bouton radio (sexe), une case à cocher (isMarried) et une liste de sélection (pays). Ce sont des éléments de formulaire.
````html
<form>

  <p>
    <label for="firstname">First Name </label>
    <input type="text" id="firstname" name="firstname">
  </p>

  <p>
    <label for="lastname">Last Name </label>
    <input type="text" id="lastname" name="lastname">
  </p>

  <p>
    <label for="email">Email </label>
    <input type="text" id="email" name="email">
  </p>

  <p>
    <label for="gender">Geneder </label>
    <input type="radio" value="male" id="gender" name="gender"> Male
    <input type="radio" value="female" id="gender" name="gender"> Female
  </p>

  <p>
    <label for="isMarried">Married </label>
    <input type="checkbox" id="isMarried" name="isMarried">
  </p>

  <p>
    <label for="country">country </label>
    <select id="country" name="country">
      <option [ngValue]="c.id" *ngFor="let c of countryList">
        {{c.name}}
      </option>
    </select>
  </p>

  <p>
    <button type="submit">Submit</button>
  </p>

</form>
 ````
## 4- Lier le modèle au modèle :
Nous devons maintenant associer notre modèle au modèle. Nous devons dire à angular que nous avons un modèle pour la forme.

Ceci est fait en utilisant la formGroup directive comme indiqué ci-dessous 
````html
<form [formGroup]="contactForm">
````
Nous avons utilisé le crochet carré (liaison unidirectionnelle) autour de la **FormGroup directive** et défini ce qui équivaut au modèle.


Ensuite, nous devons lier les champs de formulaire aux **FormControl modèles**. Nous utilisons la FormControlName directive pour cela.

Nous ajoutons cette directive à chaque élément de champ de formulaire dans notre formulaire. La valeur est définie sur le nom de l' FormControl instance correspondante dans la classe de composant.
````html
<input type="text" id="firstname" name="firstname" formControlName="firstname">
<input type="text" id="lastname" name="lastname" formControlName="lastname">
````
## Soumettre le formulaire

Nous soumettons les données du formulaire au composant à l'aide de la directive Angular nommée **ngSubmit**.

Notez que nous avons déjà un submit bouton dans notre formulaire. La **ngSubmit** directive se lie à l'événement click du submit bouton.
Nous utilisons la liaison d'événement (parenthèses) pour nous lier **ngSubmit** à la **OnSubmit** méthode.
Lorsque l'utilisateur clique sur le submit bouton **ngSubmit** appelle la **OnSubmit** méthode sur la classe Component
````html
<form [formGroup]="contactForm" (ngSubmit)="onSubmit()">
````
## Modèle final
Notre modèle final est comme indiqué ci-dessous
````html
<form [formGroup]="contactForm" (ngSubmit)="onSubmit()">

  <p>
    <label for="firstname">First Name </label>
    <input type="text" id="firstname" name="firstname" formControlName="firstname">
  </p>

  <p>
    <label for="lastname">Last Name </label>
    <input type="text" id="lastname" name="lastname" formControlName="lastname">
  </p>

  <p>
    <label for="email">Email </label>
    <input type="text" id="email" name="email" formControlName="email">
  </p>

  <p>
    <label for="gender">Geneder </label>
    <input type="radio" value="male" id="gender" name="gender" formControlName="gender"> Male
    <input type="radio" value="female" id="gender" name="gender" formControlName="gender"> Female
  </p>

  <p>
    <label for="isMarried">Married </label>
    <input type="checkbox" id="isMarried" name="isMarried" formControlName="isMarried">
  </p>

  <p>
    <label for="country">country </label>
    <select id="country" name="country"  formControlName="country">
      <option [ngValue]="c.id" *ngFor="let c of countryList">
        {{c.name}}
      </option>
    </select>
  </p>

  <p>
    <button type="submit">Submit</button>
  </p>

</form>
 ````

---------------------------------------

Lien de vidéo  

[023. Angular - Reactive Forms - PARTIE 1 ](https://youtu.be/uTTxz-xCpew)

[023.1. Angular - Reactive Forms - PARTIE 2 ]( https://youtu.be/iMuQEK9TEhc)