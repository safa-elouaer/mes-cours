# Structure directive ngIf

Les directives sont des instructions intégrées dans le DOM que vous utiliserez presque systématiquement quand vous créerez des applications Angular.  Quand Angular lit votre template et rencontre une directive qu'il reconnait, il suit les instructions correspondantes
Il existe deux types principaux de directive : les **directives structurelles** et les **directives par attribut**.

## Les directives structurelles :

Ce sont des directives qui, comme leur nom l'indique, modifient la structure du document.
Nous  allons en découvrir deux (il en existe d'autres) :  *ngIf  , pour afficher des données de façon conditionnelle, et  *ngFor , pour itérer des données dans un array, par exemple.

=>Les directives structurelles sont responsables de la mise en page HTML.

Ils façonnent ou remodèlent la structure du DOM , généralement en ajoutant, supprimant ou manipulant des éléments.

Comme avec les autres directives, vous appliquez une directive structurelle à un élément hôte . La directive fait alors tout ce qu'elle est censée faire avec cet élément hôte et ses descendants.
###1- ngIf:
Un component auquel on ajoute la directive  *ngIf="condition"  ne s'affichera que si la condition est "truthy" (elle retourne la valeur  true  où la variable mentionnée est définie et non-nulle), comme un statement  if  classique.
#### Syntaxe :
````html
<div *ngIf="hero" class="name">{{hero.name}}</div>
````
#### Exemple : 
Pour une démonstration simple, ajoutez une  <div>  rouge qui ne s'affichera que si l'appareil est éteint :
````html
<li class="list-group-item">
  <div style="width:20px;height:20px;background-color:red;" 
       *ngIf="appareilStatus === 'éteint'"></div>
  <h4>Appareil : {{ appareilName }} -- Statut : {{ getStatus() }}</h4>
  <input type="text" class="form-control" [(ngModel)]="appareilName">
</li>
````
![img](../image/img_6.png)
#### Exemple 2 : 
````html
<p *ngIf="isShown()">{{ User name}}</p>
<p *ngIf="true">{{ User name}}</p>
//un booléen, true l'élement s'affiche, false l'élément va se supprimer
<p *ngIf="show">{{ User name}}</p> //Avec une variable
````
````typascript
isShown(): boolean {
return this.show;
}
export class AppComponent {
show: boolean = true;
}
````
##2- ngFor :
NgForest une directive répéteur - un moyen de présenter une liste d'éléments. Vous définissez un bloc de HTML qui définit la façon dont un seul élément doit être affiché, puis vous indiquez à Angular d'utiliser ce bloc comme modèle pour le rendu de chaque élément de la liste. Le texte attribué à est l'instruction qui guide le processus du répéteur.*ngFor
### Syntaxe : 
````html
<div *ngFor="let item of items">{{item.name}}</div>
````
=>Lorsque l'on ajoute la directive  *ngFor="let obj of myArray"  à un component, Angular itérera l'array  myArray  et affichera un component par objet  obj

Il est possible de récupérer d'autre informations telles que l'index de l'élément :
index : position de l'élément.
- **odd** : indique si l'élément est à une position impaire.
- **even** : indique si l'élément est à une position paire.
- **first** : indique si l'élément est à la première position.
- **last** : indique si l'élément est à la dernière position

### Exemple : 
Pour en comprendre l'utilisation, je vous propose de modifier la façon dont votre application génère des appareils électriques.

On peut imaginer que votre application récupère, depuis un serveur, un array contenant tous les appareils et leurs états.  Pour l'instant, créez cet array directement dans  AppComponent  :
````typescript
export class AppComponent {
isAuth = false;

appareils = [
{
name: 'Machine à laver',
status: 'éteint'
},
{
name: 'Frigo',
status: 'allumé'
},
{
name: 'Ordinateur',
status: 'éteint'
}
];

constructor() {
}
}
````
Vous avez un array avec trois objets, chaque objet ayant une propriété  name  et une propriété  status .  Vous pourriez même créer une interface ou une class TypeScript  Appareil , mais dans ce cas simple ce n'est pas nécessaire.

Maintenant la magie  *ngFor  :
````html
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h2>Mes appareils</h2>
      <ul class="list-group">
        <app-appareil  *ngFor="let appareil of appareils"
                       [appareilName]="appareil.name"
                       [appareilStatus]="appareil.status"></app-appareil>
      </ul>
      <button class="btn btn-success"
              [disabled]="!isAuth"
              (click)="onAllumer()">Tout allumer</button>
    </div>
  </div>
</div>
````
Le statement  let appareil of appareils , comme dans une for loop classique, itère pour chaque élément  appareil  (nom arbitraire) de l'array  appareils  .  Après cette directive, vous pouvez maintenant utiliser l'objet  appareil , dont vous connaissez la forme, à l'intérieur de cette balise HTML.  Vous pouvez donc utiliser le property binding, et y passer les propriétés  name  et  status  de cet objet.
### Si je veux utiliser les deux ngIf et ngFor : 
````html
<ng-container *ngFor="let user of users; let id = index">
<div *ngIf="user.color">
   <div> {{ id }} - {{ user.name }} | {{ user.color }} </div>

<button (click)="deleteUser(id)">Supprimer</button>
</div>
</ng-container>
````
````html
deleteUser(user, id): void {
  let arr = [...this.users];
  arr.splice(id, 1);
  this.users = [...arr];

console.log(id);
}
````
=>Pour utiliser * ngFor et * ngIf ensemble,  il faut les séparer :(ex ngFor dans un container, et ngIf dans une div)

------------------------------
Lien de vidéo
[Structural directive ngIf](https://youtu.be/kuJLIVPito4)
[Structural directive ngfor](https://youtu.be/E2KwZMS6KyI)

