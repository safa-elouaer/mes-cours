# HTML
## Qu'est-ce que HTML :
HTML signifie Hyper Text Markup Language
HTML est le langage de balisage standard pour la création de pages Web
HTML décrit la structure d'une page Web
HTML se compose d'une série d'éléments
Les éléments HTML indiquent au navigateur comment afficher le contenu
Les éléments HTML étiquettent des éléments de contenu tels que "ceci est un titre", "ceci est un paragraphe", "ceci est un lien", etc.
### Exemple :
````html
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>//<title>élément spécifie un titre pour la page HTML
</head>
<body> //élément définit le corps du document 

<h1>My First Heading</h1>//élément définit un grand titre
<p>My first paragraph.</p>// <p>élément définit un paragraphe

</body>
</html>
````
## Que sont les balises HTML?
Les balises sont utilisées pour marquer le début d'un élément HTML et elles sont généralement placées entre crochets angulaires.

## Les balises
Les pages HTML sont remplies de ce qu'on appelle des balises. Celles-ci sont invisibles à l'écran pour vos visiteurs, mais elles permettent à l'ordinateur de comprendre ce qu'il doit afficher.
Les balises se repèrent facilement. Elles sont entourées de « chevrons », c'est-à-dire des symboles <  et >  , comme ceci : <balise>  .

À quoi est-ce qu'elles servent ? Elles indiquent la nature du texte qu'elles encadrent. Elles veulent dire par exemple : « Ceci est le titre de la page », « Ceci est une image », « Ceci est un paragraphe de texte », etc.

On distingue deux types de balises : les balises en paires et les balises orphelines.

### Les balises en paires
Elles s'ouvrent, contiennent du texte, et se ferment plus loin. Voici à quoi elles ressemblent :

#### Exemple : 
````html

<titre>Ceci est un titre</titre>
````
### Les balises orphelines :

Ce sont des balises qui servent le plus souvent à insérer un élément à un endroit précis (par exemple une image). Il n'est pas nécessaire de délimiter le début et la fin de l'image, on veut juste dire à l'ordinateur « Insère une image ici ».

Une balise orpheline s'écrit comme ceci :
#### Exemple : 
````html
<image />
````
### Les attributs 

Les attributs sont un peu les options des balises. Ils viennent les compléter pour donner des informations supplémentaires. L'attribut se place après le nom de la balise ouvrante et a le plus souvent une valeur, comme ceci :
````html
<balise attribut="valeur">
````
## La balise div 

**div** est un conteneur:

• Cette balise n’a aucune valeur sémantique (aucun « sens »)

• Élément de type bloc qui peut inclure n’importe quel autre balise
HTML

• Suivi d’un passage à la ligne

• Permet de regrouper d’autres blocs de HTML (paragraphes, tableaux, etc.) et de leur donner un style CSS commun
## Les titres 
Il existe six niveaux de titre h1,h2,h3, h4,h5 et h6 (pas un de plus ! ):

• Un nouveau niveau provoque un passage à la ligne, les éléments de titre étant des éléments de bloc.

• La hiérarchisation visuelle est automatique (peut être modifiée via CSS)

• Il est possible d’avoir plusieurs titres de même niveau

## Les éléments de liste :

### Liste non ordonnée

La balise **ul** permet de créer une liste non ordonnée (unordered list). 
Il faut l’ouvrir en début de liste et la fermer après le dernier élément de la liste
• La balise <li> permet de créer UN élément de la liste
#### Exemple :
![img](../image/img_38.png)

### Liste ordonnée 

La balise **ol** permet de créer une liste ordonnée et numérotée (par défaut dans le navigateur). Il faut l’ouvrir en début de liste et la fermer après le dernier élément de la liste.

• La balise **li** permet de créer UN élément de la liste

#### Exemple :
![img](../image/img_39.png)
