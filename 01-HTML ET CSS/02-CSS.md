# CSS
Les feuilles de style en cascade CSS (Cascading Style Sheets) est un langage informatique qui sert
à décrire la présentation des documents HTML, XHTML et XML. Les standards définissant
CSS sont publiés par le W3C (World Wide Web Consortium)
## OÙ CODER EN CSS
Nous allons maintenant quitter nos fichiers **.html** pour ouvrir **style.css**.

En effet, le CSS est à coder de préférence dans les fichiers type **.css**.

Avant toute chose, il va falloir dire à notre fichier .html où se trouve notre feuille de style .css. Pour cela, nous allons ajouter une ligne de code dans la balise <head> :
````html
<link rel="stylesheet" type="text/css" href="style.css" />
````
Cette balise <link> est autofermante. Elle contient 3 attributs :

- **rel** : qui défini la relation entre le fichier .html et le fichier lié. Ici, il s'agit de notre feuille de style.
  
- **type** : qui définit le type de document lié. Ici, il s'agit d'un fichier .css.
  
- **href** : permet d'indiquer le chemin vers notre fichier.

## Sélecteur 
La première chose à définir en CSS est le sélecteur. Il s'agit de l'élément auquel nous allons appliquer un style :
````css
p{}
````
Ici, je choisis de cibler les balises de type paragraphe. J'utilise donc le même sélecteur qu'en HTML : p.

Notez les crochets { et } qui suivent notre sélecteur. Ils définissent l'endroit où nous allons entrer les propriétés de style.

## LES PROPRIÉTÉS
Les propriétés permettent de choisir quels aspects nous allons styliser. Par exemple, il est possible de changer la couleur, la police de caractère, la taille, le fond, …
Ici, nous allons modifier la couleur de notre texte avec color:
````css
p{
color: white;
}
````
Les propriétés se mettent entre les crochets { et }, à la ligne avec une tabulation d'indentation.

La propriété est suivie de : qui permet de lui attribuer une valeur.
##LES VALEURS
Nous allons maintenant changer la couleur de notre paragraphe. pour cela, nous allons entrer une couleur (ici red) à notre propriété :
````css
p{
color: red;
}
````
=>La valeur doit se mettre après les :. Aussi, il faut un ; pour fermer la ligne et pouvoir passer une autre propriété.

## PLUSIEURS PROPRIÉTÉS ET SÉLECTEURS
Il est possible de définir plusieurs propriétés à un sélecteur. Pour cela, il faut les emplier, les uns à la suite des autres. Par exemple, nous allons chager la taille et la couleur de nos balises p :
````css
p{
color: red;
font-size: 16px;
}
````
Si vous devez styliser différents sélecteurs, vou pouvez les mettre les uns après les autres :
````css
p{
color: red;
font-size: 16px;
}
li{
color: green;
font-size: 14px;
}
````

#### Font-family : 
- Indiquer la (ou les) polices à utiliser body { font-family: Arial, Helvetica, sans-serif;}
  
- L’ordre des polices appliquées est celui de la propriété de gauche à droite

#### Text-align 
 Valeur permet d’aligner le texte à gauche, droite ou milieu
-  Les valeurs possibles sont : left, right, center, justify (comme sur Word)
- L’alignement se fait sur le contenu des balises de type bloc (paragraphes, titres, divs, etc.)
