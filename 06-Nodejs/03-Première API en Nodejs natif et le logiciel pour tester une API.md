# Première API en Nodejs natif

## Créer une première API :
On a la documentation Nodejs, dans la partie guide on a la partie Anatomy d'une transaction HTTP 
on trouve comment créer un serveur.

Pour créer un serveur web il faut utiliser la méthode **createServer()**

*Exemple* :

````js
const http = require('http');//inclure la méthode http(pour récupérer les docs HTTP)
const server = http.createServer((request, response) => {
    console.log('request')
  //La fonction transmise à createServerest appelée une fois pour chaque requête HTTP effectuée sur ce serveur, elle s'appelle donc le gestionnaire de requêtes
}).listen(8080, '127.0.0.1', function () {// pour configurer le serveur pour qu'il écoute et soit le port 8080, ce qui nous servira dans le cas de notre plateforme de développement et ️'127.0.0.1 '= adresse IP de l'ordinateur = localhost.
    console.log('le serveur est accessible sur http://localhost:8080')
});

````
Démarrez le serveur en exécutant node server à partir de la ligne de commande. Pour vérifier qu'il envoie la réponse correcte, utilisez une fenêtre de navigateur pour accéder à http://localhost:8080


## Logiciel pour tester API : 

Lorsqu'on developpe une API, il faut la tester et tester chaque route par les bons paramètres.

### Configuration d'un insomnia espace de travail :

1-Lancez l'Insomnia application.

2-Cliquez sur la liste déroulante en regard Insomnia et sélectionnez Nouvel espace de travail :

![img](../image/img_31.png)
3- Entrez le nom de votre API et cliquez sur Créer

4- Faire une GET demande :
Cliquez sur l'icône du menu déroulant de votre nouveau dossier, puis sélectionnez Nouvelle demande(New Request)
5- Remplacez l'URL de requête générique par votre requête.

#### Faire une POST demande :
1- Utilisez la même URL que vous l'avez fait pour les étapes de requête GET ci-dessus, mais choisissez maintenant POST d'être la méthode HTTP sélectionnée.

2- Cliquez sur le corps pour développer le menu déroulant et sélectionnez JSON
![img](../image/img_32.png)
=>Pour les données Body , entrez le code JSON suivant pour le corps (la capture d'écran suivant le JSON montre comment la requête doit apparaître) :
*Exemple* : 
![img](../image/img_33.png)

3- Cliquez sur Envoyer.



------------------------
Lien de vidéo :

[003. API - Logiciel pour tester une api]( https://youtu.be/tTbCkY0oIaA)

[004. API - Première api en NodeJs natif ]( https://youtu.be/bVPb6rYVp1E)


