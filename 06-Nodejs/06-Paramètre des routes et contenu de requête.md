# Paramètre des routes et contenu de requête

````js
const express=require('express');
const server=express();
server.use(express.json());//parser le JSON donc les payload body
server.get('/',(request,response)=>{
    response.send('<h1>Hello Word!</h1>');
});
server.get('/users',(request,response)=>{
    const users=[];
    response.send({users});
});
server.post('/users',(request,response)=>{
console.log(request.body.user);
response.statusCode=404;
    response.send({message:'user saved',user:'Sam',req:request.body.user});
});
//put /users/:id
server.put('/users/:id',(request,response)=>{
    response.send({message:'User updated successfully!',user:request.params.id,data:request.body})
});
//DELETE /users/:id
server.delete('/users/:id',(request,response)=>{
    console.log(request.params);
    // vérifie que l'id corresponds à un utilisateur dans la base de données
    const userExist=false;
    if(userExist===false){
        response.status(404).send('User not found : '+ request.params.id)
    }
    response.status(201).send({message:'user successfully deleted!',user:request.params.id})
});
server.listen(8080,()=>{
    console.log('Server running on http:://localhost:8080');
});
````

-----------------------
Lien des vidéos

[007. API - Paramètre de route et contenue de la requête](https://youtu.be/RRYAlnX0vGU)

[008. API - Gérer les erreurs et changer le status de la réponse](https://youtu.be/8tr82LyH46Q)