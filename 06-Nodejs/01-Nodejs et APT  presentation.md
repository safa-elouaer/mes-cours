# Nodejs et Api
## 1- Node js

Node est le **runtime** qui permet **d'écrire toutes nos tâches côté serveur**, en **JavaScript**, telles que la logique métier, la persistance des données et la sécurité. Node ajoute également des fonctionnalités que le JavaScript du navigateur standard ne possède pas, comme par exemple l'accès au système de fichiers local.

=> **un moteur d'exécution javascript qui permet d'interpréter du javascript coté serveur**

## 2- Api :
###  presentation :

**API** est une abréviation et signifie **Application Programming Interface** (ou **interface de programmation d’application**, en français). Pour faire simple : c’est un moyen de communication entre deux logiciels, que ce soit entre différents composants d’une application ou entre deux applications différentes.


### Fonctionnement des Api :
Les **API** permettent la **communication entre de nombreux composants différents de votre application, mais aussi entre des composants de votre application et d’autres développeurs**. Elles agissent ainsi comme un intermédiaire qui transmet des messages à travers un système **de requêtes et de réponses**.

![img](../image/img_25.png)

#### Communication entre client et serveur :

##### Serveur :
Un serveur est un logiciel qui fournit un service à d’autres logiciels. Bien souvent, il fournit ce service via le réseau informatique mais il peut aussi être installé sur la même machine que les clients.

=>Des serveurs : ce sont des ordinateurs spéciaux (souvent très puissants) qui envoient les sites web aux clients. Les serveurs "possèdent" les sites web et les distribuent à ceux qui veulent les visiter.
##### Client :
 C’est vous, votre ordinateur qui sert à aller consulter des sites web.


La **communication entre client et serveur** : le client formule une requête (ou une demande) pour obtenir une information et le serveur envoie une réponse contenant les données demandées si cela est possible.

![img](../image/img_26.png)

### Résumé :

=>Les API permettent de communiquer des données.

=>Elles permettent la communication entre différents composants de votre application et entre votre application et d’autres développeurs, par l’utilisation de requêtes et de réponses.

=>Elles donnent un moyen d’accès aux données de façon réutilisable et standardisée.

-----------------------------
Lien de vidéo 

[001. API - Présentation]( https://youtu.be/qPSx0CHKbUM)


