# Express 
## 1-Présentation :
 Express est **le framework** Web Node le plus populaire, et est la bibliothèque sous-jacente pour un certain nombre d'autres frameworks Web Node populaires.

 Express est un cadre d'application Web Node.js minimal et flexible qui fournit un ensemble robuste de fonctionnalités pour développer des applications Web et mobiles.
 
Il facilite le développement rapide d'applications Web basées sur les nœuds. Voici quelques-unes des fonctionnalités principales du framework Express -

- Permet de configurer des middlewares pour répondre aux requêtes HTTP.

- Définit une table de routage qui est utilisée pour effectuer différentes actions basées sur la méthode HTTP et l'URL.

- Permet de rendre dynamiquement les pages HTML en passant des arguments aux modèles.

## 2-Installation :
1- Tapez la CLI  :
> npm init

=>Utilisez la commande npm init va vous permet de créer un fichier package.json pour votre application

2- Pour ajouter Express à votre projet, exécutez la commande suivante à partir de votre dossier:

>npm install --save express

3- N'oubliez pas de créer  un fichier **.gitignore** avec les fichiers et mettre les fichiers que vous ne voulez pas les récupérer sur Gitlab tel que :
>/node_modules
> 
>/.idea


-----------------
Lien de vidéo 

[005. API - Présentation et installation de ExpressJs ](https://youtu.be/Mupb9Ku7r3w)