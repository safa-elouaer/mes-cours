# HTTP

## HTTP : Hyper Text Transfer Protocol
Le protocole qui permet d’échanger des pages web entre le client et le serveur.

C'est un protocol qui permet de transmettre des documents hypermédia comme le html 

=> Il est conçu pour faire la communication entre un navigateur web et un serveur web.

1- Le client, le navigateur web va envoyer une requête **http** ce qui va ouvrir la connection entre le navigateur web et le serveur .

2- Au bout d'un certains temps le serveur va renvoyer une donnée ou une réponse et cette connexion **http** va être couper

=> Un client est un logiciel capable d'émettre une requête HTTP, et qu'un serveur est un logiciel capable de recevoir des requêtes HTTP pour finalement rendre une réponse HTTP. 

Complétons un peu ce paradigme : lorsque l'on fait appel à une API, en général c'est au travers d'une application.

Ainsi, le client est en fait une autre application web capable d'émettre une requête HTTP (peu importe dans quel langage cette application est développée).

![img](../image/img_28.png)


### HTTP est protocole sans état : 
Cela ne veut pas dire qu'il ne peut y avoir d'enregistrement en base de données. Il s'agit simplement d'avoir en tête que l'API ne garde pas d'historique d'une requête à l'autre. L'API n'est là que pour répondre aux requêtes quand elle lui arrive sans a priori.

(c-à-d entre deux connection effectuées par le même serveur ne vont pas partager leurs états)

###  La structure des requêtes : 

Une **requête HTTP** émane d'un client (tout logiciel dans la capacité de forger une requête). 

Une requête est constituée des éléments suivants :

1- La première ligne (request line) doit contenir :

  - la méthode HTTP ( GET ,  POST ,  PUT ,  PATCH ,  DELETE ,  OPTIONS ,  CONNECT ,  HEAD ou  TRACE )

  - l'URI, c'est-à -dire ce qu'il y a après le nom de domaine (exemple : /users/1)

  - la version du protocole (exemple : HTTP/1.1)

2-Les entêtes (headers), une entête par ligne, chaque ligne finie par le caractère spécial "retour à la ligne" (CRLF)

3-Le contenu de la requête (body), doit être séparé de deux caractères spéciaux "retour à la ligne" ( CRLF CRLF ) - optionnel

![img](../image/img_27.png)

Chaque requête a une structure spécifique qui a cette forme :

Verbe HTTP + URI + Version HTTP + Headers + Body (facultatif)

#### Verve HTTP : 
Les verbes HTTP sont des types d’actions que l’on peut faire lors de la formulation d’une requête.

Les **verbes HTTP** correspondent à différents types d’action que vous pouvez accomplir avec votre requête.



#### Les méthodes HTTP :

![img](../image/img_29.png)

##### GET (obtenir): 
Utilisée pour **récupérer des informations** en rapport avec l'URI ; il ne faut en aucun cas modifier ces données au cours de cette requête.

Cette méthode est dite safe (sécuritaire), puisqu'elle n'affecte pas les données du serveur. 

Elle est aussi dite idempotent, c'est-à-dire qu'une requête faite en GET doit toujours faire la même chose (comme, renvoyer une liste d'utilisateurs à chaque fois que la requête est faite - d'une requête à l'autre, on ne renverra pas des produits si le client s'attend à une liste d'utilisateurs !).
##### Post (publier) :
Utilisée pour créer une ressource. Les informations (texte, fichier…) pour créer la ressource sont envoyées dans le contenu de la requête. Cette méthode n'est ni safe, ni idempotent.

=>Pour soumettre des données au serveur
##### PUT (mettre):

Utilisée pour remplacer les informations d'une resource avec ce qui est envoyé dans le contenu de la requête. Cette méthode n'est ni safe, ni idempotent.
=>Pour enregistrer des modifications 

##### Delete (supprimer):
Utilisée pour supprimer une ou plusieurs ressources. Les ressources à supprimer sont indiquées dans l'URI.

=>Pour supprimer un élément de la base des données
#### Réponse HTTP
Une réponse HTTP émane d'un serveur (tout logiciel dans la capacité de forger une réponse HTTP). Une réponse est constituée des éléments suivants :

1-La première ligne (status line) doit contenir :

- la version du protocole utilisée

- le code status

- l'équivalent textuel du code status

2-Les entêtes (headers)

3-Le contenu de la réponse (body)

![img](../image/img_30.png)

#### Code status :
Il existe cinq catégories de code status :
- 1xx : les informations
-  2xx : les succès
- 3xx : les redirections
- 4xx : les erreurs client
-  5xx : les erreurs serveur

**Exemples**:

**200** OK • Tout s'est bien passé ;

**201** Created • La création de la ressource s'est bien passée (en général le contenu de la nouvelle ressource est aussi renvoyée dans la réponse, mais ce n'est pas obligatoire - on ajoute aussi un header Locationavec l'URL de la nouvelle ressource) ;

**204** No content • Même principe que pour la 201, sauf que cette fois-ci, le contenu de la ressource nouvellement créée ou modifiée n'est pas renvoyée en réponse ;

**304** Not modified • Le contenu n'a pas été modifié depuis la dernière fois qu'elle a été mise en cache ;

**400** Bad request • La demande n'a pas pu être traitée correctement ;

**401** Unauthorized • L'authentification a échoué ;

**403** Forbidden • L'accès à cette ressource n'est pas autorisé ;

**404** Not found • La ressource n'existe pas ;

**405** Method not allowed • La méthode HTTP utilisée n'est pas traitable par l'API ;

**406** Not acceptable • Le serveur n'est pas en mesure de répondre aux attentes des entêtes  Accept . En clair, le client demande un format (XML par exemple) et l'API n'est pas prévue pour générer du XML ;

**500** Server error • Le serveur a rencontré un problème.

---------------------
Lien video 
[002. API - Hypertext Transfert Protocol (HTTP)](https://youtu.be/jLyLpQQjemg)




