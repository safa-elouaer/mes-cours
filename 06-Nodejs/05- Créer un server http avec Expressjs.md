# Créer un server HTTP avec Expressjs

Après avoir installé express il faut créer un fichier index.js et tapper ce code là:
````js
const express = require('express');

const server = express();
````
Pour activer notre server :
````js
server.listen(8080, () => {
console.log('Server running on http://localhost:8080')
});
````
Pour exécuter notre serveur dans le terminal :
>node index.js

Pour créer une route :
````js
server.get('/', (request,response) => {
response.send('<h1>hello world</h1>');
});
````
**Exemple de création d'un serveur avec Expressjs** : 

````js
const express = require('express');
//Récuperer la libraire express

const server = express();
//Créer un serveur 

server.get('/', (request,response) => {
    response.send('<h1>hello world</h1>');
});
//Créer une route

server.get('/users', (request,response) => {
    const users = [];
    response.send({users});
});
//Créer une nouvelle route

server.post('/users', (request,response) => {
    response.send({message: 'User saved', user: 'Sam'});
});

server.listen(8080, () => {
    console.log('Server running on http://localhost:8080')
});
// activer le serveur
````




------------------
Lien de vidéo
[006. API - Créer un server http avec ExpressJs ](https://youtu.be/zf3vSx5t5IU)