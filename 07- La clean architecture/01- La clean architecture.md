# La clean architecture 

## Qu'est-ce que la clean architecture 

La Clean Architecture vise à réduire les dépendances de votre logique métier avec les services que vous consommez (API, Base de données, Framework, Librairies tierces), pour maintenir une application stable au cours de ses évolutions, de ses tests mais également lors de changements ou mises à jour des ressources externes.

![img](../image/img_40.png)
=>Le schéma ci-dessus résume brièvement l’organisation et le fonctionnement d’une clean architecture. On distingue une découpe selon différentes couches.  Il faut comprendre que plus on s’approche du centre du cercle plus votre logiciel devient de haut niveau. Les couches internes ne doivent pas être conscientes des couches externes, afin de ne pas être impactées.

Les règles à suivre lorsque l’on implémente une clean architecture
Lors de l’implémentation de cette architecture il existe des règles, ayant toutes pour maître-mot « l’indépendance ».

## La logique que vous implémentez doit :

- **Être indépendante des frameworks** : les frameworks et librairies doivent être des outils, sans pour autant vous contraindre.
- **Être testable indépendamment** : les tests doivent pouvoir être réalisés sans éléments externes (interface utilisateur, base de données ...)
- **Être indépendante de l’interface utilisateur** : l’interface utilisateur doit pouvoir changer de forme (console, interface web ...)
- **Être indépendante de la base de données** : il doit être possible de changer de SGBD. 
- **Être indépendante de tout service ou système externe** : en résumé elle ne doit pas avoir conscience de ce qui l’entoure.

=> Une bonne architecture doit être :
- **Indépendante des frameworks utilisés** : L’architecture ne doit pas dépendre de l’existence de telle ou telle bibliothèque.
Ces frameworks doit juste être vus comme des outils.
- **Testable**. Les règles métiers doivent pouvoir être testées sans l’UI, la base de données, le serveur , etc.
  
- **Indépendante de l’UI** :L’UI doit pouvoir changer facilement sans modifier le reste de l’application. Une interface web doit pouvoir facilement être remplacée par une interface en ligne de commande par exemple sans avoir à modifier les règles métiers.
  
- **Indépendante de la base de données utilisée** : On doit pouvoir changer de base de données facilement, les règles métiers ne sont pas liées au type de base de données, elle doit rester un détail d’implémentation.
  
- **Indépendante de tout agent extérieur**: Les règles métiers n’ont aucune idée de ce qui se passe à l’extérieur.

Robert C Martin (a.k.a Uncle Bob) a écrit sur le sujet en 2012 en présentant ce qu’il appelle la Clean Architecture. C’est une architecture qui se divise en couches bien distinctes. Plus la couche est proche du centre, plus elle représente une haute abstraction des règles métiers, plus on s’éloigne du centre et plus on retrouve les détails d’implémentation. Il expose une règle très précise de gestion des dépendances entre les couches : une couche ne doit rien connaître des couches qui lui sont externes, ainsi l’architecture respecte la règle d’inversion de dépendances statuant que :
- Les modules de haut niveau ne doivent pas dépendre de modules de plus bas niveau. Les deux doivent dépendre d’abstractions.
- Les abstractions ne doivent pas dépendre de détails. Les détails doivent dépendre des abstractions.
## Les différentes couches de la Clean architecture :

La Clean Architecture se divise en 4 couches. Chaque couche ne peut communiquer qu’avec une couche à un niveau inférieur. C’est-à-dire qu’il ne doit y avoir aucune référence au code des couches extérieures dans une couche intérieure.

### Entities
Les entities sont les objets contenant le plus haut niveau d’abstraction des règles métiers de l’application, voire de l’entreprise. Ils doivent donc pouvoir être utilisés d’une application à l’autre si nécessaire.
### Use Cases
Les use cases sont un petit peu l’équivalent des user stories en méthodologie agile. C’est ici que sont définis les règles métiers propres à l’application. Les use cases vont manipuler les entities pour implémenter la feature concernée.
### Interface Adapters (Controllers, Gateways, Presenters)
Cette couche consiste principalement en des adapaters qui vont convertir les données provenant des couches inférieurs, vers les couches supérieurs (qui sont les détails d’implémentation).
### Frameworks et Drivers (devices, web, db, external interfaces, ui)
Cette couche est celle dans laquelle se trouve tous les frameworks et autres bibliothèques utilisées. Peu de code est écrit dans cette couche, c’est généralement que du glue code, c’est-à-dire du code écrit juste pour “brancher” les éléments externes aux éléments de notre application.

