# Manipuler les relations

## Exemple 
````js
// GET /users :Récupère la liste de tous les utilisateur
server.get('/users', (request, response) => {

    User.findAll({
        include:[{model:Post,as:'posts'}]

    })
        .then((users)=>{
            response.send(users);
        })
        .catch((err)=>{
            response.statusCode=404;
            response.send('Liste introuvable:'+ err);
        });

});

````


---------------------
Lien de vidéo 

[013. Sequelize - manipuler les relations](https://youtu.be/x_oKE4DPVfI)