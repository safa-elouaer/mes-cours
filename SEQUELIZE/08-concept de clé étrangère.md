# Concept de clé étrangère

## Clé étrangère 
![img](../image/img_74.png)

Permet de gérer des relations entre plusieurs tables et garantissent la cohérence des données.

Il a pour fonction principale la vérification de l'intégrité de votre base.
=> Une clé étrangère est un attribut ou un groupe d'attributs d'une relation R1 devant apparaître comme clé primaire dans une relation R2 afin de matérialiser une référence entre les tuples de R1 et les tuples de R2.

Une clé étrangère d'un tuple référence une clé primaire d'un autre tuple.
## Clé primaire 
Clé primaire d'une table est une contrainte d'unicité composée d'une ou plusieurs colonnes et qui permet d'identifier de manière unique chaque ligne de la table. La clé primaire ne peut pas être null.

 ✅Seule une clé primaire peut être référencée par une clé étrangère, c'est même la seule fonction de la clé primaire : être la clé qui peut être référencée par les clés étrangères.

MySQL prend en charge les clés étrangères qui permettent le croisement des données liées entre les tables et les contraintes de clé étrangère qui aide à maintenir la cohérence de données associés.

Une relation de clé étrangère implique :

- une table parent qui contient les valeurs de la colonne initiale et une table enfant parent avec des valeurs qui reference les valeurs de colonnes parents.

- Une **contrainte** de clé étrangère est définie sur la table enfant

## Contraintes associées à des actions : 

- **ON DELETE** : qui permet de déterminer le comportement de MySQL en cas de suppression d'une référence.

- **ON UPDATE** :qui permet de déterminer le comportement de MySQL en cas de modification d'une référence.

- La liste des contraintes est la suivante:


- **RESTRICT** : est le comportement par défaut. Si l'on essaye de supprimer une valeur référencée par une clé étrangère, l'action est avortée et on obtient une erreur.

- **AUCUNE ACTION** : empêche la modification si elle casse la contrainte (comportement par défaut).

=> Cette équivalence de **RESTRICT** et **NO ACTION** est propre à MySQL. Dans d'autres SGBD, ces deux options n'auront pas le même effet (RESTRICT  étant généralement plus strict que NO ACTION).

  
- **CASCADE** : modifie également la valeur là où elle est référencée. répercute la modification ou la suppression d'une référence de clé étrangère sur les lignes impactées.


- **SET NULL** : met **NULL** partout où la valeur modifiée était référencée, fait en sorte que les données de la clé étrangère ayant perdu leur référence (à la suite d'une modification ou d'une suppression) soient mises à NULL


- **SET DEFAULT** : Comme son nom l'indique va SET la valeur des utilisateurs id avec la valeur par défaut de la colonne users id.

---------------------------
Lien de vidéo 

[011. Sequelize - concept clé étrangère ](https://youtu.be/CfSBPHxHAbA)