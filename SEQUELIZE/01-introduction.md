# Introduction
##  Introduction de Sequelize 
Sequelize est un **ORM** basé sur des promesses pour Node.js et io.js. Il prend en charge les
dialectes PostgreSQL, MySQL, MariaDB, SQLite et MSSQL et offre un support de transaction
solide, des relations, une réplication de lecture, etc.

=> Sequelize est un ORM pour node.js compatible avec différents moteurs de base de données comme Mysql, Sqlite…etc.

=> Pour exécuter une base de données relationnelle avec une application Node, Sequelize , “Un ORM de dialecte multi-SQL facile à utiliser pour Node.js” est une bonne option. Il permet à l’application de fonctionner avec une instance MySQL ou PostgreSQL et offre un moyen simple de mapper la représentation des entités dans la base de données vers JavaScript et inversement.

## ORM 
**ORM** :Object Relational Mapping, c'est un ensemble de classes permettant de manipuler les tableaux de base de données relationnel comme si s'agissaient d'objet. 

Un **ORM** est une interface d'accès à la base des données qui donnent les zones de ne plus travailler avec les requêtes SQL mais de manipuler des objets.

=> L'avantage de cette couche d'abstraction est qu'il n'y a pas plus besoin de dissocier de système de base de données utilisé c'est l'ORM qui a la charge de transformer les requêtes pour les rendre compatible avec la base de données.

=>
- L'ORM va s'appuyer sur des **modèles**.
  
- Un **modèle** représente une table de la base de données. 
![img](../image/img_72.png)

## Sequelize 

Sequelize est un ORM node.js basé sur les **promesses**.

Il est compatible avec **MySQL** et d'autre systèmes de gestion de bases de données.

Nous allons pouvoir connecter notre API à notre base de données MySQL pour stocker une liste des utilisateurs, ajouter, modifier, supprimer un utilisateur et récupérer des données.
=> L'idée est de nous simplifier les développements 

=>Nous utilisons des classes de sequelize pour réaliser ces requêtes.
## Installation 

- Aller sur le site http://sequelize.org

- Cliquer sur v6 Documentation pour accéder à la documentation version 6 de Sequelize

- Pour installer les dépendances sequelize on copie la commande :

> npm install --save sequelize

- Coller la commande CLI dans le terminal de mon projet
- La deuxième étape : C'est l'installation de driver par la CLI
>npm install --save mysql2

✅ Pour vérifier que j'ai bien installé Sequelize, aller dans node module et vérifier s'il ya sequelize.


------------------------
Lien de vidéo

[001. Sequelize - Introduction](https://youtu.be/hyV7A6mScpo)

[002. Sequelize - orm]( https://youtu.be/5ZHTB3k5lbA)

[003. Sequelize - sequelize ](https://youtu.be/3UujbFFSUX0)

[004. Sequelize - installation](https://youtu.be/w6_vEJ3JYV4)