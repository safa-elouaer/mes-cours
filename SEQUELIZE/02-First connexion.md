# First connexion

## Créer la connexion à MySQL
- Pour créer la connexion on instancie une connexion grâce à un mot clé **new** et passer les paramètres et les options.
- Pour importer notre dépendance sequelize :
````js
const {sequelize}=require('sequelize');

````
- Pour tester la connexion on utilise la méthode **authenticate()** en utilisant la constante sequelize
````js
sequelize.authenticate()//cette méthode envoie une promesse
````
- Pour exécuter mon fichier server.js (dans lequel j'ai ajouté les connexions) on compose cette CLI dans le terminal:
> node server.js

## Exemple 

````js
const express = require('express');
const cors = require('cors');

const {Sequelize, DataTypes} = require('sequelize');
// créer la connexion à Mysql
const sequelize = new Sequelize('app', 'root', '', {
    host: '127.0.0.1',
    dialect: 'mysql'
});
// Tester la connexion à Mysql
sequelize.authenticate().then(() => {
    console.log('Connection successful');
}).catch((error) => {
    console.error('Unable to connect to the database', error);


});
````
-------------------
Lien vidéo 
[005. Sequelize - first connection ](https://youtu.be/XO12rXPA5Pw)