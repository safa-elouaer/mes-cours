# Enregistrer un utilisateur 
Pour sauvegarder les données dans la bes de données, on va se rendre sur les routes de API (API/users)
avec la méthode POST (c'est cette méthode qui permet d'enregister un utilisateur)

=> Pour sauvegarder un utilisateur en base, j'utilise son modèle (User.create : la méthode create)

## Exemple 
````js
server.post('/users', (request, response) => {
    User.create ({
        firstname: request.body.firstName,
        lastname: request.body.lastName,
        email: request.body.email
    }).then((user)=>{
        response.send({message: '"Utilisateur enregistré !', user});
    }).catch((err)=>{
        response.statusCode=404;
        response.send({message:'Nous n\'avaons pas pu sauvegarder l\'utilisateur', err});

    });



});
````

----------------------
Lien de vidéo 

[008. Sequelize - enregistrer un utilisateur](https://youtu.be/OInF6uqRkLc)