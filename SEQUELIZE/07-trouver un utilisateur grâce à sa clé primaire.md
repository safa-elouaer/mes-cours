# Trouver un utilisateur grâce à sa clé primaire

Pour récupérer les utilisateurs à leur clé primaire, je vais implémenter la requête **get info** dans la route './users/:id'

=> on utilise la méthode :**User.findByPk**
## Exemple 

````js
server.get('/users/:id', (request, response) => {
    User.findByPk(request.params.id)
        .then(async (user)=>{

            if(!!user){
                //Créer un nouveau post à chaque requête
                const post=await Post.create({
                    title:'Title',
                    text:'Text'
                });
                // L'associé à l'utilisateur
                await user.setPosts([post]);
                const posts=await user.getPosts();
                response.send({user,posts});
            } else {
                response.statusCode=404;
                response.send({message:'Aucun utilisateur trouvé'});
            }
        })
        .catch((err)=>{
          response.statusCode=404;
          response.send('Une erreur est survenue :' + err);
        });

});
````

----------------
Lien de vidéo 

[010. Sequelize - trouver un utilisateur grace à sa clé primaire ](https://youtu.be/LqYS2sPjJHM)