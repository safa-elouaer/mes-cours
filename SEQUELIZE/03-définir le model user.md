# Définir le modèle user

# Model 
- Objets permettant d’accéder à une table.
- Disposent des noms de colonnes de la table. 
- Création des attributs createAt et updateAt automatique.
- Accesseurs pour accéder aux données du modèle.

=> on utilise la méthode **define()**

## Exemple 
````js
// Déclarer le model User
const User = sequelize.define('User', {
        firstname: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lastname: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        note: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        presence: {
            type: DataTypes.STRING,
            allowNull: true
        }

    }, {
        sequelize,
        tableName: 'users',
        createdAt:false,
        updateAt:false,
    }
);

````

-----------------
Lien de vidéo 

[006. Sequelize - définir le model user](https://youtu.be/p-vwZ2zpuIU)