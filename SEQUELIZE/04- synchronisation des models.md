 # Synchronisation de models
 La synchronisation des modèles permet de synchroniser le modèle à la table à laquelle elle est synchronisée.

=> Pour synchroniser notre modèle, on utilise la méthode **sync()**.

=> Pour synchroniser, nous utilisons notre modèle, la méthode **sync()** qui renvoie un boolean et pareil on utilise la méthode **catch** et **then**

=> **catch** :Pour rattraper les erreurs dans la méthode **sync**

- **alter** : va vérifier l'état actuel de la table , si on met pas **alter** ça va pas synchroniser la table
 ## Exemple 

````js
User.sync({
 alter:true
})
 .then(()=>{
 console.log('Sync User success');
})
 .catch((error)=>{
console.error('Sync User error:',error);
});
````
## Explication de promesse 
![img](../image/img_73.png)

## Synchroniser tous les modèles 
### Exemple 

````js
sequelize.sync({alter: true}).then(() => {
    console.log('Sync User success');
})
    .catch((error) => {
        console.log('Sync User error:', error);
    })

````

-------------------
Lien de vidéo 

[007. Sequelize - synchronisation des modeles]( https://youtu.be/oRKgDPYVndc)

