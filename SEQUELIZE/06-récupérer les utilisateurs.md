# Récupérer les utilisateurs
Pour récupérer les utilisateurs on va utiliser la route /users avec la méthode get .

- J'utilise la méthode **User.findAll()**

## Exemple 
````js
// GET /users :Récupère la liste de tous les utilisateur
server.get('/users', (request, response) => {

    User.findAll({
        include:[{model:Post,as:'posts'}]

    })
        .then((users)=>{
        response.send(users);
    })
        .catch((err)=>{
        response.statusCode=404;
        response.send('Liste introuvable:'+ err);
    });

});
````

----------------------

[009. Sequelize - récupéré les utilisateurs ]( https://youtu.be/o9UFktSzKpQ)