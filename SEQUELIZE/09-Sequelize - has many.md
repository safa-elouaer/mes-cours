# Has many
## Exemple 

````js
//Déclarer le modèle Post ,columns:title,text
const Post=sequelize.define('Post', {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    text: {
        type: DataTypes.STRING,
        allowNull: false,
    }
},{
    sequelize,
    tableName:'posts',
    createdAt:false,
    update:false,
});
// Définir la relation :User has many Post
User.hasMany(Post,{
    as:'posts',
    foreignKey:'fk_author_id',
    sourceKey:'id',
    constraints:true,
    onDelete:'cascade',

});

````

--------------------
Lien de vidéo 
[Sequelize - has many](https://youtu.be/jb0OfjXLwXQ)