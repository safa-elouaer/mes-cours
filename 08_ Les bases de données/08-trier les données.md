# Trier les données 


## Tri des données
Pour trier vos données, c'est très simple, il suffit d'ajouter ORDER BY tri  à votre requête (après les critères de sélection de WHERE  s'il y en a) et de remplacer "tri" par la colonne sur laquelle vous voulez trier vos données bien sûr.

La commande **ORDER BY** permet de trier les lignes dans un résultat d’une requête SQL. Il est possible de trier les données sur une ou plusieurs colonnes, par ordre ascendant ou descendant.
### Syntaxe 
````
SELECT colonne1, colonne2
FROM table
ORDER BY colonne1
````
=> Pour trier :
Il faut d'abord récupérer toutes les données avec toutes les colonnes après on ordonne 

**Exemple** :
  
````
SELECT *FROM app.products ORDER BY  price_ttc
````
❎Par défaut les résultats sont classés par ordre ascendant, toutefois il est possible d’inverser l’ordre en utilisant le suffixe DESC après le nom de la colonne. Par ailleurs, il est possible de trier sur plusieurs colonnes en les séparant par une virgule.
## Tri ascendant ou descendant

Pour déterminer le sens du tri effectué, SQL possède deux mots-clés :
- **ASC** pour **ascendant**
- **DESC** pour **descendant**.
  
Par défaut, si vous ne précisez rien, c'est un tri ascendant qui est effectué : du plus petit nombre au plus grand, de la date la plus ancienne à la plus récente, et pour les chaînes de caractères et les textes, c'est l'ordre alphabétique normal qui est utilisé.

Si, par contre, vous utilisez le mot DESC, l'ordre est inversé : plus grand nombre d'abord, date la plus récente d'abord, et ordre anti-alphabétique pour les caractères.

### Exemple :
````
SELECT colonne1, colonne2, colonne3
FROM table
ORDER BY colonne1 DESC, colonne2 ASC
````
### Exemple de cours :
![img](../image/img_63.png)

-------------------------------------------
Lien de vidéo 
[011. MySQL - trier les données avec order by ](https://youtu.be/5CsdFpj7Nyc)