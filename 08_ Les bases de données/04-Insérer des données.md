# Insérer des données 

Pour insérer des données dans une table, nous allons cette fois-ci utiliser l’instruction SQL INSERT INTO suivie du nom de la table dans laquelle on souhaite insérer une nouvelle entrée avec sa structure puis le mot clef VALUES avec les différentes valeurs à insérer.
=> Concrètement, la structure de la requête SQL va être la suivante :
````sql
INSERT INTO nom_de_table (nom_colonne1, nom_colonne2, nom_colonne3, …)
VALUES (valeur1, valeur2, valeur3, …)
````

## Insérer des données : 
- La première chose à faire c'est créer une nouvelle table
![img](../image/img_52.png)
- Une fois la table est créée, on la nomme et on crée des colonnes.
![img](../image/img_53.png)
- Clique droit sur la table créée (tags :nom du table) 
![img](../image/img_54.png)
- on remarque que notre table est vide alors on va faire une requête INSERT INTO pour insérer des données .

![img](../image/img_55.png)
- Après avoir composé notre requête on exécute 

=> Deux possibilités s'offrent à nous lorsque l'on veut insérer une ligne dans une table : soit donner une valeur pour chaque colonne de la ligne, soit ne donner les valeurs que de certaines colonnes, auquel cas il faut bien sûr préciser de quelles colonnes il s'agit.

### Insertion sans préciser les colonnes

Prenant l'exemple de table Animal est composée de six colonnes : id, espece, sexe, date_naissance, nom et commentaires.

Voici donc la syntaxe à utiliser pour insérer une ligne dans Animal, sans renseigner les colonnes pour lesquelles on donne une valeur (implicitement, MySQL considère que l'on donne une valeur pour chaque colonne de la table).
````sql
INSERT INTO Animal
VALUES (1, 'chien', 'M', '2010-04-05 13:43:00', 'Rox', 'Mordille beaucoup');
````
**Deuxième exemple** : cette fois-ci, on ne connaît pas le sexe et on n'a aucun commentaire à faire sur la bestiole.
````sql
INSERT INTO Animal
VALUES (2, 'chat', NULL, '2010-03-24 02:23:00', 'Roucky', NULL);
````
**Troisième et dernier exemple** : on donne **NULL** comme valeur d'**id**, ce qui en principe est impossible puisque id est défini comme **NOT NULL** et comme **clé primaire**.

Cependant, l'auto-incrémentation fait que MySQL va calculer tout seul comme un grand quel id il faut donner à la ligne (ici : 3).
`````sql
INSERT INTO Animal
VALUES (NULL , 'chat', 'F', '2010-09-13 15:02:00', 'Schtroumpfette', NULL);
`````
Vous avez maintenant trois animaux dans votre table :
![img](../image/img_56.png)

### Insertion en précisant les colonnes
Dans la requête, nous allons donc écrire explicitement à quelle(s) colonne(s) nous donnons une valeur. Ceci va permettre deux choses :

- On ne doit plus donner les valeurs dans l'ordre de création des colonnes, mais dans l'ordre précisé par la requête.

- On n'est plus obligé de donner une valeur à chaque colonne ; plus besoin de NULL  lorsque l'on n'a pas de valeur à mettre.
##### Exemple :
````sql
INSERT INTO Animal (espece, sexe, date_naissance)
VALUES ('tortue', 'F', '2009-08-03 05:12:00');
INSERT INTO Animal (nom, commentaires, date_naissance, espece)
VALUES ('Choupi', 'Né sans oreille gauche', '2010-10-03 16:44:00', 'chat');
INSERT INTO Animal (espece, date_naissance, commentaires, nom, sexe)
VALUES ('tortue', '2009-06-13 08:17:00', 'Carapace bizarre', 'Bobosse', 'F');
````
### Insertion multiple
Si vous avez plusieurs lignes à introduire, il est possible de le faire en une seule requête de la manière suivante :Il suffit just de mettre **la virgule** et **ajouter des parenthèses aux nouvelles valeurs ajoutées**
````sql
INSERT INTO Animal (espece, sexe, date_naissance, nom)
VALUES ('chien', 'F', '2008-12-06 05:18:00', 'Caroline'),
('chat', 'M', '2008-09-11 15:38:00', 'Bagherra'),
('tortue', NULL, '2010-08-23 05:18:00', NULL);
````
### Résumé 
=>Pour insérer des lignes dans une table, on utilise la commande
````sql
INSERT INTO nom_table [(colonne1, colonne2, ...)] VALUES (valeur1, valeur2, ...);
````
=>Si l'on ne précise pas à quelles colonnes on donne une valeur, il faut donner une valeur à toutes les colonnes, et dans le bon ordre.

=>Il est possible d'insérer plusieurs lignes en une fois, en séparant les listes de valeurs par **une virgule**.


--------------------------------
Lien de vidéo 

[007. MySQL - insérer des données insert into](https://youtu.be/4-mz-PwZvnM)