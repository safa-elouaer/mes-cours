# Mise à jour 
La commande **UPDATE** permet de modifier la valeur d'un ou plusieurs champs dans une table.
## Syntaxe 
````sql
UPDATE nom_table
SET col1 = val1 [, col2 = val2, ...]
[WHERE ...];
````
=> En SQL, c'est la commande **UPDATE** qui permet de modifier (ou mettre à jour) les données d'une table.

=>La clause **SET** est utilisée pour préciser la modification demandée,

=>et la clause **WHERE** les conditions d'application (critères) de cette modification.
### Exemple :
![img](../image/img_59.png)

![img](../image/img_60.png)

--------------------------------------------
Lien de vidéo 

[009. MySQL - mise à jour des données update ]( https://youtu.be/_vdYNhbX7Kw)
