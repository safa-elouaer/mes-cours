# Base de données

## Base de donnée 
La base de données (BDD) est un système qui enregistre des informations.

Une base de données informatique est un ensemble de données qui ont été stockées sur un support informatique, organisées et structurées de manière à pouvoir facilement consulter et modifier leur contenu.


## Les SGBD 

Les **SGBD** (systèmes de gestion de bases de données) sont les programmes qui se chargent du stockage de vos données.
Les plus connus sont, pour rappel :

- **MySQL** : libre et gratuit, c'est probablement le SGBD le plus connu. Nous l'utiliserons dans cette partie ;

- **MariaDB** : c'est un clone (on dit fork) de MySQL, que des gens ont voulu lancer depuis que MySQL a été racheté par... Oracle. MariaDB et MySQL sont donc quasiment le même SGBD.

- **PostgreSQL** : libre et gratuit comme MySQL, avec plus de fonctionnalités mais un peu moins connu ;

- **SQLite** : libre et gratuit, très léger mais très limité en fonctionnalités ;

- **Oracle** : utilisé par les très grosses entreprises ; sans aucun doute un des SGBD les plus complets, mais il n'est pas libre et on le paie le plus souvent très cher ;

- **Microsoft SQL Server** : le SGBD de Microsoft.

Nous on va s'intéresser de **MySQL** 

✅  le langage SQL est un standard, c'est-à-dire que quel que soit le SGBD que vous utilisez, vous vous servirez du langage SQL

## SQL 
SQL signifie langage de requête structuré, qui vous permet d'accéder et de manipuler des bases de données.
## Que peut faire SQL :
- SQL peut exécuter des requêtes sur une base de données
- SQL peut récupérer des données à partir d'une base de données
- SQL peut insérer des enregistrements dans une base de données 
- SQL peut mettre à jour les enregistrements dans une base de données
- SQL peut supprimer des enregistrements d'une base de données
- SQL peut créer de nouvelles bases de données
- SQL peut créer de nouvelles tables dans une base de données
- SQL peut créer des procédures stockées dans une base de données
- SQL peut créer des vues dans une base de données
- SQL peut définir des autorisations sur les tables, les procédures et les vues

## Utilisation de SQL dans votre site Web
Pour créer un site Web qui affiche les données d'une base de données, vous aurez besoin de :

Un programme de base de données SGBDR (c'est-à-dire MS Access, SQL Server, MySQL)
- Pour utiliser un langage de script côté serveur, tel que PHP ou ASP
- Pour utiliser SQL pour obtenir les données souhaitées
- Pour utiliser HTML / CSS pour styliser la page
## SGBDR 
SGBDR signifie système de gestion de base de données relationnelle.

Un Système de Gestion de Base de Données, c'est un logiciel.

Ce logiciel permet de manipuler des bases de données, au sein desquelles sont stockées des informations.
=> Lorsque ces bases de données suivent les règles du modèle relationnel, alors on les qualifie de bases de données relationnelles, et le SGBD qui les manipule devient alors un système de gestion de bases de données relationnelles, ou SGBDR

Le SGBDR est la base de SQL et de tous les systèmes de base de données modernes tels que MS SQL Server, IBM DB2, Oracle, MySQL et Microsoft Access.

Les données du SGBDR sont stockées dans des objets de base de données appelés tables. Une table est une collection d'entrées de données associées et se compose de colonnes et de lignes.


