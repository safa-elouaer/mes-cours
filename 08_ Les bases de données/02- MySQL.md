# MySQL

## SGBD 
Un Système de Gestion de Base de Données (SGBD) est un logiciel (ou un ensemble de logiciels) permettant de manipuler les données d'une base de données. Manipuler, c'est-à-dire sélectionner et afficher des informations tirées de cette base, modifier des données, en ajouter ou en supprimer (ce groupe de quatre opérations étant souvent appelé "CRUD", pour Create, Read, Update, Delete).
## MySQL 

est un système de gestion de bases de données.
## SQL 
SQL signifie langage de requête structuré, ce qui vous permet d'accéder et de manipuler des bases de données
## Le langage SQL
Le SQL (Structured Query Language) est un langage informatique qui permet d'interagir avec des bases de données relationnelles. C'est le langage pour base de données le plus répandu, et c'est bien sûr celui utilisé par MySQL. C'est donc le langage que nous allons utiliser pour dire au client MySQL d'effectuer des opérations sur la base de données stockée sur le serveur MySQL.

Il a été créé dans les années 1970 et c'est devenu standard en 1986 (pour la norme ANSI ; 1987 en ce qui concerne la norme ISO). Il est encore régulièrement amélioré.
## Le paradigme client-serveur 
La plupart des **SGBD** sont basés sur un modèle client-serveur. C'est-à-dire que la base de données se trouve sur un serveur qui ne sert qu'à ça, et pour interagir avec cette base de données, il faut utiliser un logiciel "client" qui va interroger le serveur et transmettre la réponse que le serveur lui aura donnée.

=> Le serveur peut être installé sur une machine différente du client ; c'est souvent le cas lorsque les bases de données sont importantes.

Lorsque vous installez un **SGBD** basé sur ce modèle (c'est le cas de MySQL), vous installez en réalité deux choses (au moins) : le serveur et le client. 

Chaque requête (insertion/modification/lecture de données) est faite par l'intermédiaire du client. Jamais vous ne discuterez directement avec le serveur (d'ailleurs, il ne comprendrait rien à ce que vous diriez).

Vous avez donc besoin d'un langage pour discuter avec le client, pour lui donner les requêtes que vous souhaitez effectuer.

=>Dans le cas de **MySQL**, ce langage est le **SQL**.

## SGBDR

Le **R** de **SGBDR** signifie "relationnel". Un **SGBDR** est un **SGBD** qui implémente la théorie relationnelle.

**MySQL** implémente la théorie relationnelle ; c'est donc un **SGBDR**.

Dans un **SGBDR**, les données sont contenues dans ce que l'on appelle des relations, qui sont représentées sous forme de tables.

Une relation est composée de deux parties, l'en-tête et le corps.

=>**L'en-tête** est lui-même composé de plusieurs **attributs**. 

=> Quant au **corps**, il s'agit d'un ensemble de **lignes (ou n-uplets)** composées d'autant d'éléments qu'il y a d'attributs dans le corps

![img](../image/img_42.png)
## Organisation d'une base de donnée 
On représente les données sous forme de tables. Une base va donc contenir plusieurs tables (elle peut n'en contenir qu'une, bien sûr, mais c'est rarement le cas).

Chaque table définit un certain nombre de colonnes, qui sont les caractéristiques de l'objet représenté par la table (les attributs de l'en-tête dans la théorie relationnelle).

=> Si je récapitule, dans une base, nous avons donc des tables, et dans ces tables, nous avons des colonnes. Dans ces tables, vous introduisez vos données. Chaque donnée introduite le sera sous forme de ligne dans une table, définissant la valeur de chaque colonne pour cette donnée.

## En résumé 
=>**MySQL** est un Système de Gestion de Bases de Données Relationnelles (SGBDR) basé sur le modèle client-serveur.

=>Le langage **SQL** est utilisé pour la communication entre le client et le serveur.

=>Dans une base de données relationnelle, les données sont représentées sous forme de **tables**

--------------------------------
Liens de vidéo 

[001. MySQL - Introduction](https://youtu.be/zR3KW6Uy5G8)

[002. MySQL - définitions mysql sql ]( https://youtu.be/1pGpKOjl7rc)
