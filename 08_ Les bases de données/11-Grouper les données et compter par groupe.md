# Grouper les données et compter par groupe  

## GROUP BY

La commande **GROUP BY** est utilisée en SQL pour grouper plusieurs résultats et utiliser une fonction de totaux sur un groupe de résultat

Pour regrouper les lignes selon un critère, il faut utiliser GROUP BY, qui se place après l'éventuelle clause WHERE  (sinon directement après FROM), suivi du nom de la colonne à utiliser comme critère de regroupement.

## Syntaxe 
````
SELECT ...FROM nom_table [WHERE condition] GROUP BY nom_colonne;
````
### Exemple 
````
SELECT client, SUM(tarif) FROM achat GROUP BY client
````
## GROUP BY et COUNT

### Exemple
**Exemple 1 :**
````
SELECT COUNT(*) AS nb_animaux
FROM Animal
GROUP BY espece_id;
````
**Exemple 2 :**
````
SELECT COUNT(*) AS nb_males
FROM Animal
WHERE sexe = 'M'
GROUP BY espece_id;
````
----------------------------
Lien de vidéo 
[014. MySQL - grouper les données et compter par groupe](https://youtu.be/cNWj9RmRFmQ)