# Sélectionner des données 

La requête qui permet de sélectionner et d'afficher des données s'appelle **SELECT**

=>**SELECT** permet donc d'afficher des données directement : des chaînes de caractères, des résultats de calculs, etc.
##  Syntaxe
````
SELECT colonne1, colonne2, ...
FROM nom_table;
````
On peut ajouter une condition avec le mot clé **WHERE**

## Description 
**SELECT** permet également de sélectionner des données à partir d'une table.

Pour cela, il faut ajouter une clause à la commande **SELECT** : la clause **FROM** qui définit de quelle structure (dans notre cas, une table) viennent les données.

## Sélectionner toutes les colonnes 

Si vous désirez sélectionner toutes les colonnes, vous pouvez utiliser le caractère * dans votre requête :
### Exemple :
````
SELECT *FROM Table;
````
La clause **WHERE** ("où" en anglais) permet de restreindre les résultats selon des critères de recherche

#### Exemple 
![img](../image/img_61.png)
![img](../image/img_62.png)


---------------------------

Lien de vidéo 

[010. MySQL - sélectionner des données avec select]( https://youtu.be/Ej2mRKXyLjc)