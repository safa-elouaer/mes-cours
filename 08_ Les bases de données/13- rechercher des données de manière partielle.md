#  Rechercher des données de manière partielle
Pour chercher d'une manière classique j'ajoute le **WHERE** et j'ajoute ma condition 
````
SELECT *FROM app.products WHERE  description="Jean"
````
## Recherche partielle
### Syntaxe 

````
SELECT *FROM app.products WHERE  description LIKE "%Je%"
````

### Exemple 

![img](../image/img_70.png)

--------------------------
[016. MySQL - rechercher des données de manière partielle ](https://youtu.be/jnryTVWPfvk)