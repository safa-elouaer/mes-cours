# Création d'une base de donnée et des tables dans une base de données 

## Création d'une nouvelle connexion et une nouvelle base de donnée

Pour ajouter une nouvelle connexion, une fois MySQL Workbench est installé : 

![img](../image/img_45.png)
 - Ajouter les informations à ajouter comme connexion name et garder les paramètres après appuyer sur ok

=> Voila une nouvelle connexion est créé
![img](../image/img_44.png)

### Créer une nouvelle base des données 
- Cliquer sur créer :Create new schema

![img](../image/img_46.png)
- Ajouter le nom de base de données

![img](../image/img_47.png)

## Créer une table 
- clique droite sur table et appuyer sur create new table 
- Nommer la table


![img](../image/img_48.png)
- aller remplir les colonnes et donner le type de chaque valeur


![img](../image/img_49.png)
### Type de champs dans MySQL 

![img](../image/img_51.png)

- **INT** : nombre entier ;
  
- **BIGINT ()** : Nombre très grand

- **TINYINT ()** : Nombre contenant que 0 ou 1

- **VARCHAR** : texte court (entre 1 et 255 caractères) ;

- **TEXT** : long texte (on peut y stocker un roman sans problème) ;
- **LONGTEXT** : Stocker de long texte
- **DATE** : date (jour, mois, année). 
-  **DATETIME** : Date et une heure des minutes et seconde
  
- **DÉCIMALE** : Décimale


## Configuration de table
- **PK** : Primary Key :La clé primaire d'une table est une contrainte d'unicité, composée d'une ou plusieurs colonnes. La clé primaire d'une ligne permet d'identifier de manière unique cette ligne dans la table.

- **NN** : Not Null permet que cette colonne ne peut pas contenir de valeur null

- **UQ** : Unique permet d'indiquer que cette colonne soit unique

- **BIN** : Binaire :Permet d'indiquer que nous allons contenir dans cette colonne une donnée de type binaire et non pas texte

- **UN** : Permet que cette colonne contienne un nombre non signé c-à-d un nombre qui n'est pas signé par positif ou négatif

- **ZF** : Zéro fill : Le fichier Zéro permet de remplir les espaces

- **AI** : En utilisant l'auto-incrémentation des colonnes. Incrémenter veut dire "ajouter une valeur fixée". Donc, si l'on déclare qu'une colonne doit s'auto-incrémenter (grâce au mot-clé AUTO_INCREMENT), plus besoin de chercher quelle valeur on va y mettre lors de la prochaine insertion. MySQL va chercher cela tout seul, comme un grand, en prenant la dernière valeur insérée dans cette colonne et en l'incrémentant de 1.


- **G** : Généré permet d'indiquer que cette colonne contienne une valeur qui va être évaluée. Cette valeur peut avoir deux types (soit virtuel, soit stocké)

- **DEFAULT** : Permet d'insérer une valeur par défaut


![img](../image/img_50.png)



-----------------------------------
Lien de vidéo 

[003. MySQL - installation](https://youtu.be/waf2ljjGvjg)

[004. MySQL - connexion au server mysql](https://youtu.be/1LMqSjqbvdo)

[005. MySQL - créer une table découverte des options ](https://youtu.be/IVE7pGjknSc)

[006. MySQL - configuration de la table produit]( https://youtu.be/dpi-9FplE2s)


