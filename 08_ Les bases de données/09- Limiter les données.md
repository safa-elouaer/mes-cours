# Limiter les données 

La clause **LIMIT** est à utiliser dans une requête SQL pour spécifier le nombre maximum de résultats que l’on souhaite obtenir.

## Syntaxe 
````
SELECT *FROM table LIMIT 10
````
=>Cette requête permet de récupérer seulement les 10 premiers résultats d’une table. Bien entendu, si la table contient moins de 10 résultats, alors la requête retournera toutes les lignes.

### Exemple :
Si je veux récupérer deux éléments à partie de début

![img](../image/img_64.png)

**Exemple 2**
````
SELECT *FROM table LIMIT 5, 10;
````
=>Cette requête retourne les enregistrements 6 à 15 d’une table. Le premier nombre est l’OFFSET tandis que le suivant est la limite.

**Exemple 3**
Avec condition :

![img](../image/img_65.png)

---------------------------
Lien de vidéo 

[012. MySQL - limiter les données sélectionnées avec limit ](https://youtu.be/zP8J8u2THZI)