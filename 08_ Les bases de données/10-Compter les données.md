# Compter les données 
En SQL, la fonction d’agrégation **COUNT()** permet de compter le nombre d’enregistrement dans une table. Connaître le nombre de lignes dans une table est très pratique dans de nombreux cas, par exemple pour savoir combien d’utilisateurs sont présents dans une table ou pour connaître le nombre de commentaires sur un article.

## Syntaxe
Pour connaître le nombre de lignes totales dans une table, il suffit d’effectuer la requête SQL suivante :
````
SELECT COUNT(*) FROM table
````
Il est aussi possible de connaitre le nombre d’enregistrement sur une colonne en particulier. Les enregistrements qui possèdent la valeur nul ne seront pas comptabilisé. La syntaxe pour compter les enregistrement sur la colonne “nom_colonne” est la suivante :

````
SELECT COUNT(nom_colonne) FROM table
````
Enfin, il est également possible de compter le nombre d’enregistrement distinct pour une colonne. La fonction ne comptabilisera pas les doublons pour une colonne choisie. La syntaxe pour compter le nombre de valeur distincte pour la colonne “nom_colonne” est la suivante :
````
SELECT COUNT(DISTINCT nom_colonne) FROM table
````
⭕ A savoir : en général, en terme de performance il est plutôt conseillé de filtrer les lignes avec GROUP BY si c’est possible, puis d’effectuer un COUNT(*).

## Utilisation de COUNT(*)

Pour compter le nombre d’utilisateurs total depuis que le site existe, il suffit d’utiliser COUNT(*) sur toute la table :
````
SELECT COUNT(*) FROM utilisateur
````
## Utilisation de COUNT(colonne)
Une autre méthode permet de compter le nombre d’utilisateurs ayant effectué au moins un achat. Il est possible de compter le nombre d’enregistrement sur la colonne “id_dernier_achat”. Sachant que la valeur est nulle s’il n’y a pas d’achat, les lignes ne seront pas comptées s’il n’y a pas eu d’achat. La requête est donc la suivante :

````
SELECT COUNT(id_dernier_achat) FROM utilisateur
````
## Utilisation de COUNT(DISTINCT colonne)
L’utilisation de la clause DISTINCT peut permettre de connaître le nombre de villes distinctes sur lesquels les visiteurs sont répartit. La requête serait la suivante :
````
SELECT COUNT(DISTINCT ville) FROM utilisateur
````
## Utilisation de COUNT(*) avec WHERE

Pour compter le nombre d’utilisateur qui ont effectué au moins un achat, il suffit de faire la même chose mais en filtrant les enregistrements avec WHERE :
````
SELECT COUNT(*) FROM utilisateur WHERE nombre_achat > 0
````

---------------------------------
Lien de vidéo 

[013. MySQL - compter les données avec la fonction count]( https://youtu.be/_x-5nrTnQww)