# Supprimer des données
La **DELETE** commande est utilisée pour supprimer les enregistrements existants dans une table.
Pour supprimer des données d’une table, nous allons utiliser l’instruction SQL DELETE FROM.

Pour préciser quelles entrées doivent être supprimées, nous allons accompagner **DELETE FROM** d’une clause **WHERE** nous permettant de cibler des données en particulier dans notre table.

En pratique, pour supprimer une entrée en particulier, nous utiliserons la clause WHERE sur une colonne « id » en ciblant un « id » précis.

Nous pouvons également supprimer plusieurs entrées en donnant une inégalité en condition de la clause WHERE (cibler tous les « id » supérieurs à 5 par exemple) ou en ciblant un autre type de données (supprimer toutes les entrées dont la valeur dans la colonne « Prenom » est « Pierre » par exemple).

❌ Cette opération est irréversible, soyez très prudent !
## Description 
La commande **DELETE** permet de supprimer un ou plusieurs enregistrements dans une table mais ne supprime pas la structure de la table. Associée à la commande WHERE, la commande DELETE permet de supprimer des enregistrements par critères.

## syntaxe 

**DELETE FROM** table où les enregistrements sont supprimés

**WHERE** condition ;

La commande WHERE est facultative. En son absence, tous les enregistrements de la table sont supprimés.
## Exemple :

![img](../image/img_57.png)

=>Si on veut mettre deux conditions il faut juste ajouter **AND**
## Exemple :
![img](../image/img_58.png)

--------------------------------
Lien de vidéo 
[008. MySQL - supprimer des données delete from](https://youtu.be/vcXVZBIuRGs)