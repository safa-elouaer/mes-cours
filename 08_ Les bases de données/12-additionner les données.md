# Additionner les données 
Dans le langage SQL, la fonction d’agrégation SUM() permet de calculer la somme totale d’une colonne contenant des valeurs numériques. Cette fonction ne fonction que sur des colonnes de types numériques (INT, FLOAT …) et n’additionne pas les valeurs NULL.
## Syntaxe 
````
SELECT SUM(nom_colonne)
FROM table
````
=>Cette requête SQL permet de calculer la somme des valeurs contenu dans la colonne “nom_colonne”.

✔ A savoir : Il est possible de filtrer les enregistrements avec la commande WHERE pour ne calculer la somme que des éléments souhaités.
### Exemple : 
Imaginons un système qui gère des factures et enregistre chaque achat dans une base de données. Ce système utilise une table “facture” contenant une ligne pour chaque produit. La table ressemble à l’exemple ci-dessous :
![img](../image/img_66.png)

## Somme avec WHERE
Pour calculer le montant de la facture n°1 il est possible d’utiliser la requête SQL suivante:
````
SELECT SUM(prix) AS prix_total
FROM facture
WHERE facture_id = 1
````
![img](../image/img_67.png)
## Somme avec GROUP BY
Pour calculer la somme de chaque facture, il est possible de grouper les lignes en se basant sur la colonne “facture_id”. Un tel résultat peut être obtenu en utilisant la requête suivante:
````
SELECT facture_id, SUM(prix) AS prix_total
FROM facture
GROUP BY facture_id
````
![img](../image/img_68.png)

## Exemple de cours 
![img](../image/img_69.png)

-----------------------------------

Lien de vidéo 

[015. MySQL - additionner les données grâce a la fonction numérique sum ](https://youtu.be/GuyovYkZdaU)


